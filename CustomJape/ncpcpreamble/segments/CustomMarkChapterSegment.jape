Phase: MarkChapter1Segment
Input: Act_Head Part_Head Book_Head Title_Head Chapter_Head Amendment_Segment EOD
Options: control = all
Rule: DoMarkChapter1Segment
(
	({Chapter_Head notWithin Amendment_Segment}):left
	({Amendment_Segment})*
	({Act_Head notWithin Amendment_Segment} | {Part_Head notWithin Amendment_Segment} | {Book_Head notWithin Amendment_Segment} | {Title_Head notWithin Amendment_Segment} | {Chapter_Head notWithin Amendment_Segment} | {EOD}):right
):reference
-->
{
	Node chapterStart = ((AnnotationSet) bindings.get("left")).firstNode();
	Node chapterEnd = ((AnnotationSet) bindings.get("right")).firstNode();

	//create property segmentRuleName
	String segmentRuleName = "Chapter1Segment";
	FeatureMap features = Factory.newFeatureMap();
	features.put("segmentRuleName", segmentRuleName);
	outputAS.add(chapterStart, chapterEnd, "Chapter_Segment", features);

	Annotation chapterHeadAnnotation = bindings.get("left").iterator().next();
	AnnotationSet span = (AnnotationSet) bindings.get("reference");

	//================Get all the contained Section_Segment annotations ==============
	Iterator<Annotation> sectionSegmentIterator = inputAS.getContained(span.firstNode().getOffset(), span.lastNode().getOffset()).get("Section_Segment").iterator();
	while (sectionSegmentIterator.hasNext())
	{
		Annotation sectionSegment = sectionSegmentIterator.next();
		FeatureMap sectionSegmentFeatures = sectionSegment.getFeatures();
		sectionSegmentFeatures.put("Chapter_id", chapterHeadAnnotation.getFeatures().get("id"));
	}

	//================Get all the contained Subsection_Segment annotations ==============
	Iterator<Annotation> subsectionSegmentIterator = inputAS.getContained(span.firstNode().getOffset(), span.lastNode().getOffset()).get("Subsection_Segment").iterator();
	while (subsectionSegmentIterator.hasNext())
	{
		Annotation subsectionSegment = subsectionSegmentIterator.next();
		FeatureMap subsectionSegmentFeatures = subsectionSegment.getFeatures();
		subsectionSegmentFeatures.put("Chapter_id", chapterHeadAnnotation.getFeatures().get("id"));
	}

	//================Get all the contained Article_Segment annotations ==============
	Iterator<Annotation> articleSegmentIterator = inputAS.getContained(span.firstNode().getOffset(), span.lastNode().getOffset()).get("Article_Segment").iterator();
	while (articleSegmentIterator.hasNext())
	{
		Annotation articleSegment = articleSegmentIterator.next();
		FeatureMap articleSegmentFeatures = articleSegment.getFeatures();
		articleSegmentFeatures.put("Chapter_id", chapterHeadAnnotation.getFeatures().get("id"));
	}

	//================Get all the contained Paragraph_Segment annotations ==============
	Iterator<Annotation> paragraphSegmentIterator = inputAS.getContained(span.firstNode().getOffset(), span.lastNode().getOffset()).get("Paragraph_Segment").iterator();
	while (paragraphSegmentIterator.hasNext())
	{
		Annotation paragraphSegment = paragraphSegmentIterator.next();
		FeatureMap paragraphSegmentFeatures = paragraphSegment.getFeatures();
		paragraphSegmentFeatures.put("Chapter_id", chapterHeadAnnotation.getFeatures().get("id"));
	}

	//================Get all the contained Alinea_Segment annotations ==============
	Iterator<Annotation> alineaSegmentIterator = inputAS.getContained(span.firstNode().getOffset(), span.lastNode().getOffset()).get("Alinea_Segment").iterator();
	while (alineaSegmentIterator.hasNext())
	{
		Annotation alineaSegment = alineaSegmentIterator.next();
		FeatureMap alineaSegmentFeatures = alineaSegment.getFeatures();
		alineaSegmentFeatures.put("Chapter_id", chapterHeadAnnotation.getFeatures().get("id"));
	}

	//================Get all the contained CrossReference_ref annotations ==============
	Iterator<Annotation> crossRefIterator = inputAS.getContained(span.firstNode().getOffset(), span.lastNode().getOffset()).get("CrossReference_ref").iterator();
	while (crossRefIterator.hasNext())
	{
		Annotation crossRef = crossRefIterator.next();
		FeatureMap crossRefFeatures = crossRef.getFeatures();
		crossRefFeatures.put("Chapter_id", chapterHeadAnnotation.getFeatures().get("id"));
	}
}
Phase: MarkChapter2Segment
Input: Act_Head Part_Head Book_Head Title_Head Chapter_Head Amendment_Segment Amendment_End
Options: control = all
Rule: DoMarkChapter2Segment
(
	({Chapter_Head,Chapter_Head within Amendment_Segment}):left
	({Act_Head,Act_Head within Amendment_Segment} | {Part_Head,Part_Head within Amendment_Segment} | {Book_Head,Book_Head within Amendment_Segment} | {Title_Head,Title_Head within Amendment_Segment} | {Chapter_Head,Chapter_Head within Amendment_Segment} | {Amendment_End}):right
):reference
-->
{
	Node chapterStart = ((AnnotationSet) bindings.get("left")).firstNode();
	Node chapterEnd = ((AnnotationSet) bindings.get("right")).firstNode();

	//create property segmentRuleName
	String segmentRuleName = "Chapter2Segment";
	FeatureMap features = Factory.newFeatureMap();
	features.put("segmentRuleName", segmentRuleName);
	outputAS.add(chapterStart, chapterEnd, "Chapter_Segment", features);

	Annotation chapterHeadAnnotation = bindings.get("left").iterator().next();
	AnnotationSet span = (AnnotationSet) bindings.get("reference");

	//================Get all the contained Section_Segment annotations ==============
	Iterator<Annotation> sectionSegmentIterator = inputAS.getContained(span.firstNode().getOffset(), span.lastNode().getOffset()).get("Section_Segment").iterator();
	while (sectionSegmentIterator.hasNext())
	{
		Annotation sectionSegment = sectionSegmentIterator.next();
		FeatureMap sectionSegmentFeatures = sectionSegment.getFeatures();
		sectionSegmentFeatures.put("Chapter_id", chapterHeadAnnotation.getFeatures().get("id"));
	}

	//================Get all the contained Subsection_Segment annotations ==============
	Iterator<Annotation> subsectionSegmentIterator = inputAS.getContained(span.firstNode().getOffset(), span.lastNode().getOffset()).get("Subsection_Segment").iterator();
	while (subsectionSegmentIterator.hasNext())
	{
		Annotation subsectionSegment = subsectionSegmentIterator.next();
		FeatureMap subsectionSegmentFeatures = subsectionSegment.getFeatures();
		subsectionSegmentFeatures.put("Chapter_id", chapterHeadAnnotation.getFeatures().get("id"));
	}

	//================Get all the contained Article_Segment annotations ==============
	Iterator<Annotation> articleSegmentIterator = inputAS.getContained(span.firstNode().getOffset(), span.lastNode().getOffset()).get("Article_Segment").iterator();
	while (articleSegmentIterator.hasNext())
	{
		Annotation articleSegment = articleSegmentIterator.next();
		FeatureMap articleSegmentFeatures = articleSegment.getFeatures();
		articleSegmentFeatures.put("Chapter_id", chapterHeadAnnotation.getFeatures().get("id"));
	}

	//================Get all the contained Paragraph_Segment annotations ==============
	Iterator<Annotation> paragraphSegmentIterator = inputAS.getContained(span.firstNode().getOffset(), span.lastNode().getOffset()).get("Paragraph_Segment").iterator();
	while (paragraphSegmentIterator.hasNext())
	{
		Annotation paragraphSegment = paragraphSegmentIterator.next();
		FeatureMap paragraphSegmentFeatures = paragraphSegment.getFeatures();
		paragraphSegmentFeatures.put("Chapter_id", chapterHeadAnnotation.getFeatures().get("id"));
	}

	//================Get all the contained Alinea_Segment annotations ==============
	Iterator<Annotation> alineaSegmentIterator = inputAS.getContained(span.firstNode().getOffset(), span.lastNode().getOffset()).get("Alinea_Segment").iterator();
	while (alineaSegmentIterator.hasNext())
	{
		Annotation alineaSegment = alineaSegmentIterator.next();
		FeatureMap alineaSegmentFeatures = alineaSegment.getFeatures();
		alineaSegmentFeatures.put("Chapter_id", chapterHeadAnnotation.getFeatures().get("id"));
	}

	//================Get all the contained CrossReference_ref annotations ==============
	Iterator<Annotation> crossRefIterator = inputAS.getContained(span.firstNode().getOffset(), span.lastNode().getOffset()).get("CrossReference_ref").iterator();
	while (crossRefIterator.hasNext())
	{
		Annotation crossRef = crossRefIterator.next();
		FeatureMap crossRefFeatures = crossRef.getFeatures();
		crossRefFeatures.put("Chapter_id", chapterHeadAnnotation.getFeatures().get("id"));
	}
}
