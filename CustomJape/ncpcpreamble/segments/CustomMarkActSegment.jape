Phase: MarkAct1Segment
Input: Act_Head Amendment_Segment EOD
Options: control = all
Rule: DoMarkAct1Segment
(
	({Act_Head notWithin Amendment_Segment}):left
	({Amendment_Segment})*
	({Act_Head notWithin Amendment_Segment} | {EOD}):right
):reference
-->
{
	Node actStart = ((AnnotationSet) bindings.get("left")).firstNode();
	Node actEnd = ((AnnotationSet) bindings.get("right")).firstNode();

	//create property segmentRuleName
	String segmentRuleName = "Act1Segment";
	FeatureMap features = Factory.newFeatureMap();
	features.put("segmentRuleName", segmentRuleName);
	outputAS.add(actStart, actEnd, "Act_Segment", features);

	Annotation actHeadAnnotation = bindings.get("left").iterator().next();
	AnnotationSet span = (AnnotationSet) bindings.get("reference");

	//================Get all the contained Part_Segment annotations ==============
	Iterator<Annotation> partSegmentIterator = inputAS.getContained(span.firstNode().getOffset(), span.lastNode().getOffset()).get("Part_Segment").iterator();
	while (partSegmentIterator.hasNext())
	{
		Annotation partSegment = partSegmentIterator.next();
		FeatureMap partSegmentFeatures = partSegment.getFeatures();
		partSegmentFeatures.put("Act_id", actHeadAnnotation.getFeatures().get("id"));
	}

	//================Get all the contained Book_Segment annotations ==============
	Iterator<Annotation> bookSegmentIterator = inputAS.getContained(span.firstNode().getOffset(), span.lastNode().getOffset()).get("Book_Segment").iterator();
	while (bookSegmentIterator.hasNext())
	{
		Annotation bookSegment = bookSegmentIterator.next();
		FeatureMap bookSegmentFeatures = bookSegment.getFeatures();
		bookSegmentFeatures.put("Act_id", actHeadAnnotation.getFeatures().get("id"));
	}

	//================Get all the contained Title_Segment annotations ==============
	Iterator<Annotation> titleSegmentIterator = inputAS.getContained(span.firstNode().getOffset(), span.lastNode().getOffset()).get("Title_Segment").iterator();
	while (titleSegmentIterator.hasNext())
	{
		Annotation titleSegment = titleSegmentIterator.next();
		FeatureMap titleSegmentFeatures = titleSegment.getFeatures();
		titleSegmentFeatures.put("Act_id", actHeadAnnotation.getFeatures().get("id"));
	}

	//================Get all the contained Chapter_Segment annotations ==============
	Iterator<Annotation> chapterSegmentIterator = inputAS.getContained(span.firstNode().getOffset(), span.lastNode().getOffset()).get("Chapter_Segment").iterator();
	while (chapterSegmentIterator.hasNext())
	{
		Annotation chapterSegment = chapterSegmentIterator.next();
		FeatureMap chapterSegmentFeatures = chapterSegment.getFeatures();
		chapterSegmentFeatures.put("Act_id", actHeadAnnotation.getFeatures().get("id"));
	}

	//================Get all the contained Section_Segment annotations ==============
	Iterator<Annotation> sectionSegmentIterator = inputAS.getContained(span.firstNode().getOffset(), span.lastNode().getOffset()).get("Section_Segment").iterator();
	while (sectionSegmentIterator.hasNext())
	{
		Annotation sectionSegment = sectionSegmentIterator.next();
		FeatureMap sectionSegmentFeatures = sectionSegment.getFeatures();
		sectionSegmentFeatures.put("Act_id", actHeadAnnotation.getFeatures().get("id"));
	}

	//================Get all the contained Subsection_Segment annotations ==============
	Iterator<Annotation> subsectionSegmentIterator = inputAS.getContained(span.firstNode().getOffset(), span.lastNode().getOffset()).get("Subsection_Segment").iterator();
	while (subsectionSegmentIterator.hasNext())
	{
		Annotation subsectionSegment = subsectionSegmentIterator.next();
		FeatureMap subsectionSegmentFeatures = subsectionSegment.getFeatures();
		subsectionSegmentFeatures.put("Act_id", actHeadAnnotation.getFeatures().get("id"));
	}

	//================Get all the contained Article_Segment annotations ==============
	Iterator<Annotation> articleSegmentIterator = inputAS.getContained(span.firstNode().getOffset(), span.lastNode().getOffset()).get("Article_Segment").iterator();
	while (articleSegmentIterator.hasNext())
	{
		Annotation articleSegment = articleSegmentIterator.next();
		FeatureMap articleSegmentFeatures = articleSegment.getFeatures();
		articleSegmentFeatures.put("Act_id", actHeadAnnotation.getFeatures().get("id"));
	}

	//================Get all the contained Paragraph_Segment annotations ==============
	Iterator<Annotation> paragraphSegmentIterator = inputAS.getContained(span.firstNode().getOffset(), span.lastNode().getOffset()).get("Paragraph_Segment").iterator();
	while (paragraphSegmentIterator.hasNext())
	{
		Annotation paragraphSegment = paragraphSegmentIterator.next();
		FeatureMap paragraphSegmentFeatures = paragraphSegment.getFeatures();
		paragraphSegmentFeatures.put("Act_id", actHeadAnnotation.getFeatures().get("id"));
	}

	//================Get all the contained Alinea_Segment annotations ==============
	Iterator<Annotation> alineaSegmentIterator = inputAS.getContained(span.firstNode().getOffset(), span.lastNode().getOffset()).get("Alinea_Segment").iterator();
	while (alineaSegmentIterator.hasNext())
	{
		Annotation alineaSegment = alineaSegmentIterator.next();
		FeatureMap alineaSegmentFeatures = alineaSegment.getFeatures();
		alineaSegmentFeatures.put("Act_id", actHeadAnnotation.getFeatures().get("id"));
	}

	//================Get all the contained CrossReference_ref annotations ==============
	Iterator<Annotation> crossRefIterator = inputAS.getContained(span.firstNode().getOffset(), span.lastNode().getOffset()).get("CrossReference_ref").iterator();
	while (crossRefIterator.hasNext())
	{
		Annotation crossRef = crossRefIterator.next();
		FeatureMap crossRefFeatures = crossRef.getFeatures();
		crossRefFeatures.put("Act_id", actHeadAnnotation.getFeatures().get("id"));
	}
}
Phase: MarkAct2Segment
Input: Act_Head Amendment_Segment Amendment_End
Options: control = all
Rule: DoMarkAct2Segment
(
	({Act_Head,Act_Head within Amendment_Segment}):left
	({Act_Head,Act_Head within Amendment_Segment} | {Amendment_End}):right
):reference
-->
{
	Node actStart = ((AnnotationSet) bindings.get("left")).firstNode();
	Node actEnd = ((AnnotationSet) bindings.get("right")).firstNode();

	//create property segmentRuleName
	String segmentRuleName = "Act2Segment";
	FeatureMap features = Factory.newFeatureMap();
	features.put("segmentRuleName", segmentRuleName);
	outputAS.add(actStart, actEnd, "Act_Segment", features);

	Annotation actHeadAnnotation = bindings.get("left").iterator().next();
	AnnotationSet span = (AnnotationSet) bindings.get("reference");

	//================Get all the contained Part_Segment annotations ==============
	Iterator<Annotation> partSegmentIterator = inputAS.getContained(span.firstNode().getOffset(), span.lastNode().getOffset()).get("Part_Segment").iterator();
	while (partSegmentIterator.hasNext())
	{
		Annotation partSegment = partSegmentIterator.next();
		FeatureMap partSegmentFeatures = partSegment.getFeatures();
		partSegmentFeatures.put("Act_id", actHeadAnnotation.getFeatures().get("id"));
	}

	//================Get all the contained Book_Segment annotations ==============
	Iterator<Annotation> bookSegmentIterator = inputAS.getContained(span.firstNode().getOffset(), span.lastNode().getOffset()).get("Book_Segment").iterator();
	while (bookSegmentIterator.hasNext())
	{
		Annotation bookSegment = bookSegmentIterator.next();
		FeatureMap bookSegmentFeatures = bookSegment.getFeatures();
		bookSegmentFeatures.put("Act_id", actHeadAnnotation.getFeatures().get("id"));
	}

	//================Get all the contained Title_Segment annotations ==============
	Iterator<Annotation> titleSegmentIterator = inputAS.getContained(span.firstNode().getOffset(), span.lastNode().getOffset()).get("Title_Segment").iterator();
	while (titleSegmentIterator.hasNext())
	{
		Annotation titleSegment = titleSegmentIterator.next();
		FeatureMap titleSegmentFeatures = titleSegment.getFeatures();
		titleSegmentFeatures.put("Act_id", actHeadAnnotation.getFeatures().get("id"));
	}

	//================Get all the contained Chapter_Segment annotations ==============
	Iterator<Annotation> chapterSegmentIterator = inputAS.getContained(span.firstNode().getOffset(), span.lastNode().getOffset()).get("Chapter_Segment").iterator();
	while (chapterSegmentIterator.hasNext())
	{
		Annotation chapterSegment = chapterSegmentIterator.next();
		FeatureMap chapterSegmentFeatures = chapterSegment.getFeatures();
		chapterSegmentFeatures.put("Act_id", actHeadAnnotation.getFeatures().get("id"));
	}

	//================Get all the contained Section_Segment annotations ==============
	Iterator<Annotation> sectionSegmentIterator = inputAS.getContained(span.firstNode().getOffset(), span.lastNode().getOffset()).get("Section_Segment").iterator();
	while (sectionSegmentIterator.hasNext())
	{
		Annotation sectionSegment = sectionSegmentIterator.next();
		FeatureMap sectionSegmentFeatures = sectionSegment.getFeatures();
		sectionSegmentFeatures.put("Act_id", actHeadAnnotation.getFeatures().get("id"));
	}

	//================Get all the contained Subsection_Segment annotations ==============
	Iterator<Annotation> subsectionSegmentIterator = inputAS.getContained(span.firstNode().getOffset(), span.lastNode().getOffset()).get("Subsection_Segment").iterator();
	while (subsectionSegmentIterator.hasNext())
	{
		Annotation subsectionSegment = subsectionSegmentIterator.next();
		FeatureMap subsectionSegmentFeatures = subsectionSegment.getFeatures();
		subsectionSegmentFeatures.put("Act_id", actHeadAnnotation.getFeatures().get("id"));
	}

	//================Get all the contained Article_Segment annotations ==============
	Iterator<Annotation> articleSegmentIterator = inputAS.getContained(span.firstNode().getOffset(), span.lastNode().getOffset()).get("Article_Segment").iterator();
	while (articleSegmentIterator.hasNext())
	{
		Annotation articleSegment = articleSegmentIterator.next();
		FeatureMap articleSegmentFeatures = articleSegment.getFeatures();
		articleSegmentFeatures.put("Act_id", actHeadAnnotation.getFeatures().get("id"));
	}

	//================Get all the contained Paragraph_Segment annotations ==============
	Iterator<Annotation> paragraphSegmentIterator = inputAS.getContained(span.firstNode().getOffset(), span.lastNode().getOffset()).get("Paragraph_Segment").iterator();
	while (paragraphSegmentIterator.hasNext())
	{
		Annotation paragraphSegment = paragraphSegmentIterator.next();
		FeatureMap paragraphSegmentFeatures = paragraphSegment.getFeatures();
		paragraphSegmentFeatures.put("Act_id", actHeadAnnotation.getFeatures().get("id"));
	}

	//================Get all the contained Alinea_Segment annotations ==============
	Iterator<Annotation> alineaSegmentIterator = inputAS.getContained(span.firstNode().getOffset(), span.lastNode().getOffset()).get("Alinea_Segment").iterator();
	while (alineaSegmentIterator.hasNext())
	{
		Annotation alineaSegment = alineaSegmentIterator.next();
		FeatureMap alineaSegmentFeatures = alineaSegment.getFeatures();
		alineaSegmentFeatures.put("Act_id", actHeadAnnotation.getFeatures().get("id"));
	}

	//================Get all the contained CrossReference_ref annotations ==============
	Iterator<Annotation> crossRefIterator = inputAS.getContained(span.firstNode().getOffset(), span.lastNode().getOffset()).get("CrossReference_ref").iterator();
	while (crossRefIterator.hasNext())
	{
		Annotation crossRef = crossRefIterator.next();
		FeatureMap crossRefFeatures = crossRef.getFeatures();
		crossRefFeatures.put("Act_id", actHeadAnnotation.getFeatures().get("id"));
	}
}
