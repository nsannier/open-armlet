# Open ARMLET

Open ARMLET (Automated Retrieval of Metadata in Legal Texts) is the open source version of ARMLET, a framework aimed, in this version, at retrieving structural metadata, e.g., titles, chapters, articles, paragraphs, alineas as well as cross-references from legal texts, more specifically legislative acts.  
The goal of the ARMLET project is to provide a configurable framework for automated extraction of legal metadata from legal texts to support activities on legal texts such as open data, legal search, legal compliance checking legal data management and targets more particularly the conversion of legislative texts from doc(x) or PDF to XML for open and transparent legislative data

This project has been developed over the years at the Interdisciplinary Centre for Security, Reliability and Trust (SnT) from the University of Luxembourg, in collaboration with the Central Legislative Service (Service central de législation) from the Ministry of State of Luxembourg.  
In particular, ARMLET has been developed, tested and improved over several legislative acts, including codes, laws and regulations. 

Some configuration examples provided in the projects are actual legislative acts published on the legilux portal (http://legilux.public.lu), the online Official Gazette of the Grand-Duchy of Luxembourg.

# License and Copyright

ARMLET is copyrighted © by University of Luxembourg / Interdisciplinary Centre for Security, Reliability and Trust - 2014-2019.

Acknowledged co-authors (by alphabetical order):           

- Adedjouma Morayo (morayoade@gmail.com)
- Briand Lionel (briand@svv.lu)
- Dou Wei (dou@svv.lu)
- Sabetzadeh Mehrdad (mike@svv.lu)
- Sannier Nicolas (sanniver@svv.lu)
- Tassan-Zanin-Caser Virgil (virgil.tassan@gmail.com)


ARMLET is released under the GNU LESSER GENERAL PUBLIC LICENSE, version 3 of June 2007 (referred to as "the License" below). Please consider a careful reading of the license file (licence.html) in the project folder.  
We make use of several 3rd-party tools which are licensed separately, but these licenses are compatible with the License.  
In particular, we use the NLP framework Gate (http://gate.ac.uk), also available under LGPLv3 license.

This software is delivered "as is"  by the University of Luxembourg  and the co-authorswith no warranties  whatsoever, including but not limited to any warranty of use, implementation, integration, merchantability, non-infringement, fitness for any particular purpose or other warranty otherwise arising out of any proposal, specification or sample.


# Folder Organization

- Note that ARMLET is built upon the NLP framework Gate (https://gate.ac.uk), an open source framework developed and maintained by the University of Sheffield. (Gate 8.1 and Libs folders).
- The NLP framework leverages NLP scripts in the jape language, that can be bundled into applications and executed over the texts. In ARMLET, many of the scripts we use are aimed to be executed as-is over the text (Jape folder), whereas some will be automatically generated or reused (CustomJape folder), depending of the configuration you bring in. 
- Some configuration files can be found in the Test folder. 
- Regarding the structure configuration, they are defined according to two files, a <name>_structure.json file and <name>_rules.json file. The first one is aimed at describing the structural hierarchy of the document, the second one is aimed at describing the detection rules for these structures. More details are provided in the documentation. Example configurations can be found in the Structure and CustomStructure folders. Note that the file location is quite irrelevant as long as both files are collocated together and  correctly referenced in the configuration file and that the naming convention is correct. From these two files, ARMLET will automatically generate jape scripts and Gate applications in the CustomJape and Apps folders for the detection of structural metadata.


# Installation and Setup

ARMLET comes as a whole and is executed via calling the ArmletMain.jar with one argument, which is the path to a configuration file containing the necessary information for converting a document into xml.
Therefore, elements location should not be changed since the ArmletMain.Jar will dynamically call Gate and its plug-ins at precise locations.

More information can be found in the documentation folder.

# Related Publications

(All Author's preprint version of the publications are available at https://nicolassannier.wordpress.com/research/research/)

**- Digitizing Luxembourg's Legal Corpora: Experience and Vision**  
*John Dann, Mehrdad Sabetzadeh, Nicolas Sannier, and Lionel Briand (do not follow the contribution order nor alphabetical order), in the 18th Law via the Internet conference (LVI'2018), Florence, Italy, 11-12 October 2018*

**- Legal Markup Generation in the Large: An Experience Report**  
*Nicolas Sannier, Morayo Adedjouma, Mehrdad Sabetzadeh, Lionel C. Briand, John Dann, Marc Hisette, Pascal Thill, in the proceedings of the 25th IEEE International Requirements Engineering Conference (RE'2017), Lisbon, Portugal, pp 302-311, 4-8 September  2017.*

**- From RELAW Research to Practice: Reflections on an Ongoing Technology Transfer Project**  
*Nicolas Sannier, Mehrdad Sabetzadeh and Lionel C. Briand, in the proceedings of the IEEE 25th International Requirements Engineering Conference Workshops (RELAW), Lisbon, Portugal, pp 204?208, September 5th, 2017.*

**- An Automated Framework for Detection and Resolution of Cross References in Legal Texts**  
*Nicolas Sannier, Morayo Adedjouma, Mehrdad Sabetzadeh, and Lionel Briand. , in the Requirements Engineering Journal 22(2): 215-237 (2017) (1st online Nov. 17, 2015).*
