package japescriptgen;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;


import gateStandAlone.ConfigTool;

import org.json.JSONException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**	Copyright (c) {2014-2019}, University of Luxembourg, Interdisciplinary Centre for Security, Reliability and Trust (SnT)
* 	Nicolas Sannier (sannier@svv.lu),
*	Morayo Adedjouma (morayoade@gmail.com),
*	Virgil Tassan (tassan-zanin-caser@svv.lu),
*	Wei Dou (dou@svv.lu),
*	Mehrdad Sabetzadeh (sabetzadeh@svv.lu),
*	Lionel Briand (briand@svv.lu),
*/

public class JapeScriptsGenerator {
	
    public static final ObjectMapper objectMapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
    
    private Map<String, Object> commonProperties;
    private DocumentStructure documentStructure;
    private ScriptTemplates scriptTemplates;
    private Map<String, Object> rules;
    private DocumentNode elementsRoot;
    private StringBuffer indent = new StringBuffer();
    private BufferedWriter bufferedWriter;
	private ArrayList<String> structure;
	

    //Constructor initialize templates file and structure file
    public JapeScriptsGenerator (File structureJsonFile, File templateFile) throws IOException, JSONException {
        assert(structureJsonFile.exists());
        extractElements(structureJsonFile);
        loadTemplates(templateFile);
    }

    //generate the Header.jape files
    public void generateHeaderScripts (File ruleFile, File generatedRuleFile, String outputFolderName) throws IOException, JSONException {
    	System.out.println("ruleFile :"+ruleFile);
        loadJapeRules(ruleFile, generatedRuleFile);
        //create output directory
        prepareOutputFolder(outputFolderName);
        DocumentNode element = elementsRoot;
        structure = new ArrayList<String>();
        //generate a file for each element
        // TODO generate highest element script after the while to have all the elements of the structure (and no more)
        while (element != null) {
            structure.add(element.name);
            System.out.println("ELEMENT NAME : "+element.name);
            if(element.name.equals(ConfigTool.highestElementName)){
            	writeToHighestElementHeaderScriptFile(outputFolderName + ConfigTool.pathJoiner + String.format(ConfigTool.headerScriptFileNameTemplate, StringUtils.capitalize(element.name)), element);
            }else{
            	writeToHeaderScriptFile(outputFolderName + ConfigTool.pathJoiner + String.format(ConfigTool.headerScriptFileNameTemplate, StringUtils.capitalize(element.name)), element); 
            }
            //iterate to the child
            element = element.child;    
        }
        System.out.println("Done with header scripts!");
        
        try{
    		if(generatedRuleFile.delete()){
    			System.out.println(generatedRuleFile.getName() + " is deleted!");
    		}else{
    			System.out.println("Delete operation is failed.");
    		}
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    }

	//generate the Segment.jape files
    public void generateSegmentScripts (String outputFolderName) throws IOException {
    	//create output directory
        prepareOutputFolder(outputFolderName);
        DocumentNode element = elementsRoot;
        List<String> parentElementsCapitalizedNames = new ArrayList<>();
        //generate a file for each element

        while (element != null) {
            parentElementsCapitalizedNames.add(StringUtils.capitalize(element.name));// Inclusive
            writeToSegmentScriptFile(outputFolderName + ConfigTool.pathJoiner + String.format(ConfigTool.segmentScriptFileNameTemplate, StringUtils.capitalize(element.name)), element, parentElementsCapitalizedNames);

            //iteration
            element = element.child;

        }
        writeCustomSegmentPreamble(outputFolderName);
        System.out.println("Done with segment scripts!");
    }
    
    //write Act_Header.jape file
    private void writeToHighestElementHeaderScriptFile(String outputFilePath, DocumentNode element) throws IOException {
        FileWriter fileWriter = null;
        String elementName = element.name;
        try {
        	//generate the file and the buffered writer to write in it
            fileWriter = new FileWriter(outputFilePath);
            bufferedWriter = new BufferedWriter(fileWriter);
            
            //build list of rules (one for all but alinea)
            String capitalizedElementName = StringUtils.capitalize(elementName);
            Object rule = rules.getOrDefault(elementName, String.format(ConfigTool.defaultRule, elementName));

            if (!(rule instanceof List)) {
                rule = Arrays.asList(rule);
            }
            
			@SuppressWarnings("unchecked")
			List<String> rulesList = (List<String>)rule;
            String ruleName = capitalizedElementName;
            String headSuffix = "Head";
            writeEndWithNewLine(ConfigTool.importFileExpression);
            for (int i = 0; i < rulesList.size(); i++, ruleName = capitalizedElementName+i) {
            	
            	//write Phase/Input/Options
                writeTemplateEndWithNewLine(scriptTemplates.phase, Arrays.asList(ruleName+headSuffix));
                writeTemplateEndWithNewLine(scriptTemplates.input, Arrays.asList("Token SOD Preamble_start Article_Head Part_Head Book_Head Title_Head Chapter_Head Section_Head Subsection_Head NamedParagraph_Head"));
                writeTemplateEndWithNewLine(scriptTemplates.options, Arrays.asList("once"));
                
                //write the rules
                writeTemplateEndWithNewLine(scriptTemplates.rule, Arrays.asList(ruleName+headSuffix));
                //write open parenthesis
                
//                remove the mandatory header to improve flexibilty in the header span <: header> will have to be specified
//                openBlock(scriptTemplates.openRuleBlock);
                
                writeActRuleLine();
                //write closed parenthesis
                
//                closeBlock(String.format(scriptTemplates.closeRuleBlock, headerString));
                
                //write an arrow
                writeEndWithNewLine(scriptTemplates.arrow);

                //write bracket
                openBlock(scriptTemplates.openJavaBlock);

                writeTemplateEndWithNewLine(scriptTemplates.getAnnotationSet, Arrays.asList(ConfigTool.headerString, ConfigTool.headerString));
                writeTemplateEndWithNewLine(scriptTemplates.nodeStart, Arrays.asList(ConfigTool.headerString, ConfigTool.headerString));
                writeTemplateEndWithNewLine(scriptTemplates.nodeEnd, Arrays.asList(ConfigTool.headerString, ConfigTool.headerString));

            
		        for (String propertyName : commonProperties.keySet()) {
		            bufferedWriter.write(scriptTemplates.newLine);
		            comment(ConfigTool.commentCreateProperty, Arrays.asList(propertyName));
		            writeTemplateEndWithNewLine(scriptTemplates.createNodeString, Arrays.asList(propertyName));
		            writeTemplateEndWithNewLine(scriptTemplates.getAnnotationSet, Arrays.asList(propertyName, propertyName));
		            writeTemplateEndWithNewLine(scriptTemplates.ifAnnotationNotNull, Arrays.asList(propertyName));
		
		            openBlock(scriptTemplates.openJavaBlock);
		
		            writeTemplateEndWithNewLine(scriptTemplates.nodeStart, Arrays.asList(propertyName, propertyName));
		            writeTemplateEndWithNewLine(scriptTemplates.nodeEnd, Arrays.asList(propertyName, propertyName));
		            writeTemplateEndWithNewLine(scriptTemplates.getNodeString, Arrays.asList(propertyName, propertyName, propertyName));
		            
		            if(propertyName.equals("id")){
		            	writeEndWithNewLine("id = GateHeaderConverter.convertExpression(id);");
		            }
		            
		            closeBlock(scriptTemplates.closeJavaBlock);
		        }
		        
		        bufferedWriter.write(scriptTemplates.newLine);
		        comment(ConfigTool.commentCreateFeatureMap);
		        writeEndWithNewLine(scriptTemplates.createFeatures);
		
		        for (String propertyName : commonProperties.keySet()) {
		            writeTemplateEndWithNewLine(scriptTemplates.addFeature, Arrays.asList(propertyName, propertyName));
		        }

		        writeTemplateEndWithNewLine(scriptTemplates.associateFeatures, Arrays.asList(ConfigTool.headerString, ConfigTool.headerString, capitalizedElementName+"_"+headSuffix));
		
		        closeBlock(scriptTemplates.closeJavaBlock);               
		            
		        if(element.child == null){
		            bufferedWriter.write(writeCleanAnnotation());             
		        }
            }
	        System.out.printf("Generating file %s...\n", outputFilePath);
        }catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bufferedWriter != null)
                bufferedWriter.close();
            if (fileWriter != null)
                fileWriter.close();
        }
	}
        
        
    //write a Header.jape file
    public void writeToHeaderScriptFile(String outputFilePath, DocumentNode element) throws IOException {
        FileWriter fileWriter = null;
        String elementName = element.name;
        try {
        	//generate the file and the buffered writer to write in it
            fileWriter = new FileWriter(outputFilePath);
            bufferedWriter = new BufferedWriter(fileWriter);
            
            //build list of rules (one for all but alinea)
            String capitalizedElementName = StringUtils.capitalize(elementName);
            Object rule = rules.getOrDefault(elementName, String.format(ConfigTool.defaultRule, elementName));

            if (!(rule instanceof List)) {
                rule = Arrays.asList(rule);
            }
            
			@SuppressWarnings("unchecked")
			List<String> rulesList = (List<String>)rule;
            String ruleName = capitalizedElementName;
            String headSuffix = "Head";
            writeEndWithNewLine(ConfigTool.importFileExpression);
            for (int i = 0; i < rulesList.size(); i++) {
            	
            	ruleName = capitalizedElementName+(i+1);
            	
            	//write Phase/Input/Options
                writeTemplateEndWithNewLine(scriptTemplates.phase, Arrays.asList(ruleName+headSuffix));
                if(elementName.equals("alinea")){
                	writeTemplateEndWithNewLine(scriptTemplates.input, Arrays.asList(capitalizedElementName+" Article_Head "+ConfigTool.headerScriptInput));
                }else{
                	writeTemplateEndWithNewLine(scriptTemplates.input, Arrays.asList(capitalizedElementName+" "+ConfigTool.headerScriptInput));
                }
                writeTemplateEndWithNewLine(scriptTemplates.options, Arrays.asList(ConfigTool.headerOption));
                
                //write the rules
                writeTemplateEndWithNewLine(scriptTemplates.rule, Arrays.asList(ruleName+headSuffix));
                //write open parenthesis
                
//                remove the mandatory header to improve flexibilty in the header span <: header> will have to be specified
//                openBlock(scriptTemplates.openRuleBlock);
                
                writeEndWithNewLine(rulesList.get(i));
                //write closed parenthesis

//                closeBlock(String.format(scriptTemplates.closeRuleBlock, headerString));
                
                //write an arrow
                writeEndWithNewLine(scriptTemplates.arrow);

                //write bracket
                openBlock(scriptTemplates.openJavaBlock);

                writeTemplateEndWithNewLine(scriptTemplates.getAnnotationSet, Arrays.asList(ConfigTool.headerString, ConfigTool.headerString));
                writeTemplateEndWithNewLine(scriptTemplates.nodeStart, Arrays.asList(ConfigTool.headerString, ConfigTool.headerString));
                writeTemplateEndWithNewLine(scriptTemplates.nodeEnd, Arrays.asList(ConfigTool.headerString, ConfigTool.headerString));

                //write common properties
                for (String propertyName : commonProperties.keySet()) {
                    if (rulesList.get(i).contains(propertyName)){
	                    bufferedWriter.write(scriptTemplates.newLine);
	                    comment(ConfigTool.commentCreateProperty, Arrays.asList(propertyName));
	                    writeTemplateEndWithNewLine(scriptTemplates.createNodeString, Arrays.asList(propertyName));
	                    writeTemplateEndWithNewLine(scriptTemplates.getAnnotationSet, Arrays.asList(propertyName, propertyName));
	                    writeTemplateEndWithNewLine(scriptTemplates.ifAnnotationNotNull, Arrays.asList(propertyName));
	
	                    openBlock(scriptTemplates.openJavaBlock);
	
	                    writeTemplateEndWithNewLine(scriptTemplates.nodeStart, Arrays.asList(propertyName, propertyName));
	                    writeTemplateEndWithNewLine(scriptTemplates.nodeEnd, Arrays.asList(propertyName, propertyName));
	                    writeTemplateEndWithNewLine(scriptTemplates.getNodeString, Arrays.asList(propertyName, propertyName, propertyName));
	                    
	                    if(propertyName.equals("id")){
	                    	writeEndWithNewLine("id = GateHeaderConverter.convertExpression(id);");
	                    }
	                    
	                    closeBlock(scriptTemplates.closeJavaBlock);
	                }
                }
                
		        //Part useful for the test of the SuperApp. Can may be remove after
	            bufferedWriter.write(scriptTemplates.newLine);
	            comment(ConfigTool.commentCreateProperty, Arrays.asList("ruleName"));
	            writeEndWithNewLine("String ruleName = \""+ruleName+headSuffix+"\";");


                bufferedWriter.write(scriptTemplates.newLine);
                comment(ConfigTool.commentCreateFeatureMap);
                writeEndWithNewLine(scriptTemplates.createFeatures);

                for (String propertyName : commonProperties.keySet()) {
                	if (rulesList.get(i).contains(propertyName)){
                		writeTemplateEndWithNewLine(scriptTemplates.addFeature, Arrays.asList(propertyName, propertyName));
                	}
                }
                
		        //Line for the superApp
		        writeTemplateEndWithNewLine(scriptTemplates.addFeature, Arrays.asList("ruleName", "ruleName"));
		        
                writeTemplateEndWithNewLine(scriptTemplates.associateFeatures, Arrays.asList(ConfigTool.headerString, ConfigTool.headerString, capitalizedElementName+"_"+headSuffix));

                closeBlock(scriptTemplates.closeJavaBlock);               
                
            }
            if(element.child == null){
                bufferedWriter.write(writeCleanAnnotation());             
            }
            
            System.out.printf("Generating file %s...\n", outputFilePath);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bufferedWriter != null)
                bufferedWriter.close();
            if (fileWriter != null)
                fileWriter.close();
        }
    }

    // TODO write with Wei's methods
    
    private void writeCustomSegmentPreamble(String outputFolderName) throws IOException{
    	
    	String highLevelElement = structure.get(1);
    	String headerHighLevel = WordUtils.capitalize(highLevelElement)+"_Head";
    	String titleScript = "PreambleSegment";
    	StringBuffer japeScript = new StringBuffer();
    	japeScript.append(beginScript(titleScript,
    			"Preamble_start Article_Head Section_Head "+headerHighLevel,
    			"once",
    			titleScript));
		japeScript.append("(");
		japeScript.append(System.getProperty("line.separator"));
		japeScript.append("({Preamble_start}):start");
		japeScript.append(System.getProperty("line.separator"));
		//TODO remove hard-coded annotations
		japeScript.append("({"+headerHighLevel+"}  | {Article_Head} | {Section_Head}):end");
		japeScript.append(System.getProperty("line.separator"));
		japeScript.append(")");
		japeScript.append(System.getProperty("line.separator"));
		japeScript.append("-->");
		japeScript.append(System.getProperty("line.separator"));
		japeScript.append("{");
		japeScript.append(System.getProperty("line.separator"));
		japeScript.append("Node start = ((AnnotationSet) bindings.get(\"start\")).firstNode();");
		japeScript.append(System.getProperty("line.separator"));
		japeScript.append("Node end   = ((AnnotationSet) bindings.get(\"end\")).firstNode(); ");      
		japeScript.append(System.getProperty("line.separator"));
		japeScript.append("String preamble = doc.getContent().toString().substring(start.getOffset().intValue(), end.getOffset().intValue());");
		japeScript.append(System.getProperty("line.separator"));		  
		japeScript.append("//create the new token");
		japeScript.append(System.getProperty("line.separator"));
		japeScript.append("FeatureMap features = Factory.newFeatureMap();");
		japeScript.append(System.getProperty("line.separator"));
		japeScript.append("features.put(\"string\", preamble);");
		japeScript.append(System.getProperty("line.separator"));
		japeScript.append("outputAS.add(start, end, \"Preamble_Segment\", features);");
		japeScript.append(System.getProperty("line.separator"));
		japeScript.append("}");
		//Cleaning annotations
		japeScript.append(System.getProperty("line.separator"));
		japeScript.append("//Clean the Alinea_Head annotations inside Preamble_Segment");
		japeScript.append(System.getProperty("line.separator"));
		japeScript.append("Phase: MarkCleanAlineaAnnotationsInsidePreamble");
		japeScript.append(System.getProperty("line.separator"));
		japeScript.append("Input:  Alinea_Head Preamble_Segment");
		japeScript.append(System.getProperty("line.separator"));
		japeScript.append("Options: control = appelt");
		japeScript.append(System.getProperty("line.separator"));
		japeScript.append("Rule: DoMarkCleanAlineaInsidePreamble");
		japeScript.append(System.getProperty("line.separator"));
		japeScript.append("(");
		japeScript.append(System.getProperty("line.separator"));
		japeScript.append("{Alinea_Head within Preamble_Segment}");
		japeScript.append(System.getProperty("line.separator"));
		japeScript.append("):reference");
		japeScript.append(System.getProperty("line.separator"));
		japeScript.append("-->");
		japeScript.append(System.getProperty("line.separator"));
		japeScript.append("{");
		japeScript.append(System.getProperty("line.separator"));
		japeScript.append("AnnotationSet toRemove = (gate.AnnotationSet) bindings.get(\"reference\");");
		japeScript.append(System.getProperty("line.separator"));
		japeScript.append("outputAS.removeAll(toRemove);");
		japeScript.append(System.getProperty("line.separator"));
		japeScript.append("inputAS.removeAll(toRemove);");
		japeScript.append(System.getProperty("line.separator"));
		japeScript.append("}");
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(outputFolderName+ConfigTool.pathJoiner+ConfigTool.customSegmentPreambleFile);
	        bufferedWriter = new BufferedWriter(fileWriter);
	        bufferedWriter.write(new String(japeScript));
		} catch (IOException e) {
			e.printStackTrace();
		 } finally {
	        if (bufferedWriter != null)
	            bufferedWriter.close();
	        if (fileWriter != null)
	            fileWriter.close();
	    }
    }
    // TODO write with Wei's methods
    private String beginScript(String scriptTitle, String scriptInput, String scriptOptions, String scriptRule) {
    	StringBuffer japeScript = new StringBuffer();
    	japeScript.append(String.format(scriptTemplates.phase,scriptTitle));
    	japeScript.append(scriptTemplates.newLine);
		japeScript.append(String.format(scriptTemplates.input,scriptInput));
		japeScript.append(scriptTemplates.newLine);
		japeScript.append(String.format(scriptTemplates.options,scriptOptions));
		japeScript.append(scriptTemplates.newLine);
		japeScript.append(String.format(scriptTemplates.rule,scriptRule));
		japeScript.append(scriptTemplates.newLine);
		return new String(japeScript);
	}

    // TODO write with Wei's methods
	private String writeCleanAnnotation(){
    	
    	ArrayList<String> capitalizedStructure = new ArrayList<String>();
		for (String s : structure){
			capitalizedStructure.add(WordUtils.capitalize(s));
		}
		ArrayList<String> headerList = new ArrayList<String>();
		for (String s : capitalizedStructure){
			headerList.add(s+"_Head"); 
		}

		// getting the last element (e.g.: alinea)
		String lastElement = capitalizedStructure.get(capitalizedStructure.size()-1);

		
		// preparing the list of "within" annotations
		ArrayList<String> withinAnnotations = new ArrayList<String>();
		//Alinea_Head within Article_Head and without the last element
		for (int i = 0; i < headerList.size()-1; i++){
			String s = headerList.get(i);
			withinAnnotations.add("{"+headerList.get(headerList.size()-1) + " within " + s + "}");
		}
		String regExp = withinAnnotations.toString();
		regExp = regExp.replace("[","(");
		regExp = regExp.replace("]",")");
		regExp = regExp.replace(", "," | ");

		
		// getting the input parameters
		String input = "";
		for (String s : headerList){
			input = input + " " + s;
		}
//		input = "Input:" + input;
//
    	StringBuffer japeScript = new StringBuffer(); // to replace by a file writer		
		japeScript.append("//Clean the "+lastElement+"_Head annotations (remove duplicates and the annotations within other)");
 		japeScript.append(System.getProperty("line.separator"));
		japeScript.append(beginScript("Clean"+lastElement+"Annotations",
				input,
				"appelt",
				"Clean"+lastElement));
		
		japeScript.append("(");
		japeScript.append(System.getProperty("line.separator"));
		
		japeScript.append(regExp);
				
		japeScript.append(System.getProperty("line.separator"));
		japeScript.append("):reference");
		japeScript.append(System.getProperty("line.separator"));
		japeScript.append("-->");
		japeScript.append(System.getProperty("line.separator"));
		japeScript.append("{");
		japeScript.append(System.getProperty("line.separator"));
		japeScript.append("AnnotationSet toRemove = (gate.AnnotationSet) bindings.get(\"reference\");");
		japeScript.append(System.getProperty("line.separator"));
		japeScript.append("outputAS.removeAll(toRemove);");
		japeScript.append(System.getProperty("line.separator"));
		japeScript.append("inputAS.removeAll(toRemove);");
		japeScript.append(System.getProperty("line.separator"));
		japeScript.append("}");
		return new String(japeScript);
    }
    
    //write a Segment.jape file
    private void writeToSegmentScriptFile(String outputFilePath, DocumentNode element, List<String> parentElementsCapitalizedNames) throws IOException {
        FileWriter fileWriter = null;
        try {
            String elementName = element.name;
            String capitalizedElementName = StringUtils.capitalize(elementName);
            String segmentSuffix = "Segment";
            //Create new file
            fileWriter = new FileWriter(outputFilePath);
            bufferedWriter = new BufferedWriter(fileWriter);
            for(int k=0; k<2;k++){
            	DocumentNode elementCopy = element; 
	            writeTemplateEndWithNewLine(scriptTemplates.phase, Arrays.asList(capitalizedElementName+(k+1)+segmentSuffix)); 
	            StringBuilder inputBuilder = new StringBuilder();
	            String inputSuffix = "_Head";
	            String inputDelimiter = " ";
	            StringBuilder ruleBuilder = new StringBuilder();
	            String ruleDelimiter = " | ";
	            String parentElementCapitalizedName;
	            for (int i = 0; i < parentElementsCapitalizedNames.size(); i++) {
	                parentElementCapitalizedName = parentElementsCapitalizedNames.get(i);
	                inputBuilder.append(parentElementCapitalizedName+inputSuffix+inputDelimiter);
		            //to deal with amendments
		            if(k==0){
//		            	ruleBuilder.append("{"+parentElementCapitalizedName+inputSuffix+"}"+ruleDelimiter);
		            	ruleBuilder.append("{"+parentElementCapitalizedName+inputSuffix+" notWithin Amendment_Segment}"+ruleDelimiter);
		            }else{
		            	ruleBuilder.append("{"+parentElementCapitalizedName+inputSuffix+","+parentElementCapitalizedName+inputSuffix+" within Amendment_Segment}"+ruleDelimiter);
		            }
	            }
	            //to deal with amendments
	            inputBuilder.append("Amendment_Segment ");
	            if(k==0){
		            inputBuilder.append(ConfigTool.endOfDocument);
		            ruleBuilder.append("{"+ConfigTool.endOfDocument+"}");
	            }else{
		            inputBuilder.append("Amendment_End");
		            ruleBuilder.append("{Amendment_End}");
	            }
	            writeTemplateEndWithNewLine(scriptTemplates.input, Arrays.asList(inputBuilder.toString()));
	            writeTemplateEndWithNewLine(scriptTemplates.options, Arrays.asList(ConfigTool.segmentOption));
	            writeTemplateEndWithNewLine(scriptTemplates.rule, Arrays.asList(capitalizedElementName+(k+1)+segmentSuffix));
	
	            openBlock(scriptTemplates.openRuleBlock);
	            //to deal with amendments
	            if(k==0){
//	            	writeTemplateEndWithNewLine(ConfigTool.segmentRuleGroup, Arrays.asList("{"+capitalizedElementName+inputSuffix+"}", ConfigTool.segmentBeginVariable));
	            	writeTemplateEndWithNewLine(ConfigTool.segmentRuleGroup, Arrays.asList("{"+capitalizedElementName+inputSuffix+" notWithin Amendment_Segment}", ConfigTool.segmentBeginVariable));
	            	writeEndWithNewLine("({Amendment_Segment})*");
	            }else{
	            	writeTemplateEndWithNewLine(ConfigTool.segmentRuleGroup, Arrays.asList("{"+capitalizedElementName+inputSuffix+","+capitalizedElementName+inputSuffix+" within Amendment_Segment}", ConfigTool.segmentBeginVariable));
	            }
	            writeTemplateEndWithNewLine(ConfigTool.segmentRuleGroup, Arrays.asList(ruleBuilder.toString(), ConfigTool.segmentEndVariable));
	
	            closeBlock(String.format(scriptTemplates.closeRuleBlock, ConfigTool.segmentSpanVariable));
	
	            writeEndWithNewLine(scriptTemplates.arrow);
	
	            openBlock(scriptTemplates.openJavaBlock);

	            writeTemplateEndWithNewLine(scriptTemplates.segmentStart, Arrays.asList(elementName, ConfigTool.segmentBeginVariable));
	            writeTemplateEndWithNewLine(scriptTemplates.segmentEnd, Arrays.asList(elementName, ConfigTool.segmentEndVariable));
	            
		        //Part useful for the test of the SuperApp. Can may be remove after
	            bufferedWriter.write(scriptTemplates.newLine);
	            comment(ConfigTool.commentCreateProperty, Arrays.asList("segmentRuleName"));
	            writeEndWithNewLine("String segmentRuleName = \""+capitalizedElementName+(k+1)+segmentSuffix+"\";");

	            writeEndWithNewLine(scriptTemplates.createFeatures);
		        //To know which rule was detected
	            writeTemplateEndWithNewLine(scriptTemplates.addFeature, Arrays.asList("segmentRuleName", "segmentRuleName"));
	            
	            writeTemplateEndWithNewLine(scriptTemplates.associateFeatures, Arrays.asList(elementName, elementName, capitalizedElementName+"_"+segmentSuffix));
	            bufferedWriter.write(scriptTemplates.newLine);
	
	            writeTemplateEndWithNewLine(scriptTemplates.getHeadAnnotation, Arrays.asList(elementName, ConfigTool.segmentBeginVariable));
	            writeTemplateEndWithNewLine(scriptTemplates.getSpan, Arrays.asList(ConfigTool.segmentSpanVariable));
	
	            String subElementSegmentVariable;
	            String subElementSegmentId;
	            while (elementCopy.child != null) {
	            	elementCopy = elementCopy.child;
	                subElementSegmentVariable = elementCopy.name+segmentSuffix;
	                subElementSegmentId = StringUtils.capitalize(elementCopy.name)+"_"+segmentSuffix;
	                bufferedWriter.write(scriptTemplates.newLine);
	                comment(ConfigTool.commentGetElementAnnotations, Arrays.asList(subElementSegmentId));
	                writeTemplateEndWithNewLine(scriptTemplates.getContainedIterator, Arrays.asList(subElementSegmentVariable, subElementSegmentId));
	                writeTemplateEndWithNewLine(scriptTemplates.whileIteratorHasNext, Arrays.asList(subElementSegmentVariable));
	                openBlock(scriptTemplates.openJavaBlock);
	                writeTemplateEndWithNewLine(scriptTemplates.getNextAnnotation, Arrays.asList(subElementSegmentVariable, subElementSegmentVariable));
	                writeTemplateEndWithNewLine(scriptTemplates.getFeatures, Arrays.asList(subElementSegmentVariable, subElementSegmentVariable));
	                writeTemplateEndWithNewLine(scriptTemplates.addContextualFeature, Arrays.asList(subElementSegmentVariable, capitalizedElementName, elementName));
	                closeBlock(scriptTemplates.closeJavaBlock);
	            }
	            bufferedWriter.write(scriptTemplates.newLine);
	            comment(ConfigTool.commentGetElementAnnotations, Arrays.asList(ConfigTool.crossReferenceId));
	            writeTemplateEndWithNewLine(scriptTemplates.getContainedIterator, Arrays.asList(ConfigTool.crossReferenceVariable, ConfigTool.crossReferenceId));
	            writeTemplateEndWithNewLine(scriptTemplates.whileIteratorHasNext, Arrays.asList(ConfigTool.crossReferenceVariable));
	            openBlock(scriptTemplates.openJavaBlock);
	            writeTemplateEndWithNewLine(scriptTemplates.getNextAnnotation, Arrays.asList(ConfigTool.crossReferenceVariable, ConfigTool.crossReferenceVariable));
	            writeTemplateEndWithNewLine(scriptTemplates.getFeatures, Arrays.asList(ConfigTool.crossReferenceVariable, ConfigTool.crossReferenceVariable));
	            writeTemplateEndWithNewLine(scriptTemplates.addContextualFeature, Arrays.asList(ConfigTool.crossReferenceVariable, capitalizedElementName, elementName));
	            closeBlock(scriptTemplates.closeJavaBlock);
	            closeBlock(scriptTemplates.closeJavaBlock);
        	}
            System.out.printf("Generating file %s...\n", outputFilePath);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bufferedWriter != null)
                bufferedWriter.close();
            if (fileWriter != null)
                fileWriter.close();
        }
    }

    //indent the future text
    public void indent() {
        indent.append(scriptTemplates.tab);
    }

    //unindent the future text
    public void unindent() {
        if (indent.length() > 0) {
            indent.deleteCharAt(indent.length() - 1);
        }
    }

    //write an open bracket/parentheses then indent text on next lines
    public void openBlock(String open) throws IOException {
        writeEndWithNewLine(open);
        indent();
    }

    //write a closed bracket/parentheses then unindent text on next lines
    public void closeBlock(String close) throws IOException {
        unindent();
        writeEndWithNewLine(close);
    }

    //write content as comment
    public void comment(String content) throws IOException {
        writeTemplateEndWithNewLine(scriptTemplates.comment, Arrays.asList(content));
    }

    //write a comment line with a placeholder
    public void comment(String templateString, List<String> content) throws IOException {
        writeTemplateEndWithNewLine(scriptTemplates.comment, Arrays.asList(String.format(templateString, content.toArray())));
    }

    //write content on a line with new line
    public void writeEndWithNewLine(String content) throws IOException {
        bufferedWriter.write(indent+content+ scriptTemplates.newLine);
    }

    //not used
    public void writeTemplate(String templateString, List<Object> args) throws IOException {
        bufferedWriter.write(indent+String.format(templateString, args.toArray()));
    }
    
    //write line with several placeholders then make a new line
    public void writeTemplateEndWithNewLine(String templateString, List<String> list) throws IOException {
        bufferedWriter.write(indent+String.format(templateString, list.toArray())+ scriptTemplates.newLine);
    }

    public void writeActRuleLine() throws IOException{
    	//TODO remove hard-coded names
    	String content = "(({Token} ({Token})+):title):header({Preamble_start}|{Article_Head}|{Part_Head}|{Book_Head}|{Title_Head}|{Chapter_Head}|{Section_Head}|{Subsection_Head}|{NamedParagraph_Head}):right";
    	writeEndWithNewLine(content);
    }
    
    //load the templates from templateFile in the attribute templates 
    public void loadTemplates(File templateFile) throws IOException, JSONException {
        scriptTemplates = objectMapper.readValue(templateFile, ScriptTemplates.class);
    }

    // Load Jape rules TO DO
    @SuppressWarnings("unchecked")
	public void loadJapeRules(File ruleFile, File generatedRuleFile) throws IOException, JSONException {
        // Read the headerRules file
        if (generatedRuleFile.exists() && generatedRuleFile.lastModified() > ruleFile.lastModified()) {
            rules = objectMapper.readValue(generatedRuleFile, Map.class);
        } else {
            rules = objectMapper.readValue(ruleFile, Map.class);
            List<String> tempRulesList;

            String generatedRuleFileName = generatedRuleFile.getPath();
            if (!generatedRuleFile.exists()) {
                // Create the rule file
                if(!generatedRuleFile.createNewFile()) {
                    System.out.printf("Error: the file \"%s\" cannot be created. Please make sure you have write permission.\n", generatedRuleFileName);
                }
            }
            // Set up JsonGenerator
            JsonGenerator ruleFileGenerator = objectMapper.getFactory().createGenerator(generatedRuleFile, JsonEncoding.UTF8);
            ruleFileGenerator.useDefaultPrettyPrinter();
            ruleFileGenerator.writeStartObject();

            String key;
            Object rule;
            List<String> rulesList;
            // Create Rules
            for (Map.Entry<String, Object> rulesEntry: rules.entrySet()) {
                key = rulesEntry.getKey();
                rule = rulesEntry.getValue();
                
                if (rule instanceof List) {
                    rulesList = (List<String>)rule;
                    tempRulesList = new ArrayList<>();
                    for (int i = 0; i < rulesList.size(); i++) {
                        tempRulesList.add(parseJapeRule(rulesList.get(i)));
                    }
                    rules.put(key, tempRulesList);
                } else {
                    rules.put(key, parseJapeRule((String)rule));
                }
                //write rule in the generateHeaderRules.json
                ruleFileGenerator.writeObjectField(key, rules.get(key));
            }
            ruleFileGenerator.writeEndObject();
            ruleFileGenerator.close();
            System.out.printf("Generating rule file %s...\n", generatedRuleFileName);
        }
    }

    //
    public String parseJapeRule(String rule) {
        Matcher ruleMatcher = Pattern.compile(ConfigTool.ruleReplacePattern).matcher(rule);
        StringBuffer sb = new StringBuffer();
        String subRuleKey;

        // http://stackoverflow.com/a/38296697
        while (ruleMatcher.find()) {
            subRuleKey = ruleMatcher.group(1);
            if (rules.containsKey(subRuleKey)) {
//            	System.out.println("TESTTTTT : "+subRuleKey+" "+rules.get(subRuleKey));
                ruleMatcher.appendReplacement(sb, parseJapeRule((String)rules.get(subRuleKey)));
            } else {
                ruleMatcher.appendReplacement(sb, String.format(ConfigTool.defaultRule, subRuleKey));
            }
        }
        ruleMatcher.appendTail(sb);
        return sb.toString();
    }


    @SuppressWarnings("unchecked")
	public void extractElements(File structureJsonFile) throws IOException {
    	//read structure.json file
        documentStructure = objectMapper.readValue(structureJsonFile, DocumentStructure.class);
        //get the common properties
        commonProperties = objectMapper.convertValue(documentStructure.commonProperties, Map.class);
          
        //link the 3 kinds of element
        elementsRoot = documentStructure.highLevelDivisionElements;
        buildDoubleLinkedList(
                buildDoubleLinkedList(
                        buildDoubleLinkedList(null, elementsRoot),
                        documentStructure.basicElements
                ),
                documentStructure.subDivisionElements
        );
    }

    
    //Construct the parent-child relations between elements
    private DocumentNode buildDoubleLinkedList(DocumentNode parent, DocumentNode childRoot) {
        if (parent != null) {
            parent.child = childRoot;
            childRoot.parent = parent;
        }
        DocumentNode element = childRoot;
        while (element.child != null) {
            element.child.parent = element; // Add parent
            element = element.child;
        }
        return element; // Now the element is at the bottom
    }

    //Create directory for the output files
    private File prepareOutputFolder(String outputFolderName) {
        File outputFolder = new File(outputFolderName);

        if (!outputFolder.exists()) {
            if (!outputFolder.mkdirs()) {
                System.out.printf("Error: the directory \"%s\" cannot be created. Please make sure you have write permission.\n", outputFolderName);
            }
        }
        //why return???
        return outputFolder;
    }

}