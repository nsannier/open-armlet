package japescriptgen;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**	Copyright (c) {2014-2019}, University of Luxembourg, Interdisciplinary Centre for Security, Reliability and Trust (SnT)
* 	Nicolas Sannier (sannier@svv.lu),
*	Morayo Adedjouma (morayoade@gmail.com),
*	Virgil Tassan (tassan-zanin-caser@svv.lu),
*	Wei Dou (dou@svv.lu),
*	Mehrdad Sabetzadeh (sabetzadeh@svv.lu),
*	Lionel Briand (briand@svv.lu),
*/

@JsonIgnoreProperties(ignoreUnknown = true)
public class ScriptTemplates {
    public String comment;
    public String newLine;
    public String tab;
    public String arrow;
    public String openJavaBlock;
    public String closeJavaBlock;
    public String openRuleBlock;
    public String closeRuleBlock;
    public String phase;
    public String input;
    public String options;
    public String rule;
    public String getAnnotationSet;
    public String ifAnnotationNotNull;
    public String nodeStart;
    public String nodeEnd;
    public String createNodeString;
    public String getNodeString;
    public String createFeatures;
    public String addFeature;
    public String associateFeatures;
    public String segmentStart;
    public String segmentEnd;
    public String getHeadAnnotation;
    public String getSpan;
    public String getContainedIterator;
    public String whileIteratorHasNext;
    public String getNextAnnotation;
    public String getFeatures;
    public String addContextualFeature;
}
