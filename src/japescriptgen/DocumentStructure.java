package japescriptgen;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**	Copyright (c) {2014-2019}, University of Luxembourg, Interdisciplinary Centre for Security, Reliability and Trust (SnT)
* 	Nicolas Sannier (sannier@svv.lu),
*	Morayo Adedjouma (morayoade@gmail.com),
*	Virgil Tassan (tassan-zanin-caser@svv.lu),
*	Wei Dou (dou@svv.lu),
*	Mehrdad Sabetzadeh (sabetzadeh@svv.lu),
*	Lionel Briand (briand@svv.lu),
*/

public class DocumentStructure {
    public DocumentNode highLevelDivisionElements;
    public DocumentNode basicElements;
    public DocumentNode subDivisionElements;

    @JsonIgnoreProperties
    public Object commonProperties;
}
