package japescriptgen;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**	Copyright (c) {2014-2019}, University of Luxembourg, Interdisciplinary Centre for Security, Reliability and Trust (SnT)
* 	Nicolas Sannier (sannier@svv.lu),
*	Morayo Adedjouma (morayoade@gmail.com),
*	Virgil Tassan (tassan-zanin-caser@svv.lu),
*	Wei Dou (dou@svv.lu),
*	Mehrdad Sabetzadeh (sabetzadeh@svv.lu),
*	Lionel Briand (briand@svv.lu),
*/

public class DocumentNode {
    public String name;

    @JsonIgnoreProperties
    public String plural;

    @JsonIgnoreProperties
    public DocumentNode child;

    @JsonIgnoreProperties
    public DocumentNode parent;
}
