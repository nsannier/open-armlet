package fontGetterModel;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

/**	Copyright (c) {2014-2019}, University of Luxembourg, Interdisciplinary Centre for Security, Reliability and Trust (SnT)
* 	Nicolas Sannier (sannier@svv.lu),
*	Morayo Adedjouma (morayoade@gmail.com),
*	Virgil Tassan (tassan-zanin-caser@svv.lu),
*	Wei Dou (dou@svv.lu),
*	Mehrdad Sabetzadeh (sabetzadeh@svv.lu),
*	Lionel Briand (briand@svv.lu),
*/

public class Example {
	
	public static void main(String[] args) throws IOException, URISyntaxException{
//		File file = new File("Test/RG_20161103/RG_20161103_N14.doc");
//		File file = new File("Test/CodeTravailFirstPart.docx");
//		File file = new File("Test/TextForTest/Test.docx");
		File file = new File("Test/TextForTest/CoupledTanksReqs.docx");
		System.out.println(file.getAbsolutePath());
		ParsedDocument pd = new ParsedDocument(file.getAbsolutePath());
//		ParsedDocument pd = new ParsedDocument("files/Loi.docx");
//		ParsedDocument pd = new ParsedDocument("files/CodeTravail.docx");
		pd.compute();
//		pd.displayHeader();
//		System.out.println(pd.getContent());
//		System.out.println(pd.getText());
		System.out.println(pd.getExtractedText().length());
		System.out.println(pd.getText().length());
		System.out.println(pd.getRunsInd());
		System.out.println(pd.getSplits());
		System.out.println();
		//TODO solve index issues (small gap between docx and GATE)
		
//		NumberingTest nbt = new NumberingTest();
		
	}
	
}
