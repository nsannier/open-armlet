package fontGetterModel;

/**	Copyright (c) {2014-2019}, University of Luxembourg, Interdisciplinary Centre for Security, Reliability and Trust (SnT)
* 	Nicolas Sannier (sannier@svv.lu),
*	Morayo Adedjouma (morayoade@gmail.com),
*	Virgil Tassan (tassan-zanin-caser@svv.lu),
*	Wei Dou (dou@svv.lu),
*	Mehrdad Sabetzadeh (sabetzadeh@svv.lu),
*	Lionel Briand (briand@svv.lu),
*/

//Get all the information for a run
public class Run {

	private String text;
	private String fontFamily;
	private int fontSize;
	
	private String color;
	
	private boolean bold;
	private boolean italic;
	private String underline;
	
	private int beginRun;
	private int endRun;
	
	public Run(){

	}
	
	public Run(String text, String fontFamily, int fontSize, String color, boolean bold, boolean italic, String underline) {
		super();
		this.text = text;
		this.fontFamily = fontFamily;
		this.fontSize = fontSize;
		this.color = color;
		this.bold = bold;
		this.italic = italic;
		this.underline = underline;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getFontFamily() {
		return fontFamily;
	}

	public void setFontFamily(String fontFamily) {
		this.fontFamily = fontFamily;
	}

	public int getFontSize() {
		return fontSize;
	}

	public void setFontSize(int fontSize) {
		this.fontSize = fontSize;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public boolean isBold() {
		return bold;
	}

	public void setBold(boolean bold) {
		this.bold = bold;
	}

	public boolean isItalic() {
		return italic;
	}

	public void setItalic(boolean italic) {
		this.italic = italic;
	}

	public String getUnderline() {
		return underline;
	}

	public void setUnderline(String underline) {
		this.underline = underline;
	}	

	public int getBeginRun() {
		return beginRun;
	}

	public void setBeginRun(int beginRun) {
		this.beginRun = beginRun;
	}

	public int getEndRun() {
		return endRun;
	}

	public void setEndRun(int endRun) {
		this.endRun = endRun;
	}

	public String toString(){
		String res = "Text : "+this.getText()
			+"\nFont : "+this.getFontFamily()
			+"\nColor : "+this.getColor()
			+"\nFont Size : "+this.getFontSize()
			+"\nBold? : "+this.isBold()
			+"\nItalic? : "+this.isItalic()
			+"\nUnderline? : "+this.getUnderline()+"\n";	
		return res;
	}
	
	public String displayNum(){
		return "Run : begin : "+this.beginRun+", end : "+this.endRun+"\n";
	}

	
}
