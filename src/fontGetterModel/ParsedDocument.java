package fontGetterModel;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

/**	Copyright (c) {2014-2019}, University of Luxembourg, Interdisciplinary Centre for Security, Reliability and Trust (SnT)
* 	Nicolas Sannier (sannier@svv.lu),
*	Morayo Adedjouma (morayoade@gmail.com),
*	Virgil Tassan (tassan-zanin-caser@svv.lu),
*	Wei Dou (dou@svv.lu),
*	Mehrdad Sabetzadeh (sabetzadeh@svv.lu),
*	Lionel Briand (briand@svv.lu),
*/

public class ParsedDocument {

	private ArrayList<Line> content;
	private String text;
	private String documentPath;
	private String extractedText;
	
	public ParsedDocument(String documentPath){
		this.documentPath = documentPath;
	}
	
	public void compute() throws IOException{
		//read the document from documentPath
		 File file = new File(this.documentPath);
		 FileInputStream in = new FileInputStream(file);
	     XWPFDocument document= new XWPFDocument(in); 

		 XWPFWordExtractor ex = new XWPFWordExtractor(document);
		 this.extractedText = ex.getText();
		 ex.close();
		 
//		  get Default font size ... should work but doesn't exist...      
//	      document.getStyles().getDefaultRunStyles().getFontSize();,
	      
	      //create paragraph
	      List<XWPFParagraph> paragraphs = document.getParagraphs();
	      
	      
	      ArrayList<Line> seqPara = new ArrayList<Line>();
	      
	      
	      // place of the text
	      int textInd = 0;
	      //generate ID for each paragraph
	      int numID = 0;
	      //Initialize text of the whole document
	      StringBuffer alltext = new StringBuffer();
	      for(XWPFParagraph para : paragraphs){
	    	ArrayList<Run> seqRuns = new ArrayList<Run>();
      		String textPara = para.getText();
      		if(textPara.length() > 1){
		      	List<XWPFRun> runs = para.getRuns();
		      	for(XWPFRun run : runs){	      		
		        	Run r = new Run();
		        	r.setBold(run.isBold());
		        	r.setColor(run.getColor());
		        	r.setFontFamily(run.getFontFamily());
		        	r.setFontSize(run.getFontSize()); //get standard size from doc
		        	r.setItalic(run.isItalic());
		        	r.setUnderline(run.getUnderline().toString());
		        	r.setBeginRun(textInd);
		        	String text = run.getText(0);
		        	if(run.getText(0) != null){
		        		r.setText(text);
		        		textInd += text.length();
		        	}else{
		        		r.setText(new String(""));
		        	}
		        	
		        	r.setEndRun(textInd);
		        	seqRuns.add(r);
	      		}
		      	seqPara.add(new Line(numID,para.getText(),seqRuns));
		      	alltext.append(para.getText()+"\n");
		      	numID++;
      		}
	      }
	      this.content = seqPara;
	      this.text = new String(alltext);
//	      document.close();
	}
	
	public ArrayList<Line> getContent(){
		return this.content;
	}
	
	public ArrayList<Run> getRuns(){
		ArrayList<Run> runs = new ArrayList<Run>();
		for(Line p : this.content){
			runs.addAll(p.getRuns());
		}
		return runs;
	}
	
	public ArrayList<Integer> getSplits(){
		ArrayList<Integer> split = new ArrayList<Integer>();
		Run last = null;
		for(Line p : this.content){
			for(Run r : p.getRuns()){
				split.add(r.getBeginRun());
				last = r;
			}
		}
		split.add(last.getEndRun());
		return split;
	}
	
	public String getText(){
		return this.text;
	}

	public String getRunsInd() {
		StringBuffer sb = new StringBuffer();
		for(Line l : content){
			for(Run r : l.getRuns()){
				sb.append(r.displayNum());
			}
		}
		return new String(sb);
	}
	
	public String getExtractedText(){
		return this.extractedText;
	}
	
}
