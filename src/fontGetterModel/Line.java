package fontGetterModel;

import java.util.ArrayList;

/**	Copyright (c) {2014-2019}, University of Luxembourg, Interdisciplinary Centre for Security, Reliability and Trust (SnT)
* 	Nicolas Sannier (sannier@svv.lu),
*	Morayo Adedjouma (morayoade@gmail.com),
*	Virgil Tassan (tassan-zanin-caser@svv.lu),
*	Wei Dou (dou@svv.lu),
*	Mehrdad Sabetzadeh (sabetzadeh@svv.lu),
*	Lionel Briand (briand@svv.lu),
*/

public class Line {

	private ArrayList<Run> runs;
	private String text;
	private int id;
	private int beginLine;
	private int endLine;
	
	public Line(int id, String text, ArrayList<Run> runs){
		this.id = id;
		this.text = text;
		this.runs = runs;
	}

	public ArrayList<Run> getRuns() {
		return runs;
	}

	public void setRuns(ArrayList<Run> runs) {
		this.runs = runs;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String toString(){
		return "id : "+id+", Text : "+text+", Length : "+text.length()+"\n";
	}

	public int getBeginLine() {
		return beginLine;
	}

	public void setBeginLine(int beginLine) {
		this.beginLine = beginLine;
	}

	public int getEndLine() {
		return endLine;
	}

	public void setEndLine(int endLine) {
		this.endLine = endLine;
	}
	
}
