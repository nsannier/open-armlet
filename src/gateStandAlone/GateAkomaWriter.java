package gateStandAlone;

import gate.Annotation;
import gate.AnnotationSet;

import gate.util.OffsetComparator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**	Copyright (c) {2014-2019}, University of Luxembourg, Interdisciplinary Centre for Security, Reliability and Trust (SnT)
* 	Nicolas Sannier (sannier@svv.lu),
*	Morayo Adedjouma (morayoade@gmail.com),
*	Virgil Tassan (tassan-zanin-caser@svv.lu),
*	Wei Dou (dou@svv.lu),
*	Mehrdad Sabetzadeh (sabetzadeh@svv.lu),
*	Lionel Briand (briand@svv.lu),
*/

public class GateAkomaWriter {
	
	GateAkomaConverter converter = new GateAkomaConverter();
	
	public int idAmendment = 0;
	
	/** 
	 * Writes the Preface of the XML file
	 * @return
	 * @throws IOException
	 */
	public void writePreface(String title) throws IOException {
		GateGenericXMLWriter.XMLWriter.write("<preface><longTitle>");
		GateGenericXMLWriter.XMLWriter.newLine();
		if(!title.equals("")){
			GateGenericXMLWriter.XMLWriter.write("<p>"+title+"</p>");//TODO Updated by mark
		}else{
			GateGenericXMLWriter.XMLWriter.write("<p>"+Configuration.longTitle+"</p>");
		}

		GateGenericXMLWriter.XMLWriter.newLine();
		GateGenericXMLWriter.XMLWriter.write("</longTitle></preface>");
		GateGenericXMLWriter.XMLWriter.newLine();
	}
	
	
	/** 
	 * writes the Header of the XML AKOMA Ntoso file (<akomaNtoso><act>)
	 * requires the configuration file has been read and Configuration elements filled
	 * @return
	 * @throws IOException
	 */
	public void writeAkomaHeader() throws IOException {
		// writing the XML prolog
		GateGenericXMLWriter.XMLWriter.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		GateGenericXMLWriter.XMLWriter.newLine();
		// writing the Akoma Ntoso block
		GateGenericXMLWriter.XMLWriter.write("<akomaNtoso xmlns=\"http://docs.oasis-open.org/legaldocml/ns/akn/3.0/CSD13\" xmlns:scl=\"http://www.scl.lu\">");
		GateGenericXMLWriter.XMLWriter.newLine();
		// writing the opener for the act --- to be put elsewhere ?
		GateGenericXMLWriter.XMLWriter.write(converter.openAct());
		GateGenericXMLWriter.XMLWriter.newLine();
	}
	
	/**
	 * closes the act and the Akoma Ntoso elements (</body></act></akomaNtoso>)
	 * @throws IOException
	 */
	public void writeAkomaClosing() throws IOException{
		GateGenericXMLWriter.XMLWriter.write(converter.closeBody());
		GateGenericXMLWriter.XMLWriter.newLine();
		GateGenericXMLWriter.XMLWriter.write(converter.closeAct());
		GateGenericXMLWriter.XMLWriter.newLine();
		GateGenericXMLWriter.XMLWriter.write("</akomaNtoso>");
	}
	
	
	/**
	 *  Writes the Meta elements of the XML file
	 * @return
	 * @throws IOException
	 */
	public void writeMeta(AnnotationSet documentAnnotations) throws IOException {
		GateGenericXMLWriter.XMLWriter.write("<meta>");
		GateGenericXMLWriter.XMLWriter.newLine();
		
		
		// writing the FRBR part
		GateGenericXMLWriter.XMLWriter.write("<identification source=\"#somebody\">");
		GateGenericXMLWriter.XMLWriter.newLine();
		
		// writing the FRBR work part
		GateGenericXMLWriter.XMLWriter.write("<FRBRWork>");
		GateGenericXMLWriter.XMLWriter.newLine();
		GateGenericXMLWriter.XMLWriter.write("<FRBRthis value=\"\"/><FRBRuri value=\"\"/>");
		GateGenericXMLWriter.XMLWriter.newLine();
		GateGenericXMLWriter.XMLWriter.write("<FRBRdate name=\"\" date=\"2999-01-01\"/>" );
		GateGenericXMLWriter.XMLWriter.newLine();
		GateGenericXMLWriter.XMLWriter.write("<FRBRauthor href=\"#mdda\" as=\"#author\"/>");
		GateGenericXMLWriter.XMLWriter.newLine();
		GateGenericXMLWriter.XMLWriter.write("<FRBRcountry value=\"\"/>");
		GateGenericXMLWriter.XMLWriter.newLine();
		GateGenericXMLWriter.XMLWriter.write("</FRBRWork>");
		GateGenericXMLWriter.XMLWriter.newLine();

		// writing the FRBR expression part
		GateGenericXMLWriter.XMLWriter.write("<FRBRExpression>");
		GateGenericXMLWriter.XMLWriter.newLine();
		GateGenericXMLWriter.XMLWriter.write("<FRBRthis value=\"\"/>");
		GateGenericXMLWriter.XMLWriter.write("<FRBRuri value=\"\"/>");
		GateGenericXMLWriter.XMLWriter.write("<FRBRdate date=\"2999-01-01\" name=\"\"/>"); 
		GateGenericXMLWriter.XMLWriter.newLine();
		GateGenericXMLWriter.XMLWriter.write("<FRBRauthor href=\"#somebody\" as=\"#editor\"/>");
		GateGenericXMLWriter.XMLWriter.newLine();
		GateGenericXMLWriter.XMLWriter.write("<FRBRlanguage language=\"\"/>");
		GateGenericXMLWriter.XMLWriter.write("</FRBRExpression>");
		GateGenericXMLWriter.XMLWriter.newLine();

		// writing the manifestation part
		GateGenericXMLWriter.XMLWriter.write("<FRBRManifestation>");
		GateGenericXMLWriter.XMLWriter.newLine();
		GateGenericXMLWriter.XMLWriter.write("<FRBRthis value=\"\"/>");
		GateGenericXMLWriter.XMLWriter.newLine();
		GateGenericXMLWriter.XMLWriter.write("<FRBRuri value=\"\"/>");
		GateGenericXMLWriter.XMLWriter.newLine();
		GateGenericXMLWriter.XMLWriter.write("<FRBRdate date=\"2999-01-01\" name=\"\"/>"); 
		GateGenericXMLWriter.XMLWriter.newLine();
		GateGenericXMLWriter.XMLWriter.write("<FRBRauthor href=\"#somebody\" as=\"#editor\"/>");
		GateGenericXMLWriter.XMLWriter.write("</FRBRManifestation>");
		GateGenericXMLWriter.XMLWriter.newLine();
		
		// writing the scl work
		
		GateGenericXMLWriter.XMLWriter.write("<scl:JOLUXWork>");
		GateGenericXMLWriter.XMLWriter.newLine();
		
		GateGenericXMLWriter.XMLWriter.write("<scl:JOLUXComplexWork>");
		GateGenericXMLWriter.XMLWriter.newLine();
		GateGenericXMLWriter.XMLWriter.write("<scl:jolux scl:name=\"uriThis\"/>");
		GateGenericXMLWriter.XMLWriter.newLine();
		GateGenericXMLWriter.XMLWriter.write("</scl:JOLUXComplexWork>");
		GateGenericXMLWriter.XMLWriter.newLine();
		
		// writing the scl resource
		
		GateGenericXMLWriter.XMLWriter.write("<scl:JOLUXLegalResource>");
		GateGenericXMLWriter.XMLWriter.newLine();
		GateGenericXMLWriter.XMLWriter.write("<scl:jolux scl:name=\"memorial\">TO COMPLETE</scl:jolux>");
		GateGenericXMLWriter.XMLWriter.newLine();
		GateGenericXMLWriter.XMLWriter.write("<scl:jolux scl:name=\"typeDocument\">TO COMPLETE</scl:jolux>");
		GateGenericXMLWriter.XMLWriter.newLine();
		GateGenericXMLWriter.XMLWriter.write("<scl:jolux scl:name=\"dateDocument\">TO COMPLETE</scl:jolux>");
		GateGenericXMLWriter.XMLWriter.newLine();
		GateGenericXMLWriter.XMLWriter.write("<scl:jolux scl:name=\"publicationDate\"/>");
		GateGenericXMLWriter.XMLWriter.newLine();
		GateGenericXMLWriter.XMLWriter.write("<scl:jolux scl:name=\"dateEntryInForce\"/>");
		GateGenericXMLWriter.XMLWriter.newLine();
		GateGenericXMLWriter.XMLWriter.write("<scl:jolux scl:name=\"responsibilityOf\"/>");
		GateGenericXMLWriter.XMLWriter.newLine();
		GateGenericXMLWriter.XMLWriter.write("<scl:jolux scl:name=\"subjectLevel1\"/>");
		GateGenericXMLWriter.XMLWriter.newLine();
		GateGenericXMLWriter.XMLWriter.write("</scl:JOLUXLegalResource>");
		GateGenericXMLWriter.XMLWriter.newLine();
		
		GateGenericXMLWriter.XMLWriter.write("</scl:JOLUXWork>");
		GateGenericXMLWriter.XMLWriter.newLine();
		
		// writing the scl expression
		
		GateGenericXMLWriter.XMLWriter.write("<scl:JOLUXExpression>");
		GateGenericXMLWriter.XMLWriter.newLine();
		GateGenericXMLWriter.XMLWriter.write("<scl:jolux scl:name=\"title\">"+Configuration.longTitle+"</scl:jolux>");
		GateGenericXMLWriter.XMLWriter.newLine();
		GateGenericXMLWriter.XMLWriter.write("<scl:jolux scl:name=\"language\">http://publications.europa.eu/resource/authority/language/FRA</scl:jolux>");
		GateGenericXMLWriter.XMLWriter.newLine();
		GateGenericXMLWriter.XMLWriter.write("</scl:JOLUXExpression>");
		GateGenericXMLWriter.XMLWriter.newLine();		
		
		// writing the scl manifestation
		
		GateGenericXMLWriter.XMLWriter.write("<scl:JOLUXManifestation>");
		GateGenericXMLWriter.XMLWriter.newLine();
		GateGenericXMLWriter.XMLWriter.write("<scl:jolux scl:name=\"uriThis\"/>");
		GateGenericXMLWriter.XMLWriter.newLine();
		GateGenericXMLWriter.XMLWriter.write("</scl:JOLUXManifestation>");
		GateGenericXMLWriter.XMLWriter.newLine();
		
		// closing the FRBR part
		GateGenericXMLWriter.XMLWriter.write("</identification>");
		
		// writing the lifecycle
		
		GateGenericXMLWriter.XMLWriter.write("<lifecycle source=\"#\">");
		GateGenericXMLWriter.XMLWriter.newLine();
		GateGenericXMLWriter.XMLWriter.write("<eventRef date=\"TO COMPLETE\" id=\"#\" source=\"#\" type=\"generation\"/>");
		GateGenericXMLWriter.XMLWriter.newLine();
		GateGenericXMLWriter.XMLWriter.write("</lifecycle>");
		GateGenericXMLWriter.XMLWriter.newLine();
		GateGenericXMLWriter.XMLWriter.write("<analysis source=\"#\"/>");
		
		// writing the analyse and references
		
		GateGenericXMLWriter.XMLWriter.newLine();
		
		// begin addition NSA20190109 for handling footnote annotations
		// writing footnotes
		GateGenericXMLWriter.XMLWriter.write(GateAkomaConverter.notesStart);
		GateGenericXMLWriter.XMLWriter.newLine();
		
		writeAkomaFootnotes(documentAnnotations);
		
		GateGenericXMLWriter.XMLWriter.write(GateAkomaConverter.notesEnd);
		GateGenericXMLWriter.XMLWriter.newLine();
		// enf addition NSA20190109 for handling annotations
		
		// closing the meta elements
		GateGenericXMLWriter.XMLWriter.write("</meta>");
		GateGenericXMLWriter.XMLWriter.newLine();
	}
	
	/**
	 * writes the footnote content (in the <meta> part) of the XML file 
	 * @param documentAnnotations the documents annotations. It contains the Footnote_Content annotations
	 * @throws IOException
	 */
	public void writeAkomaFootnotes(AnnotationSet documentAnnotations) throws IOException {
		// getting the footnoteContent annotations
		AnnotationSet footnoteContents = documentAnnotations.get("Footnote_Content");		
		// if any, for each writing the footnote
		if (!footnoteContents.isEmpty()){
			for (Annotation footnote: footnoteContents){
				String id = footnote.getFeatures().get("id").toString();
				String content = footnote.getFeatures().get("content").toString();
				GateGenericXMLWriter.XMLWriter.write(String.format(GateAkomaConverter.note, id, content));
				GateGenericXMLWriter.XMLWriter.newLine();
			}
		}
		else{
			System.out.println("info: no footnote spotted in the text");
		}
	}


	/** 
	 * writes the preamble of the text (SCL miniAPP)
	 * @param preambleText String that contains the Preamble Segment (Be careful, this is the complete String, not the annotation)
	 * @return
	 * @throws IOException
	 */
	public void writeAkomaPreamble(String preambleText) throws IOException{
		// in case of issue, will not do anything - content will be lost
		if (preambleText == null || preambleText.equals("")){return;}
		
		//starting preamble
		GateGenericXMLWriter.XMLWriter.write("<preamble>");
		// treating each line (first is preamble, the middle is considerando, the last line is a formula)
		String [] preambleTable = preambleText.split("\n");
		if (preambleTable.length>=2){
			//managing the first line "Nous Henri, ...."
			String preamble = preambleTable[0];
			GateGenericXMLWriter.XMLWriter.newLine();
			GateGenericXMLWriter.XMLWriter.write("<container name=\"preamble\"><p>" + preamble + "</p></container>"); 
		
			// managing considerando lines
			for (int i = 1; i < preambleTable.length-1; i++){
				GateGenericXMLWriter.XMLWriter.newLine();
				GateGenericXMLWriter.XMLWriter.write("<container name=\"considerando\"><p>" + preambleTable[i] + "</p></container>"); 
			}
			// managing the final formula ("arreteons, ordonnons, "
			String formula = preambleTable[preambleTable.length-1];
			GateGenericXMLWriter.XMLWriter.newLine();
			GateGenericXMLWriter.XMLWriter.write("<formula name =\"\"><p>" + formula + "</p></formula>");
		}
		else {
			GateGenericXMLWriter.XMLWriter.write("<container name=\"\">");
			for (String s: preambleTable){
				GateGenericXMLWriter.XMLWriter.newLine();
				GateGenericXMLWriter.XMLWriter.write("<p>" + s + "</p>");
			}
			GateGenericXMLWriter.XMLWriter.newLine();
			GateGenericXMLWriter.XMLWriter.write("</container>");
		}
		//closing preamble
		GateGenericXMLWriter.XMLWriter.newLine();
		GateGenericXMLWriter.XMLWriter.write("</preamble>");
	}

	
	/**
	 * writes the <body> tag that would contain the legal text itself 
	 * @throws IOException
	 */
	public void writeBodyHead() throws IOException {
		GateGenericXMLWriter.XMLWriter.write(converter.openBody());
		GateGenericXMLWriter.XMLWriter.newLine();
	}

	
	/**
	 * writes the Akoma Ntoso headers (<akoma><meta><identification><preface><preamble><body>)
	 * preamble needs a text to be processed otherwise will not be inserted
	 * @param title the title of the document
	 * @param preamble the text of the preamble to be formatted
	 * @param documentAnnotations the annotation set containing all the annotations of the document (used for footnotes) (added: NSA20190109 for handling footnotes)
	 * @throws IOException
	 */
	public void writeAkomaHeaders(String title, String preamble, AnnotationSet documentAnnotations) throws IOException {
		writeAkomaHeader();
		writeMeta(documentAnnotations); // NSA20190109: adding the annotationSet documentAnnotations for the handling of footnotes   
		writePreface(title);
		writeAkomaPreamble(preamble);
		writeBodyHead();		
	}

	/**
	 * generates the cross reference XML tag from the cross reference string and the eli
	 * @param cr the cross reference
	 * @param eli its URI address
	 * @return a cross reference XML tag
	 */
	public String writeAkomaCrossReference(String cr, String eli){
		String result = String.format(GateAkomaConverter.crossRef,eli, cr);
		//System.out.println("Cross reference formatted as: " + result);
		return result;
	} 	
	

	/**
	 * Transform the content in XML, line by line. Special treatment is called for Cross references prior to the generation of the XML elements for the alinea structure 
	 * @param theInputAS
	 * @param elementSegment
	 * @return
	 */
	public String processText(AnnotationSet inputDocument, Annotation elementSegment, boolean isAmendment) {

		// getting the elem offsets (to align with the CR, so that the text offsets can be normalized with a new string)
		int elementStart = elementSegment.getStartNode().getOffset().intValue();
		int elementEnd = elementSegment.getEndNode().getOffset().intValue();
		// getting the original textual content and put it aside in case of issue
		String originalText;
		
		//if it is from an amendment, extract the substring from the amendment. Otherwise, extract the substring from the whole text.
		if(isAmendment){
			originalText = GateAmendmentWriter.document.substring(elementStart, elementEnd);
		}else{
			originalText = GateGenericXMLWriter.document.substring(elementStart, elementEnd);
		}
		
		String processText = originalText;
		StringBuffer sb;
		// tooling variables instantiation that will change in the loop below while treating cross references
		// initialization for logging purpose in case of crash
		String crTxt = ""; int crStart = 0; int crEnd = 0;

		// TODO don't deal with crossRefs if there are amendments
		//System.out.println("processing element between offset "+ elementStart + " and " + elementEnd + "\n" + originalText);
		try{

			int amendStart;
			int amendEnd;

			String AmendText;
			Annotation amendment;
			AnnotationSet amendments = inputDocument.getContained(elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset()).get("Amendment_Segment");
			List<Annotation> AmenList = new ArrayList<Annotation>(amendments);
			Collections.sort(AmenList, new OffsetComparator());
			
			//remove amendment segment if it is the amendment which is processed
			if(AmenList.size() > 0){
				for(int i=AmenList.size()-1; i>=0 ; i--){
					if(AmenList.get(i).getStartNode().equals(elementSegment.getStartNode()) && AmenList.get(i).getEndNode().equals(elementSegment.getEndNode())){
						AmenList.remove(i);
						System.out.println("Processing amendment without a subdivision element");
					}
				}
			}

			// Don't deal with crossreferences in general. Not only in segment with amendment?
			if(AmenList.size() > 0){
				for(int i=AmenList.size()-1; i>=0 ; i--){
					amendment = AmenList.get(i);
					amendStart = amendment.getStartNode().getOffset().intValue();
					amendEnd = amendment.getEndNode().getOffset().intValue();
					AmendText = GateGenericXMLWriter.document.substring(amendStart, amendEnd);
					
					//remove amendments from the text and write an ID to be replaced after proccessed
					sb = new StringBuffer(processText);
					sb.replace(amendStart - elementStart, amendEnd - elementStart, "##"+(idAmendment+i)+"##");

					processText = sb.toString();
//					System.out.println("Size "+AmenList.size()+"AMENDMENT ALINEA: "+AmendText.substring(0, 30)+"...");
//					System.out.println(processText);
				}	
				idAmendment += AmenList.size();
			}else{	//either you have 		
				
				// get the cross references in the alinea
				AnnotationSet myRefs = inputDocument.getContained(elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset()).get("CrossReferences_ref");
				List<Annotation> refsList = new ArrayList<Annotation>(myRefs);
				Collections.sort(refsList, new OffsetComparator());
				// Look at the CR within the alinea to replace them with their resolution
				//bottom-up approach for changes so that the CR offsets are not affected
				for (int i = refsList.size()-1; i>=0; i--){
					Annotation myRef = refsList.get(i);
					// capturing the text of the cross reference and its offsets
					crStart = myRef.getStartNode().getOffset().intValue();
					crEnd = myRef.getEndNode().getOffset().intValue();
					crTxt = GateGenericXMLWriter.document.substring(crStart, crEnd);
					//System.out.println("processing cross-reference between offset "+ crStart + " and " + crEnd + " -- " + crTxt);			
					// building the cross reference resolution via the "eli" features in the cross reference
					String eli = (String) myRef.getFeatures().get("eli");
					String XMLRef =   writeAkomaCrossReference(crTxt, Configuration.serverRoot+eli);
					sb = new StringBuffer(processText);
					sb.replace(crStart - elementStart, crEnd - elementStart, XMLRef);
					processText = sb.toString();
					//processText = processText.trim();
				}
			}
				// at this point al CRs have been resolved
				//Defining the lists for letter, numbers, subletters, dashes via preprocessing processing (incuding cleaning to shape up the XML) and ifnal post processing
				String processElement = preProcessListElement(processText);
				processText = processListElements(processElement);
				
				//process amendment and insert them in the text
			if(AmenList.size()>0){
				
				ArrayList<Integer> startPatternList = new ArrayList<Integer>();
				ArrayList<Integer> endPatternList = new ArrayList<Integer>();
				ArrayList<Integer> idPatternList = new ArrayList<Integer>();
				Pattern amendmentPattern = Pattern.compile("(##([0-9]{1,3})##)");
				Matcher mAP = amendmentPattern.matcher(processText);
				while(mAP.find()){
					System.out.println("DETECTION PATTERN"+mAP.group(2)+" "+mAP.start()+" "+mAP.end());
					startPatternList.add(mAP.start());
					endPatternList.add(mAP.end());
					idPatternList.add(Integer.parseInt(mAP.group(2)));

				}

				for(int i=idPatternList.size()-1; i>=0; i--){
					sb = new StringBuffer(processText);
					sb.replace(startPatternList.get(i), endPatternList.get(i), processAmendment(idPatternList.get(i)));
					processText = sb.toString();
				}
			}	
			processText = postProcessElement(processText);
			return processText;
		}
		catch (Exception e){
			// degradation mechanism return the original element
			System.err.println("error while processing the element: " + originalText);
			System.err.println("current cross reference:" + crTxt + " at offsets: "+ crStart + "-" + crEnd);
			e.printStackTrace();
			return originalText;
		}
	}
	
	/**
	 * will preprocess the alinea segment, which a re in put in a single line to put them back into multiple lines
	 * @param s the textual content of the alinea segment to process
	 * @return
	 */
	public String preProcessListElement(String s){
		// finding numbers, letters and dash items to put in differents lines that will be processed by processAlineas
		// There in an intermediary step that inserts tags "@#" with a check then that these tags are not actually false positives and eventually tries to remove false positives
		// the intermediary tags is then changed into a carriage return and the string is finally cleaned from extra carriage returns
		// patterns are character-sensitive. Change only on purpose

		Pattern patternNumber = Pattern.compile("(\\n| )([0-9]{1,2})(\\)|°|\\.)( |	)(.*)"); //1) 1° 1. up to 99
		Pattern patternLetter = Pattern.compile("(\\n| )(\\()?([a-z]+)(\\)|°)( |	)(.*)");
		Pattern patternSubLetter = Pattern.compile("(\\n| )(\\()?(i|ii|iii|iv|v|vi|vii|viii|ix|x|xi|xii|xiii|xiv|xv|xvi|xvii|xviii|xix|xx)(\\)|°|\\.)( |	)(.*)");
		Pattern patternDash = Pattern.compile("(\\n| )(-|–|–)( |	)(.*)");
			
		Matcher mDash = patternDash.matcher(s);
		Matcher mNumber = patternNumber.matcher(s);
		Matcher mLetter = patternLetter.matcher(s);
		Matcher mSubLetter = patternSubLetter.matcher(s);
		
		while (mNumber.find()) {s=s.replace(mNumber.group(0), "#@"+ mNumber.group(0) + "#@");}	
		while (mLetter.find()) {s=s.replace(mLetter.group(0), "#@"+ mLetter.group(0) + "#@");}
		while (mSubLetter.find()) {s=s.replace(mSubLetter.group(0), "#@"+ mSubLetter.group(0) + "#@");			}
		while (mDash.find()) {s=s.replace(mDash.group(0), "#@"+ mDash.group(0)+ "#@");}
		//postprocessing - removing extra carriage return (need to be done twice, don't know why)
		s = s.replace("#@"+"#@", "#@");
		s = s.replace("#@"+"#@", "#@");

		Pattern patternFalsePositive = Pattern.compile("((,|point|points|sous|au|aux|numéro|numéros|lettre|lettres|alinéa|alinéas|et|ou|à)( )?)(#@)");
		Matcher mFalsePositive = patternFalsePositive.matcher(s);
		while(mFalsePositive.find()){
			//System.out.println(mFalsePositive.group(0) + "  " + mFalsePositive.group(1));
			s = s.replace(mFalsePositive.group(0), mFalsePositive.group(1));
		}
		s = s.replace("#@", System.getProperty("line.separator"));
		//removing extra carriage returns - done twice
		s =s.replace(System.getProperty("line.separator")+System.getProperty("line.separator"), System.getProperty("line.separator"));
		s =s.replace(System.getProperty("line.separator")+System.getProperty("line.separator"), System.getProperty("line.separator"));
		return s;	
		}
	
	
	/**
	 * will process the textual material and split it into <p> elements and <ul><ol><li> elements for the nested elements
	 * @param s
	 * @return
	 */
	public String processListElements(String s) {
		// initialization
		String res = "";
		String separators = System.getProperty("line.separator");
		boolean notationChanged = false;
		boolean isDash = false;
		boolean isNumber = false;
		boolean isLetter = false;
		boolean isSubLetter = false;
		boolean started = false;
		int listEncountered = 0; // will indicate the first list that is met in the alinea
		String newType = "";
		LinkedList<String> memory = new LinkedList<String>();
		Pattern patternNumber = Pattern.compile("(([0-9]{1,2})(\\)|°|\\.)( |	)(.*))");
		Pattern patternLetter = Pattern.compile("((\\()?([a-z]+)(\\)|°|\\.)( |	|\\[)(.*))");
		Pattern patternDash = Pattern.compile("((-|–|–)(.*))");
		Pattern patternSubLetter = Pattern.compile("((\\()?(i|ii|iii|iv|v|vi|vii|viii|ix|x|xi|xii|xiii|xiv|xv|xvi|xvii|xviii|xix|xx)(\\)|°|\\.)( |	)(.*))");

		//
		String[] tab = s.split(separators);
		for (String line : tab) {
			Matcher mDash = patternDash.matcher(line);
			Matcher mNumber = patternNumber.matcher(line);
			Matcher mLetter = patternLetter.matcher(line);
			Matcher mSubLetter = patternSubLetter.matcher(line);
			// determining the type of the line
			isNumber = mNumber.matches();
			isLetter = mLetter.matches();
			isDash = mDash.matches();
			isSubLetter = mSubLetter.matches();
			// normal line (without list)
			if (!isNumber && !isLetter && !isDash && !isSubLetter) {
				res = res + line + separators;
			}
			// list detected
			else {
				// what is the type detected
				if (isDash) {
					newType = "dash";
				} else if (isNumber) {
					newType = "number";
				} else if (isLetter) {
					newType = "letter";
				} 
				else if (isSubLetter) {
					newType = "subLetter";
				}
				
				// has the type changed?
				if (!memory.isEmpty()) {
					notationChanged = !(memory.peek().equals(newType));
				}
				// if list to get closed? (going from a sublevel back to a upper level)
				if (notationChanged) {
					// sublevel or back to a upper level - computing the difference of level and eventually the numbers of braces to close
					int pos = -1;
					for (int i = 0; i < memory.size(); i++) {
						if (memory.get(i).equals(newType)) {
							pos = i;
						}
					}
					// Close stuff
					for (int i = 0; i < pos; i++) {
						String typeToClose = memory.get(i);
						memory.remove(i);
						pos--;
						i--;
						if (typeToClose.equals("dash")) {
							res = res + "</ul>" + System.getProperty("line.separator");
						} else {
							res = res + "</ol>" + System.getProperty("line.separator");
						}
					}
				}
				// opening a list - the first treatment is to close the </p> before going for lists
				if (started == false || (started && notationChanged)) {
					if (!memory.contains(newType)) {
						listEncountered++;
						if (listEncountered==1){ // is done only once when meeting the first list
							res = res + "</p>" + separators;
						}

						if (isDash) {
							memory.push("dash");
							if (listEncountered==1){
								res = res + "<ul style=\"\" >" + System.getProperty("line.separator");
							}
							else{
								res = res + "<li>" + "<ul style=\"\" >" + System.getProperty("line.separator");
							}
								
						} else {
							if (isNumber) { memory.push("number");}
							if (isLetter) { memory.push("letter");}
							if (isSubLetter) { memory.push("subLetter");}
							if (listEncountered==1){
								res = res + "<ol style=\"\">" + System.getProperty("line.separator");
							}
							else {res = res +"<li>" +"<ol style=\"\">" + System.getProperty("line.separator");
							}
						}
					}
					// we start a list (or a new sublevel)
					started = true;
				}
				// Writing the list item itself + the matched sequence
				if (isNumber) {res = res + line.replace(mNumber.group(0), "<li>" + mNumber.group(0) + "</li>" + separators);}
				if (isLetter) {res = res + line.replace(mLetter.group(0), "<li>" + mLetter.group(0) + "</li>" + separators);}
				if (isSubLetter) {res = res + line.replace(mSubLetter.group(0), "<li>" + mSubLetter.group(0) + "</li>" + separators);}
				if (isDash) {res = res + line.replace(mDash.group(0), "<li>" + mDash.group(0) + "</li>" + separators);}
				// here we go for the next line
				notationChanged = false;
			}
		}
		// closing the remaining item Lists that have been met
		for (int i = 0; i < memory.size(); i++) {
			String typeToClose = memory.get(i);
			if (typeToClose.equals("dash")) {
				res = res + "</ul>" + separators;
			} else {
				res = res + "</ol>" + separators;
			}
		}
		// closing the imbricated lists
		res = res.replace("</ul>" + separators + "<li>", "</ul>" + separators + "</li>" + separators + "<li>");
		res = res.replace("</ol>" + separators + "<li>", "</ol>" + separators + "</li>" + separators + "<li>");
		res = res.replace("</ol>" + separators + "</ol>", "</ol>"  + "</li>" + separators + "</ol>" );
		res = res.replace("</ol>" + separators + "</ul>", "</ol>"  + "</li>" + separators + "</ul>" );
		res = res.replace("</ul>" + separators + "</ul>", "</ul>"  + "</li>" + separators + "</ul>" );
		res = res.replace("</ul>" + separators + "</ol>", "</ul>"  + "</li>" + separators + "</ol>" );
		//done twice
		res = res.replace("</ol>" + separators + "</ol>", "</ol>"  + "</li>" + separators + "</ol>" );
		res = res.replace("</ol>" + separators + "</ul>", "</ol>"  + "</li>" + separators + "</ul>" );
		res = res.replace("</ul>" + separators + "</ul>", "</ul>"  + "</li>" + separators + "</ul>" );
		res = res.replace("</ul>" + separators + "</ol>", "</ul>"  + "</li>" + separators + "</ol>" );

		
		//return res + an opening <p> that will be handled by the calling method (closed at the end of processCRText)
		if (listEncountered > 0) {return res + "<p>";}
		else {return res;}
	}
		
	/**
	 *  post-processing that was manually ad externally done previously 
	 * @param s the alinea String to process
	 * @return post-process line with some treatment and layout 
	 */
	public String postProcessElement(String s){
//		System.out.println("TEXT TO PROCESS "+s);
		String result;
		String missingAlineaPattern = "."+System.getProperty("line.separator");
		String flawedOrderedListPattern = "</ol>" + System.getProperty("line.separator") + "</content></alinea>" + System.getProperty("line.separator") + "<alinea><content>" + System.getProperty("line.separator") + "<ol style=\"\">";
		String flawedUnorderedListPattern = "</ul>" + System.getProperty("line.separator") + "</content></alinea>" + System.getProperty("line.separator") + "<alinea><content>" + System.getProperty("line.separator") + "<ul style=\"\">";
		String emptyAlineas3 = "<alinea>" + System.getProperty("line.separator") + "<content><p></p>" + System.getProperty("line.separator") + "</content></alinea>";
		String emptyAlineas2 = "<alinea><content><p></p>( )?\n( )?</content></alinea>";
		String emptyAlineas = "<alinea><content>" + System.getProperty("line.separator") + "</content></alinea>";
		String strangeBraces = "<alinea><content><p></content></alinea>";


		
		//TODO clean empty alineas ===> not here
		
		result = s.replace(missingAlineaPattern, ".</p>"+System.getProperty("line.separator")+"</content></alinea>"+System.getProperty("line.separator")+"<alinea><content><p>");
		result = result.replace(flawedOrderedListPattern, "");
		result = result.replace(flawedUnorderedListPattern, "");
		// putting back </p> at the end of the previous line
//		result = result.replace(System.getProperty("line.separator")+"</p>", "</p>");
		// removing empty alineas -- another one will be required externally
		//System.out.println(result);
		result = result.replaceAll(emptyAlineas3,"");
		result = result.replaceAll(emptyAlineas2,"");
		result = result.replaceAll(emptyAlineas, "");
		result = result.replaceAll(strangeBraces,"");

		//removing extra line breaks // need to be done twice or three tomes to remove extra extra
		result = result.replaceAll(">" + System.getProperty("line.separator") + System.getProperty("line.separator") + "<", ">" + System.getProperty("line.separator") + "<");
		result = result.replaceAll(">" + System.getProperty("line.separator") + System.getProperty("line.separator") + "<", ">" + System.getProperty("line.separator") + "<");
		return result;
	}
	
	
	/**
	 * Generate the xml process for the amendments only
	 */
	private String processAmendment(int idAmendment){
		System.out.println("get "+idAmendment+"th amendment");
		return GateGenericXMLWriter.amendmentContentList.get(idAmendment);	
	}
			
}
