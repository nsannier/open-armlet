package gateStandAlone;

import gate.CorpusController;
import gate.Factory;
import gate.LanguageAnalyser;
import gate.creole.ResourceInstantiationException;
import gate.creole.SerialAnalyserController;
import gate.persist.PersistenceException;
import gate.util.persistence.PersistenceManager;
import gateStandAlone.ConfigTool;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collections;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import japescriptgen.DocumentNode;
import japescriptgen.DocumentStructure;

/**	Copyright (c) {2014-2019}, University of Luxembourg, Interdisciplinary Centre for Security, Reliability and Trust (SnT)
* 	Nicolas Sannier (sannier@svv.lu),
*	Morayo Adedjouma (morayoade@gmail.com),
*	Virgil Tassan (tassan-zanin-caser@svv.lu),
*	Wei Dou (dou@svv.lu),
*	Mehrdad Sabetzadeh (sabetzadeh@svv.lu),
*	Lionel Briand (briand@svv.lu),
*/

public class GateScriptManager {

	/**
	 * Maps json structure and java object
	 */
    public static final ObjectMapper objectMapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
    
    /**
     * the strucuture of the document
     */
    public static DocumentStructure documentStructure;
    
    
    /**
	 *  defines the .jape extension filter for importing only jape scripts
	 */
	FilenameFilter japeFilter = new FilenameFilter() {
		public boolean accept(File dir, String name) {
			String lowercaseName = name.toLowerCase();
			if (lowercaseName.endsWith(".jape")) {
				return true;
			} else {
				return false;
			}
		}
	};
	
	
	/**
	 * 
	 * @param app the path of the gate application (.gapp) to load
	 * @throws PersistenceException
	 * @throws ResourceInstantiationException
	 * @throws IOException
	 */
	public CorpusController loadGateApplication(String app) throws PersistenceException, ResourceInstantiationException, IOException{
		System.out.println("Please wait...");
		System.out.println("*** Loading the application scripts ***");
		String appPath = new File(app).getAbsolutePath();
		System.out.println("Loading: " + appPath);
		return (CorpusController) PersistenceManager.loadObjectFromFile(new File(appPath));
	}
	
	
	/**
	 * reads structure.json file and convert it into an ArrayList
	 * @param structureJsonFile the configuration file stating the structural organization of the text
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	public ArrayList<String> importStructure(File structureJsonFile) throws JsonParseException, JsonMappingException, IOException{
		assert(structureJsonFile.exists());
		documentStructure = objectMapper.readValue(structureJsonFile, DocumentStructure.class);
		DocumentNode root = documentStructure.highLevelDivisionElements;
		DocumentNode basicNode = documentStructure.basicElements;
		DocumentNode bottomSubDivisionNode = documentStructure.subDivisionElements;

		ArrayList<String>elementList = new ArrayList<String>();
		DocumentNode node = root;
		// putting the root element in the element list
		 while (node != null) {
			 elementList.add(node.name);
			 node=node.child;
		 }
		 elementList.add(basicNode.name);
		 node = bottomSubDivisionNode;
		 while (node != null) {
			 elementList.add(node.name);
			 node=node.child;
		 }		 
		 return elementList;
	}
	
	/**
	 * revert the list for the structural elements without changing the initial list (the revert method changes the list itself)
	 * @param list the list to be inverted
	 * @return
	 */
	public ArrayList<String>invertStructure(ArrayList<String> list){
		ArrayList<String> result =  list;
		Collections.reverse(result);
		return result;
	}
	
	
	/**
	 * Creates a Gate Application from generated jape scripts and structure of the document
	 * @param appName name of the Gate Application that will be created
	 * @param folder the path to the folder containing the jape scripts to import
	 * @param structureList the (ordered) list of structural elements in the text (can be null or empty)
	 * @return the gate application. it needs to be further completed with the corpus it would be run over
	 * @throws ResourceInstantiationException
	 * @throws MalformedURLException
	 */
				

	public SerialAnalyserController createHeaderAppFromScripts(String appName, String folder, ArrayList<String> structureList) throws ResourceInstantiationException, MalformedURLException{
		System.out.println("APPNAME : "+appName);
		SerialAnalyserController myApp = (SerialAnalyserController) Factory.createResource("gate.creole.SerialAnalyserController", Factory.newFeatureMap(), Factory.newFeatureMap(), appName);
		// getting the jape scripts in the folder
        File headerdir = new File(folder);       
        File[] headerScripts = headerdir.listFiles(japeFilter);  
        ArrayList<String> headerList = new ArrayList<String>(structureList);
        headerList.add(headerList.get(0));
        headerList.remove(0);
        // importing the file in the order defined by structureList
        if (headerList != null && headerList.size()>0){
        	for(String element:headerList){
        		for (File script:headerScripts){
        			String scriptName = script.getName();
            		scriptName = scriptName.replace(".jape","");
            		scriptName = scriptName.replace("Head", "");
            		scriptName = scriptName.replaceAll("CustomMark", "");
                	if (scriptName.toLowerCase().equals(element.toLowerCase())){
                		System.out.println("importing the script" + script.getName().replace(".jape",""));
        				LanguageAnalyser myScript = importJapeScript(script.getAbsolutePath(), script.getName());
                		myApp.add(myScript);
        			}
        		}
        	}
        }
        else{ // no particular order has been defined
        	// for each pathname in pathname array
        	for(File script:headerScripts) {
        		// prints file and directory paths
        		String scriptName = script.getName();
        		scriptName = scriptName.replace(".jape","");
        		System.out.println("importing the script" + scriptName);
        		LanguageAnalyser myScript = importJapeScript(script.getAbsolutePath(), script.getName());
        		myApp.add(myScript);
        	}
        }
        //Add cleaning Header Rules
		File cleaningFile = new File(ConfigTool.customCleaningExludedRuleFile);
		LanguageAnalyser cleaningRulesScript = importJapeScript(cleaningFile.getAbsolutePath(), cleaningFile.getName());
		myApp.add(cleaningRulesScript);
		
        //Add write Info User
		File writeInfoRuleFile = new File(ConfigTool.customWriteMatchedRule);
		LanguageAnalyser infoRuleScript = importJapeScript(writeInfoRuleFile.getAbsolutePath(), writeInfoRuleFile.getName());
		myApp.add(infoRuleScript);
        
	
		
		return myApp;
	}
	

	public SerialAnalyserController createSegmentAppFromScripts(String appName, String folder, ArrayList<String> structureList) throws ResourceInstantiationException, MalformedURLException{
		System.out.println("APPNAME : "+appName);
		SerialAnalyserController myApp = (SerialAnalyserController) Factory.createResource("gate.creole.SerialAnalyserController", Factory.newFeatureMap(), Factory.newFeatureMap(), appName);
		        
		//add Amendment Segment Scripts
		File amendmentFile = new File(ConfigTool.customSegmentAmendmentFile);
		LanguageAnalyser amendmentScript = importJapeScript(amendmentFile.getAbsolutePath(), amendmentFile.getName());
		myApp.add(amendmentScript);
        //add Preamble Segment Scripts
        File preambleFile = new File(folder+ConfigTool.pathJoiner+ConfigTool.customSegmentPreambleFile);
        LanguageAnalyser preambleScript = importJapeScript(preambleFile.getAbsolutePath(), preambleFile.getName());
		myApp.add(preambleScript);

        File segmentdir = new File(folder);
        File[] segmentScripts = segmentdir.listFiles(japeFilter);
     // importing the file in the order defined by structureList
        if (structureList != null && structureList.size()>0){
        	for(String element:structureList){
        		for (File script:segmentScripts){
        			String scriptName = script.getName();
            		scriptName = scriptName.replace(".jape","");
            		scriptName = scriptName.replace("Segment", "");
            		scriptName = scriptName.replaceAll("CustomMark", "");
                	if (scriptName.toLowerCase().equals(element.toLowerCase())){
                		System.out.println("importing the script" + script.getName().replace(".jape",""));
        				LanguageAnalyser myScript = importJapeScript(script.getAbsolutePath(), script.getName());
                		myApp.add(myScript);
        			}
        		}
        	}
        }
        else{ // no particular order has been defined
        	// for each pathname in pathname array
        	for(File script:segmentScripts) {
        		// prints file and directory paths
        		String scriptName = script.getName();
        		scriptName = scriptName.replace(".jape","");
        		System.out.println("importing the script" + scriptName);
        		LanguageAnalyser myScript = importJapeScript(script.getAbsolutePath(), script.getName());
        		myApp.add(myScript);
        	}
        }		
		
		return myApp;
	}
		
	/**
	 * 
	 * @param pathFile relative path of the jape script
	 * @param name name of the jape script as visible in Gate
	 * @return the processing resource to be added to the execution pipeline
	 * @throws ResourceInstantiationException
	 * @throws MalformedURLException
	 */
	public LanguageAnalyser importJapeScript(String pathFile, String name) throws ResourceInstantiationException, MalformedURLException{
		//System.out.println(file+" "+name);
//		LanguageAnalyser myJapeScript = (LanguageAnalyser)gate.Factory.createResource("gate.creole.Transducer", gate.Utils.featureMap("grammarURL", new File(file).toURI().toURL(),"encoding", "UTF-8"));
		LanguageAnalyser myJapeScript = (LanguageAnalyser)gate.Factory.createResource("gate.jape.plus.Transducer", gate.Utils.featureMap("grammarURL", new File(pathFile).toURI().toURL(),"encoding", "UTF-8"));
		myJapeScript.setName(name);
		return myJapeScript;
	}
	
	
}
