package gateStandAlone;

import gate.Annotation;
import gate.AnnotationSet;
import gate.util.OffsetComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**	Copyright (c) {2014-2019}, University of Luxembourg, Interdisciplinary Centre for Security, Reliability and Trust (SnT)
* 	Nicolas Sannier (sannier@svv.lu),
*	Morayo Adedjouma (morayoade@gmail.com),
*	Virgil Tassan (tassan-zanin-caser@svv.lu),
*	Wei Dou (dou@svv.lu),
*	Mehrdad Sabetzadeh (sabetzadeh@svv.lu),
*	Lionel Briand (briand@svv.lu),
*/

public class GateCrossreferences {

	//String rootUri= Configuration.rootELI;


	public GateCrossreferences() {
		super();
	}

	/**
	 * tests if the given string contains a full written date
	 * @param cr the input string. It is a cross reference
	 * @return true if it contains a date, false otherwise
	 */
	public Boolean containsFullDate (String cr){
		String[] month= {"janvier", "février", "fevrier", "mars", "avril", "mai", "juin", "juillet", "août", "aout", "septembre", "octobre", "novembre", "décembre", "decembre"};
		for(int i = 1; i <= month.length; i++ ){
			if(cr.toLowerCase().contains(month[i])){
				return true; 
			}
		}
		return false;
	}
	
	
	/**
	 * resolve all internal CR that do not contain yet the feature ELI resolution mean adding the feature ELI with the required value
	 * @param inputDocument : set of the annotations in the parsed document
	 * @param myCR : the CR expression to be resolved
	 * @param myDoc: the full document to parse
	 */
	public void resolveInternalCR(AnnotationSet inputDocument, Annotation myCR, String myDoc) {
		// verbatimCR is the original verbatim from the text
		String verbatimCR = myDoc.substring(myCR.getStartNode().getOffset().intValue(), myCR.getEndNode().getOffset().intValue());
//		System.out.println("article level: "+ verbatimCR+", "+verbatimCR.replace("L.", ""));
			
		//CR contains at least an article reference
		if(!inputDocument.getContained(myCR.getStartNode().getOffset(), myCR.getEndNode().getOffset()).get("Article_ref").isEmpty()){
			//get from the first Article_ref contains in the CR, its feature Article

			AnnotationSet articleRefs = inputDocument.getContained(myCR.getStartNode().getOffset(), myCR.getEndNode().getOffset()).get("Article_ref");
			List<Annotation> articleList = new ArrayList<Annotation>(articleRefs);
			Collections.sort(articleList, new OffsetComparator());
			if((articleList.get(0)).getFeatures().get("Article") != null){
				String article_id= (articleList.get(0)).getFeatures().get("Article").toString();
				// The feature ELI refers to the first??? article cited in the annotation
				myCR.getFeatures().put("eli", Configuration.rootELI + "/" + ConfigTool.basicEli + article_id);
			}
			return;
		}
		
		// CR does not contains article references
		if(inputDocument.getContained(myCR.getStartNode().getOffset(), myCR.getEndNode().getOffset()).get("Article_ref").isEmpty()){
			// Reference to upper level --> resolve to code level
			GateLuxembourg.defineHighLevelDivisionsList();
			//System.out.println("GateLuxembourg.HighLevelDivisions "+ GateLuxembourg.HighLevelDivisions.toString());
			for (String s: GateLuxembourg.HighLevelDivisions){
				if(verbatimCR.toLowerCase().contains(s.toLowerCase())) {
					//System.out.println("Cross references related to high level division: "+ verbatimCR);
					// The feature ELI refers to the higher level division in the text
					myCR.getFeatures().put("eli", Configuration.rootELI); 
					return;
				}
			}
			// Reference to lower level --> tag to article level in which the CR is contained
			// get the article number that contains the CR from its feature Article_id
			//System.out.println("Cross references related to subdivision: "+ verbatimCR);
			if(myCR.getFeatures().get("Article_id")!=null){
			// The feature ELI refers to the article level in which the CR is contained
				myCR.getFeatures().put("eli", Configuration.rootELI + "/" + ConfigTool.basicEli + myCR.getFeatures().get("Article_id").toString() );
			}
			// last case: add an empty feature ELI
			else{
				myCR.getFeatures().put("eli","");
			}
		}
	}

	/**
	 * resolve all external CR taht do not contain yet the feature ELI resolution mean adding the feature ELI with the required value
	 * @param inputDocument : set of the annotations in the parsed document
	 * @param myCR : the CR expression to be resolved
	 * @param myDoc: the full document to parse
	 */

	public void resolveExternalCR(AnnotationSet inputDocument, Annotation myCR, String myDoc) {
		String codeName = "", dateStr = "";
		// verbatimCR is the original verbatim from the text
		String verbatimCR = myDoc.substring(myCR.getStartNode().getOffset().intValue(), myCR.getEndNode().getOffset().intValue());
		
		// references a code
		if (!inputDocument.get("Code_ref",	myCR.getStartNode().getOffset(), myCR.getEndNode().getOffset()).isEmpty()) {
			// get the annotation code_ref in myCR
			Annotation annot = (inputDocument.get("Code_ref", myCR.getStartNode().getOffset(), myCR.getEndNode().getOffset())).iterator().next();
			codeName = myDoc.substring(annot.getStartNode().getOffset().intValue(), annot.getEndNode().getOffset().intValue());
			codeName = GateLuxembourg.formatCodeRecueilELI(codeName);
			codeName = codeName.trim().toLowerCase().replace(" ", "/");
			// ELI feature refers to the cited code
			myCR.getFeatures().put("eli", codeName);
		}
		// get the tag for the loi with Date, otherwise the reference was already classified as genericCR in the jape script
		else if (verbatimCR.toLowerCase().contains("loi") || verbatimCR.contains("L.") || verbatimCR.toLowerCase().contains("l.")) {
			// get the date and write it with the anti-slash for the ELI feature
			dateStr = returnDate(verbatimCR.toLowerCase());
			myCR.getFeatures().put("eli", GateLuxembourg.prefix+"/loi/"+formatDate(dateStr));
		}
		// get the tag for the arrete with Date.
		else if (verbatimCR.toLowerCase().contains("arr.") || verbatimCR.toLowerCase().contains("arrêté")) {
			// get the date and write it with the anti-slash for the ELI feature	
			dateStr = verbatimCR.toLowerCase();	
			dateStr = returnDate(dateStr);
			// add eli for arr. without grand-ducal and ministeriel
			if (verbatimCR.toLowerCase().contains("grand-ducal") || verbatimCR.toLowerCase().contains("grand-ducaux")
					|| verbatimCR.toLowerCase().contains("g.-d.") || verbatimCR.toLowerCase().contains("gr.-d.")) {
				myCR.getFeatures().put("eli", GateLuxembourg.prefix+"/agd/"+formatDate(dateStr)); 	
			}
			else if (verbatimCR.toLowerCase().contains("ministeriel") || verbatimCR.toLowerCase().contains("ministériel")) {
				myCR.getFeatures().put("eli", GateLuxembourg.prefix+"/amin/"+formatDate(dateStr)); 
			}

		}
		//get the tag for reglement start with r.
		else if (verbatimCR.substring(0, 2).toLowerCase().contains("r.")){
			dateStr = returnDate(verbatimCR.toLowerCase());
			myCR.getFeatures().put("eli", GateLuxembourg.prefix+"/rgd|rmin|agd/"+formatDate(dateStr));
		}
		// get the tag for the reglement with Date.
		else if (verbatimCR.toLowerCase().contains("règlement") || verbatimCR.contains("Règl.")) {
			if (verbatimCR.toLowerCase().contains("grand-ducal") || verbatimCR.toLowerCase().contains("grand-ducaux")
					|| verbatimCR.toLowerCase().contains("g.-d.") || verbatimCR.toLowerCase().contains("gr.-d.")) {
				// get the date and write it with the anti-slash for the ELI feature		
				dateStr = returnDate(verbatimCR.toLowerCase());
				myCR.getFeatures().put("eli", GateLuxembourg.prefix+"/rgd/"+formatDate(dateStr));
				return;
			}
			// it is an EU text: get the date and write it with the anti-slash for the ELI feature + jo if applicable
			if(!inputDocument.get("Ref_ref", myCR.getStartNode().getOffset(), myCR.getEndNode().getOffset()).isEmpty()){
				Annotation annot = (inputDocument.get("Ref_ref", myCR.getStartNode().getOffset(), myCR.getEndNode().getOffset())).iterator().next();
				myCR.getFeatures().put("eli", "eli/reg_ue/"+ annot.getFeatures().get("num").toString() + "/jo");
				return;
			}
		}
		// get the tag for the directive with Date.It is an EU text
		else if (verbatimCR.toLowerCase().contains("directive")) {
			//means it is an EU text: get the date and write it with the anti-slash for the ELI feature + jo if applicable
			if(!inputDocument.get("Ref_ref", myCR.getStartNode().getOffset(), myCR.getEndNode().getOffset()).isEmpty()){
				Annotation annot = (inputDocument.get("Ref_ref", myCR.getStartNode().getOffset(), myCR.getEndNode().getOffset())).iterator().next();
				myCR.getFeatures().put("eli",  "eli/dir_ue/"+ annot.getFeatures().get("num").toString() + "/jo");
				return;
			}		
		}
		// last case: add an empty text for the ELI feature
		else {
			myCR.getFeatures().put("eli", ""); 
		}
	}
	
	 /**
	  * extracts from a given cross reference a date substring 
	  * @param cr the input string. It is a cross reference
	  * @return the substring corresponding to a date from the given string
	  */
	//get the substring corresponding to a Date from the CR verbatim
	public static String returnDate(String cr){
		//replace insecable space by normal space
		cr = cr.replaceAll(" "," ");
		System.out.println("CROSS REF DATE :"+cr);
		Matcher m;
		String s=cr.toLowerCase().trim();
		Pattern p = Pattern.compile("(.*) (\\d+)(er)? (janvier|février|fevrier|mars|avril|mai|juin|juillet|août|aout|septembre|octobre|novembre|décembre|decembre) (\\d+)(.*)");
		Pattern p2 = Pattern.compile("(.+) (\\d+)(.)(\\d+)(.)(\\d+)(.*)");
		Pattern p3 = Pattern.compile("(loi du )(\\d+)(er)? (janvier|février|fevrier|mars|avril|mai|juin|juillet|août|aout|septembre|octobre|novembre|décembre|decembre) (\\d+)(.*)");
		m = p.matcher(s);
		String result = "";
		if (m.matches()){
			result = m.group(2)+"/"+m.group(4)+"/"+m.group(5);
			return result;
		}
		m = p2.matcher(s);
		if (m.matches()){
			result = m.group(2)+"/"+m.group(4)+"/"+m.group(6);
			return result;
		}
		m = p3.matcher(s);
		if (m.matches()){
			result = m.group(2)+"/"+m.group(4)+"/"+m.group(6);
			return result;
		}
		else {
			System.err.println("issue while simplyfing " + cr);
			return cr;
		}
	}
 
	// rewrite the date as XX/XX/XX where XX is a number
	/**
	 * converts a date expression as a DD/MM/YY date
	 * @param date a string which represents a date
	 * @return the date as the new format
	 */
	public String formatDate(String date) {
		String[] tmp = date.split("/");
		tmp[0] = tmp[0].replace("er", "").trim();
		
		if (Integer.parseInt(tmp[0]) < 10) {
			tmp[0] = "0" + tmp[0];
		}
		// if the month is written in letter
		tmp[1] = tmp[1].replace("janvier", "01");
		tmp[1] = tmp[1].replace("fevrier", "02");
		tmp[1] = tmp[1].replace("mars", "03");
		tmp[1] = tmp[1].replace("avril", "04");
		tmp[1] = tmp[1].replace("mai", "05");
		tmp[1] = tmp[1].replace("juin", "06");
		tmp[1] = tmp[1].replace("juillet", "07");
		tmp[1] = tmp[1].replace("aout", "08");
		tmp[1] = tmp[1].replace("septembre", "09");
		tmp[1] = tmp[1].replace("octobre", "10");
		tmp[1] = tmp[1].replace("novembre", "11");
		tmp[1] = tmp[1].replace("decembre", "12");
		tmp[1] = tmp[1].replace("février", "02");
		tmp[1] = tmp[1].replace("décembre", "12");
		tmp[1] = tmp[1].replace("août", "08");
		// if the numnber of digit of the month is not equals 2
		if (Integer.parseInt(tmp[1]) < 10 && tmp[1].length()<2) {
			tmp[1] = "0" + tmp[1];
		}
		String correctDate = tmp[2] + "/" + tmp[1] + "/" + tmp[0];
		return correctDate;
	}

}	
