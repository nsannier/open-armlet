package gateStandAlone;

import java.util.ArrayList;

/**	Copyright (c) {2014-2019}, University of Luxembourg, Interdisciplinary Centre for Security, Reliability and Trust (SnT)
* 	Nicolas Sannier (sannier@svv.lu),
*	Morayo Adedjouma (morayoade@gmail.com),
*	Virgil Tassan (tassan-zanin-caser@svv.lu),
*	Wei Dou (dou@svv.lu),
*	Mehrdad Sabetzadeh (sabetzadeh@svv.lu),
*	Lionel Briand (briand@svv.lu),
*/

public class GateLuxembourg {

	static String prefix = "eli/etat/leg";
	static ArrayList<String> DefaulthighLevelDivisions = new ArrayList<String>();
	static ArrayList<String> HighLevelDivisions;

	public static void createDefaulthighLevelDivisionsList(){
		DefaulthighLevelDivisions.add("partie"); DefaulthighLevelDivisions.add("livre"); 
		DefaulthighLevelDivisions.add("titre"); DefaulthighLevelDivisions.add("chapitre"); 
		DefaulthighLevelDivisions.add("section"); DefaulthighLevelDivisions.add("soussection");  
		DefaulthighLevelDivisions.add("sous-section"); 
		//return DefaulthighLevelDivisions;
	}

	public static void defineHighLevelDivisionsList(){
		//Verify if highLevelElementTypes contains word in french
		if (Configuration.highLevelElementTypes.indexOf("chapitre") >=0) {
			HighLevelDivisions = Configuration.highLevelElementTypes;
		}
		else {
			GateLuxembourg.createDefaulthighLevelDivisionsList();
			HighLevelDivisions= DefaulthighLevelDivisions;
		}	
	}

	public static String formatCodeRecueilELI(String s){
		//codes loi
		if (s.toLowerCase().contains("code civil")){return "eli/etat/leg/code/civil";}
		if (s.toLowerCase().contains("instruction criminelle")){return "eli/etat/leg/code/instruction";}
		if (s.toLowerCase().contains("code de commerce")){return "eli/etat/leg/code/commerce";}
		if (s.toLowerCase().contains("code de la consommation")){return "eli/etat/leg/code/consommation";}
		// sécurité sociale && assurances sociales
		if (s.toLowerCase().contains("sociale")){return "eli/etat/leg/code/secu";}
		if (s.toLowerCase().contains("nouveau code de procédure civile")){return "eli/etat/leg/code/npc";}
		if (s.toLowerCase().contains("code de procédure civile")){return "eli/etat/leg/code/pc";}
		if (s.toLowerCase().contains("travail")){return "eli/etat/leg/code/travail";}
		if (s.toLowerCase().contains("code pénal")){return "eli/etat/leg/code/penal";}
		// To verify code penal && code instruction penale
		if (s.toLowerCase().contains("pénal")){return "eli/etat/leg/code/penal";}

		// codes-compilation et recueils
		if (s.toLowerCase().contains("code administratif") && s.toLowerCase().contains("administrations")){return "eli/etat/recueil/administratif_admin";}
		if (s.toLowerCase().contains("code administratif") && s.toLowerCase().contains("fonction publique")){return "￼eli/etat/recueil/administratif_fp";}
		if (s.toLowerCase().contains("code administratif") && s.toLowerCase().contains("institutions")){return "￼eli/etat/recueil/institutions";}
		if (s.toLowerCase().contains("code administratif") && s.toLowerCase().contains("procédures")){return "￼eli/etat/recueil/administratif_proc";}
		if (s.toLowerCase().contains("code communal")){return "￼eli/etat/recueil/communal";}
		if (s.toLowerCase().contains("code de l'environnement") && !s.toLowerCase().contains("annexes")){return "eli/etat/recueil/environnement";}
		if (s.toLowerCase().contains("code de l'environnement") && s.toLowerCase().contains("annexes")){return "eli/etat/recueil/environnement_annexes";}
		if (s.toLowerCase().contains("code de l'éducation nationale")){return "eli/etat/recueil/education";}
		if (s.toLowerCase().contains("code de la route")){return "￼eli/etat/recueil/route";}
		if (s.toLowerCase().contains("code de la santé")){return "￼eli/etat/recueil/sante";}
		if (s.toLowerCase().contains("coopération judiciaire")){return "￼￼eli/etat/recueil/coop_judiciaire";}
		if (s.toLowerCase().contains("accessibilite des lieux")){return "eli/etat/recueil/accessibilite";}
		if (s.toLowerCase().contains("barèmes de l'impôt")){return "eli/etat/recueil/bareme";}
		if (s.toLowerCase().contains("chambre des députés")){return "eli/etat/recueil/chd";}
		if (s.toLowerCase().contains("chambres professionnelles")){return "eli/etat/recueil/chp";}
		if (s.toLowerCase().contains("comptabilité de l'état")){return "eli/etat/recueil/compta";}
		if (s.toLowerCase().contains("conseil d'état")){return "eli/etat/recueil/ce";}
		if (s.toLowerCase().contains("conseil économique et social")){return "eli/etat/recueil/ces";}
		if (s.toLowerCase().contains("constitution")){return "eli/etat/leg/constitution";}
		if (s.toLowerCase().contains("cours et tribunaux")){return "eli/etat/recueil/cours";}
		if (s.toLowerCase().contains("cultes")){return "eli/etat/recueil/cultes";}
		if (s.toLowerCase().contains("directives européennes")){return "eli/etat/recueil/directives";}
		if (s.toLowerCase().contains("droits de l'homme")){return "eli/etat/recueil/droits_Homme";}
		if (s.toLowerCase().contains("élections") || s.toLowerCase().contains("elections")){return "eli/etat/recueil/elections";}
		if (s.toLowerCase().contains("emblèmes nationaux")){return "eli/etat/recueil/emblemes/";}
		if (s.toLowerCase().contains("établissements classés") || s.toLowerCase().contains("etablissements classés")){return "eli/etat/recueil/etablissements_classes";}
		if (s.toLowerCase().contains("expropriation") || s.toLowerCase().contains("utilité publique")){return "eli/etat/recueil/expropriation";}
		if (s.toLowerCase().contains("gouvernement")){return "eli/etat/recueil/gouvernement";}
		if (s.toLowerCase().contains("informatique") || s.toLowerCase().contains("identification numérique")){return "eli/etat/recueil/identification";}
		if (s.toLowerCase().contains("institut national") || s.toLowerCase().contains("aministration publique")){return "eli/etat/recueil/inap";}
		if (s.toLowerCase().contains("logement")){return "eli/etat/recueil/logement";}
		if (s.toLowerCase().contains("marchés publics")){return "eli/etat/recueil/marches_publics";}
		if (s.toLowerCase().contains("mémorial")){return "eli/etat/recueil/memorial/";}
		if (s.toLowerCase().contains("nationalité luxembourgeoise")){return "eli/etat/recueil/nationalite";}
		if (s.toLowerCase().contains("presse et médias")){return "eli/etat/recueil/medias";}
		if (s.toLowerCase().contains("réquisitions")){return "eli/etat/recueil/requisitions";}
		if (s.toLowerCase().contains("sites et monuments")){return "eli/etat/recueil/sites_monuments";}
		if (s.toLowerCase().contains("sociétés et associations")){return "eli/etat/recueil/societes_associations";}
		if (s.toLowerCase().contains("subvention de loyer")){return "eli/etat/recueil/loyer";}
		return s;
	}


	public GateLuxembourg() {
		super();
		// TODO Auto-generated constructor stub
	}
}
