package gateStandAlone;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.text.WordUtils;

import gate.Annotation;
import gate.AnnotationSet;
import gate.util.OffsetComparator;

/**	Copyright (c) {2014-2019}, University of Luxembourg, Interdisciplinary Centre for Security, Reliability and Trust (SnT)
* 	Nicolas Sannier (sannier@svv.lu),
*	Morayo Adedjouma (morayoade@gmail.com),
*	Virgil Tassan (tassan-zanin-caser@svv.lu),
*	Wei Dou (dou@svv.lu),
*	Mehrdad Sabetzadeh (sabetzadeh@svv.lu),
*	Lionel Briand (briand@svv.lu),
*/

public class GateAmendmentWriter {
	
	private ArrayList<Integer> parsedBasicElementOffset;
	private ArrayList<Integer> parsedSubdivisionElementOffset;
	
	private GateGenericXMLWriter ggxmlw;
	private StringBuffer sb;
	
	public static String document;
	
	public GateAmendmentWriter(){
		System.out.println("NEW AMENDMENT");
		ggxmlw = new GateGenericXMLWriter();
		sb = new StringBuffer();
		parsedBasicElementOffset = new ArrayList<Integer>();
		parsedSubdivisionElementOffset = new ArrayList<Integer>();
	}
	
	// TODO this last function
	public String GenerateAmendmentXML(AnnotationSet amendment, Annotation amendmentSegment){
		document = amendment.getDocument().getContent().toString();
		try {
			sb.append(ggxmlw.openElement("amendment",null));
			writeDocument(amendment, amendmentSegment);
			
			sb.append(ggxmlw.closeElement("amendment"));
			sb.append(System.lineSeparator());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return new String(sb);
	}
	
	
	public void writeDocument(AnnotationSet inputDocument, Annotation elementSegment) throws IOException {
		String elementType = Configuration.highLevelElementTypes.get(0);
		int i = 1;
		
		while (i < Configuration.highLevelElementTypes.size() && inputDocument.getContained(elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset()).get(Configuration.highLevelElementTypes.get(i) + "_Segment").isEmpty()){
			i++;
		}		

		// if there are high-level elements found in the text, go for writing the remaining high-level elements
		if (i<Configuration.highLevelElementTypes.size()){
			// writing the interleaved basic elements that are before the first high-Level division element
			writeIntroductoryBasicElements(inputDocument, elementSegment, elementType);
			
			// going for the rest of the structure
			WordUtils.capitalize(elementType);
			String highLevelElementType = Configuration.highLevelElementTypes.get(i);
			AnnotationSet highLevelElementSegments = inputDocument.getContained(elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset()).get(highLevelElementType + "_Segment");
			List<Annotation> elemList = new ArrayList<Annotation>(highLevelElementSegments);
			Collections.sort(elemList, new OffsetComparator());
			// Iterate through the different segments of the given type and write their contents
			Iterator<Annotation> elemIter = elemList.iterator();
			while (elemIter.hasNext()) {
				Annotation highLevelElemSegment = elemIter.next();
				writeHighLevelElement(inputDocument, highLevelElemSegment, highLevelElementType);
			}
		}else{
			// write the contained basic element if no high level divisions elements are here
			AnnotationSet basicElementSegments = inputDocument.getContained(elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset()).get(Configuration.basicElement + "_Segment");
			List<Annotation> basicElemList = new ArrayList<Annotation>(basicElementSegments);
			if(basicElemList.size() > 0){
				Collections.sort(basicElemList, new OffsetComparator());
				for (Annotation ann : basicElemList){
					writeBasicElement(inputDocument, ann, Configuration.basicElement);
				}
			}else{
				writeSubdivision(inputDocument, elementSegment, Configuration.subElementTypes.get(0));
			}
		}
	}
	
	public void writeHighLevelElement(AnnotationSet inputDocument, Annotation elementSegment, String elementType) throws IOException {
		//get the header of the element
		WordUtils.capitalize(elementType);
		AnnotationSet elementHeaders = inputDocument.get(elementType + "_Head", elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset());
		Annotation elementHead = elementHeaders.iterator().next();

		//process the cross references in the element features
		Map<String, String> features = processBlocks(inputDocument, elementHead);

		// writing the XML element header
		sb.append(ggxmlw.openElement(elementType,features));
		sb.append(System.lineSeparator());
		
		// writing the interleaved basic elements
		writeIntroductoryBasicElements(inputDocument, elementSegment, elementType);
		
		// writing the rest of HL elements
		// getting all the annotations of subdivision (the first level that is not empty) within a given span)
		int i = Configuration.highLevelElementTypes.indexOf(elementType);
		//System.out.println("looking for the index of " + elementType + " in the high-level elements lists - position is: " + i);
		i++;
		while (i < Configuration.highLevelElementTypes.size() && inputDocument.getContained(elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset()).get(Configuration.highLevelElementTypes.get(i) + "_Segment").isEmpty()){
			i++;
			}
		//System.out.println("next subdivision of " + elementType + " in the high-level elements lists - position is: " + i);
		if (i<Configuration.highLevelElementTypes.size()){
			// we have found a subdivision segment
			String highLevelElementType = Configuration.highLevelElementTypes.get(i);
			AnnotationSet highLevelElementSegments = inputDocument.getContained(elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset()).get(highLevelElementType + "_Segment");
			List<Annotation> elemList = new ArrayList<Annotation>(highLevelElementSegments);
			Collections.sort(elemList, new OffsetComparator());
			// Iterate through the different segments of the given type and write their contents
			Iterator<Annotation> elemIter = elemList.iterator();
			while (elemIter.hasNext()) {
				Annotation highLevelElemSegment = elemIter.next();
				writeHighLevelElement(inputDocument, highLevelElemSegment, highLevelElementType);
			}
		}
		else{
			// write the contained basic element if no high level divisions elements are here
			AnnotationSet basicElementSegments = inputDocument.getContained(elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset()).get(Configuration.basicElement + "_Segment");
			List<Annotation> basicElemList = new ArrayList<Annotation>(basicElementSegments);
			Collections.sort(basicElemList, new OffsetComparator());
			for (Annotation ann : basicElemList){
				writeBasicElement(inputDocument, ann, Configuration.basicElement);
			}
		}
		// writes the closing tags for the element
		sb.append(ggxmlw.closeElement(elementType));
		sb.append(System.lineSeparator());
	}
	
	/**
	 * 
	 * @param inputDocument the global input document 
	 * @param elementHeader the header containing the block feature to process
	 * @param features the map to process and modify
	 * @return
	 */
	public Map<String, String> processBlocks(AnnotationSet inputDocument, Annotation elementHeader){
		Map<String, String> features = importFeatures(elementHeader);
		// managing the potential CRs in the block elements
		// will change the content of the Jape feature that were imported by replacing them with the XML tags to introduce
		String blockFeature = features.get("block");
		if (blockFeature != null && !blockFeature.equals("")){
			// some initialization for debugging and logging 
			int crStart = 0; int crEnd = 0;
			String crText = "";
			try{
				// get the cross references in the alinea
				AnnotationSet myRefs = inputDocument.getContained(elementHeader.getStartNode().getOffset(), elementHeader.getEndNode().getOffset()).get("CrossReferences_ref");
				List<Annotation> refsList = new ArrayList<Annotation>(myRefs);
				Collections.sort(refsList, new OffsetComparator());
				// Look at the CR within the alinea to replace them with their resolution
				//bottom-up approach for changes so that the CR offsets are not affected
				for (Annotation myRef : refsList){			
					// capturing the text of the cross reference and its offsets
					crStart = myRef.getStartNode().getOffset().intValue();
					crEnd = myRef.getEndNode().getOffset().intValue();
					crText = GateGenericXMLWriter.document.substring(crStart, crEnd);
					//System.out.println("processing cross-reference between offset "+ crStart + " and " + crEnd + " -- " + crText);			
					// building the cross reference resolution via the "eli" features in the cross reference
					String eli = (String) myRef.getFeatures().get("eli");
					String XMLRef =   ggxmlw.gaw.writeAkomaCrossReference(crText, Configuration.serverRoot+eli);
					blockFeature = blockFeature.replace(crText,XMLRef);
				}
				features.put("block", blockFeature);
			}
			catch(Exception e){
				System.err.println("Something went wrong while processing cross references in header's block -- CR: " + crText + " at offsets: " + crStart +" - " + crEnd);
				e.printStackTrace();
			}
		}
		return features;
	}

	/**
	 * imports the annotation_Head (will not work for other type of headers) features into a external Map
	 * @param ann the annotation whose FeatureMap has to be imported to capture its list of features
	 * @return the list of features of the annotation into a map
	 */
	public Map<String, String> importFeatures(Annotation ann){
		HashMap<String, String> features = new HashMap<String, String>();
		features.put("num", (String) ann.getFeatures().get("num"));
		features.put("title", (String) ann.getFeatures().get("title"));
		features.put("block", (String) ann.getFeatures().get("block"));
		features.put("id", (String) ann.getFeatures().get("id"));
		return features;
	}
	
	/**
	 * checks that the basic element has the same spelling than in the structure,
	 * checks that it has not been already treated (test on the header's offset), 
	 * writes the basic element XML tags, 
	 * write the basic element subdivisions.
	 * @param inputDocument
	 * @param elementSegment
	 * @param elementType
	 * @throws IOException
	 */
	public void writeBasicElement(AnnotationSet inputDocument, Annotation elementSegment, String elementType) throws IOException {
		//get the header of the element
		WordUtils.capitalize(elementType);
		AnnotationSet elementHeaders = inputDocument.get(elementType + "_Head", elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset());
//		// TODO remove at the end
		//System.out.println("Article Segment : "+elementSegment.getStartNode().getOffset().intValue()+" "+elementSegment.getEndNode().getOffset().intValue());
		// To get the first basic element
		List<Annotation> elementHeadersList = new ArrayList<Annotation>(elementHeaders);
		Collections.sort(elementHeadersList, new OffsetComparator());
		Annotation elementHead = elementHeadersList.iterator().next();

		//process the cross references in the element features
		Map<String, String> features = processBlocks(inputDocument, elementHead);
			
		// writing the XML element header
		// verify that the segment (detected by its starting offset), was not already handled
		int startElem = elementHead.getStartNode().getOffset().intValue();
		
		if (parsedBasicElementOffset.indexOf(startElem) == -1){
			parsedBasicElementOffset.add(startElem);
			// writes the XML opening tags related to the structure element
			sb.append(ggxmlw.openElement(elementType,features));
			sb.append(System.lineSeparator());
			// writes the basic element's subdivisions
			// getting all the annotations of subdivision (the first level that is not empty) within a given span)
			int i = 0;
			while (i < Configuration.subElementTypes.size() && inputDocument.getContained(elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset()).get(Configuration.subElementTypes.get(i) + "_Segment").isEmpty()){
				i++;
				}
			if (i>=Configuration.subElementTypes.size()){
				// in case of error, warn and return the textual content of the segment
				int elemStart = elementSegment.getStartNode().getOffset().intValue();
				int elemEnd = elementSegment.getEndNode().getOffset().intValue();
				String workAround = document.substring(elemStart, elemEnd);
				sb.append(workAround);
				System.err.println("Unable to find a subdvision segment in "+Configuration.basicElement.toLowerCase()+": " + features.get("num") + " at offset: " +startElem + "\n"
						+ "The following text will be inserted as-is in the XML document: \n" + workAround);
			}
			else{
				String subElementType = Configuration.subElementTypes.get(i);
				AnnotationSet subElementSegments = inputDocument.getContained(elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset()).get( subElementType + "_Segment");
				List<Annotation> elemList = new ArrayList<Annotation>(subElementSegments);
				Collections.sort(elemList, new OffsetComparator());
				// Iterate through the different segments of the given type and write their contents
				Iterator<Annotation> elemIter = elemList.iterator();
				while (elemIter.hasNext()) {
					Annotation subElemSegment = elemIter.next();
					writeSubdivision(inputDocument, subElemSegment, subElementType);
				}
			}
			// writes the closing tags for the element
			sb.append(ggxmlw.closeElement(elementType));
			sb.append(System.lineSeparator());

		}
		else{
			// for debugging only
			System.out.println("Basic element already written: " + features.get("num") + " at offset: " + startElem);
		}
	}
	
	/**
	 * will look for basic elements that are written between two high-level divisions as introductory provisions
	 * from a given element type, it will look for the first lower high-level division coming in the span, and will look for basic elements between the start of the segment and the start of the new division
	 * 
	 * @param inputDocument the document to parse
	 * @param elementSegment the current segment to write
	 * @param elementType the current high level division of the segment
	 * @throws IOException
	 */
	public void writeIntroductoryBasicElements(AnnotationSet inputDocument, Annotation elementSegment, String elementType) throws IOException{
		// look for the first HL element segment in the span
		int i = Configuration.highLevelElementTypes.indexOf(elementType);
		i++;
		while (i < Configuration.highLevelElementTypes.size() && inputDocument.getContained(elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset()).get(Configuration.highLevelElementTypes.get(i) + "_Segment").isEmpty()){
			i++;
			}

		long start = 0;
		long end = 0;
		
		if (i<Configuration.highLevelElementTypes.size()){
			// getting the type of the first HL division found in the segment
			String highLevelElementType = Configuration.highLevelElementTypes.get(i);
			//System.out.println("writeIntroductoryBasicElements for element: " + elementType + " - Having found " + highLevelElementType + " as next subElement");
			// capturing the first HL element in the span
			AnnotationSet highLevelElementSegments = inputDocument.getContained(elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset()).get(highLevelElementType + "_Segment");
			List<Annotation> elemList = new ArrayList<Annotation>(highLevelElementSegments);
			Collections.sort(elemList, new OffsetComparator());
			Annotation firstElem = elemList.get(0);
			
			// getting the span where to look for interleaved basic elements
			start = elementSegment.getStartNode().getOffset();
			end =  firstElem.getStartNode().getOffset();
			//System.out.println("span to look for interleaved elements: " + start + " - " + end);
		}
		// no structure in the text, only articles -- the span is the entire <root>_Segment
		else{
			// getting the span where to look for interleaved basic elements
			start = elementSegment.getStartNode().getOffset();
			end =  elementSegment.getEndNode().getOffset();
			//System.out.println("span to look for interleaved elements: " + start + " - " + end);
		}			
		// catching the interleaved basic elements
		AnnotationSet basicElementSegments = inputDocument.getContained(start, end).get(Configuration.basicElement + "_Segment");
		List<Annotation> basicElemList = new ArrayList<Annotation>(basicElementSegments);
		Collections.sort(basicElemList, new OffsetComparator());
		//System.out.println("have found: " + basicElemList.size() + " interleaved basic elements");
		// writing the XML for each elements
		for (Annotation ann : basicElemList){
			writeBasicElement(inputDocument, ann, Configuration.basicElement);
		}
	}
	
	
	/**
	 * checks that the subdivision element has the same spelling than in the structure,
	 * writes the subdivision element XML tags, 
	 * goes into the recursion for other subdivision or, if last, go for the content with the call to processText
	 * @param inputDocument
	 * @param elementSegment
	 * @param elementType
	 * @throws IOException
	 */
	public void writeSubdivision(AnnotationSet inputDocument, Annotation elementSegment, String elementType) throws IOException {
		//get the header of the element
		WordUtils.capitalize(elementType);
		AnnotationSet elementHeaders = inputDocument.get(elementType + "_Head", elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset());
//		System.out.println("Element Headers: "+elementHeaders.toString());
		// process the cross references in the element features
		Map<String, String> features = new HashMap<String, String>();
		if(elementHeaders.size() > 0){
			// get the feature from the element_head annotation
			sb.append(ggxmlw.openElement(elementType, features));
		}
		// writing the rest of subdivisions elements
		// getting all the annotations of subdivision (the first level that is not empty) within a given span)
		int i = Configuration.subElementTypes.indexOf(elementType);
		// System.out.println("looking for the index of " + elementType " in the subdivision elements lists - position is: " + i);
		i++;
		while (i < Configuration.subElementTypes.size() && inputDocument.getContained(elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset()).get(Configuration.subElementTypes.get(i) + "_Segment").isEmpty()) {
			i++;
		}
		// System.out.println("next subdivision of " + elementType + " in the subdivision elements lists - position is: " + i);
		if (i < Configuration.subElementTypes.size()) {
			// we have found a subdivision segment
			String subdivisionElement = Configuration.subElementTypes.get(i);
			AnnotationSet subdivisionElementSegments = inputDocument.getContained(elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset()).get(subdivisionElement + "_Segment");
			List<Annotation> elemList = new ArrayList<Annotation>(subdivisionElementSegments);
			Collections.sort(elemList, new OffsetComparator());
			// Iterate through the different segments of the given type and write their contents
			Iterator<Annotation> elemIter = elemList.iterator();
			while (elemIter.hasNext()) {
				Annotation subdivisionElementSegment = elemIter.next();
				writeSubdivision(inputDocument, subdivisionElementSegment, subdivisionElement);
			}
		} else {
			// write the contained element if no more subdivisions elements are here
			AnnotationSet lastElementSegments = inputDocument.getContained(elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset()).get(elementType + "_Segment");
			if(lastElementSegments.size() > 0){
				List<Annotation> elemList = new ArrayList<Annotation>(lastElementSegments);
				Collections.sort(elemList, new OffsetComparator());
				for (Annotation elem : elemList) {
					// verify that the segment (detected by its starting offset), was not already handled
					int startElem = elem.getStartNode().getOffset().intValue();
					if (parsedSubdivisionElementOffset.indexOf(startElem) == -1) {
						parsedSubdivisionElementOffset.add(startElem);
						sb.append(processText(inputDocument, elem).trim());
					}
					else{
						// for debugging only
						System.out.println("subdivision element already written at offset: " + startElem);
					}
				}
			}else{
				String type = Configuration.subElementTypes.get(Configuration.subElementTypes.size()-1);
				sb.append(ggxmlw.openElement(type, new HashMap<String,String>()));
				sb.append(processText(inputDocument, elementSegment).trim());
				sb.append(ggxmlw.closeElement(type));
			}
		}
		if(elementHeaders.size() > 0){
			// writes the closing tags for the element
			sb.append(ggxmlw.closeElement(elementType));
		}
		sb.append(System.lineSeparator());
	}
	
	public String processText(AnnotationSet inputDocument, Annotation element){
		if (Configuration.style.toLowerCase().contains("akoma")){return ggxmlw.gaw.processText(inputDocument, element, true);}
		return "";
	}
	
}
