package gateStandAlone;
import gate.Annotation;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**	Copyright (c) {2014-2019}, University of Luxembourg, Interdisciplinary Centre for Security, Reliability and Trust (SnT)
* 	Nicolas Sannier (sannier@svv.lu),
*	Morayo Adedjouma (morayoade@gmail.com),
*	Virgil Tassan (tassan-zanin-caser@svv.lu),
*	Wei Dou (dou@svv.lu),
*	Mehrdad Sabetzadeh (sabetzadeh@svv.lu),
*	Lionel Briand (briand@svv.lu),
*/

public class GateAkomaConverter {

	static String partStart="<part><num>%s</num><heading>%s<docNumber>%s</docNumber></heading>";
	static String partEnd="</part>";
	
	static String bookStart="<book><num>%s</num><heading>%s<docNumber>%s</docNumber></heading>";
	static String bookEnd="</book>";

	static String titleStart="<title><num>%s</num><heading>%s<docNumber>%s</docNumber></heading>";
	static String titleEnd="</title>";

	static String chapterStart="<chapter><num>%s</num><heading>%s<docNumber>%s</docNumber></heading>";
	static String chapterEnd="</chapter>";

	static String sectionStart="<section><num>%s</num><heading>%s<docNumber>%s</docNumber></heading>";
	static String sectionEnd="</section>";

	static String subsectionStart="<subsection><num>%s</num><heading>%s<docNumber>%s</docNumber></heading>";
	static String subsectionEnd="</subsection>";

	static String articleStart="<article><num>%s</num><heading>%s<docNumber>%s</docNumber></heading>";
	static String articleEnd="</article>";

	static String paragraphStart="<paragraph><num>%s</num><heading>%s<docNumber>%s</docNumber></heading>"+System.getProperty("line.separator");
	static String paragraphEnd="</paragraph>";

	static String alineaStart="<alinea><num>%s</num><heading>%s<docNumber>%s</docNumber></heading>"+System.getProperty("line.separator")+"<content><p>";
	static String alineaEnd="</p>"+System.getProperty("line.separator")+"</content></alinea>";
	
	static String actStart="<act name=\"%s\" scl:uri=\"%s\" scl:verify=\"true\">";
	static String actEnd="</act>";
	
	static String amendmentStart="<embeddedStructure>";
	static String amendmentEnd="</embeddedStructure>";
	
	static String mainStart="<body>";
	static String mainEnd="</body>";

	static String crossRef = "<ref href=\"%s\" scl:verify=\"true\">%s</ref>";
	
	static String notesStart="<notes>";
	static String notesEnd="</notes>";
	static String note = "<note scl:uri=\"%s\"><p>%s</p></note>";
	static String noteRef = "<noteRef href=\"%s\"/>";
	

	/**
	 * imports the annotation_Head (will not work for other type of headers) features into a external Map
	 * @param ann the annotation whose FeatureMap has to be imported to capture its list of features
	 * @return the list of features of the annotation into a map
	 */
	public Map<String, String> importFeatures(Annotation ann){
		HashMap<String, String> features = new HashMap<String, String>();
		features.put("num", (String) ann.getFeatures().get("num"));
		features.put("title", (String) ann.getFeatures().get("title"));
		features.put("block", (String) ann.getFeatures().get("block"));
		features.put("id", (String) ann.getFeatures().get("id"));
		return features;
	}
	
	/**
	 * depending of the structural element and the features the Element_Header has would create the related Akoma Ntoso XML element
	 * @param structure the structural element to be opened
	 * @param features the list of features of the header element
	 * @return the xml tag which matched with the input
	 */
	public String openElement(String structure, Map<String, String> features){
		if (structure.toLowerCase().equals("act")){return openAct();}
		else if (structure.toLowerCase().equals("part")){return openPart(features);}
		else if (structure.toLowerCase().equals("book")){return openBook(features);}
		else if (structure.toLowerCase().equals("title")){return openTitle(features);}
		else if (structure.toLowerCase().equals("chapter")){return openChapter(features);}
		else if (structure.toLowerCase().equals("section")){return openSection(features);}
		else if (structure.toLowerCase().equals("subsection")){return openSubsection(features);}
		else if (structure.toLowerCase().equals("namedparagraph")){return openSubsection(features);}
		else if (structure.toLowerCase().equals("article")){return openArticle(features);}
		else if (structure.toLowerCase().equals("paragraph")){return openParagraph(features);}
		else if (structure.toLowerCase().equals("alinea")){return openAlinea(features);}
		else if (structure.toLowerCase().equals("amendment")){return openAmendment(features);}
		System.err.println("no corresponding Akoma Ntoso structural element found with name: " + structure);
		System.exit(-1);
		return structure;
	}
	
	/**
	 * 
	 * @param structure the structural element to be closed
	 * @return the xml tag which matched with the structure input
	 */
	public String closeElement(String structure){
		if (structure.toLowerCase().equals("act")){return closeAct();}
		else if (structure.toLowerCase().equals("part")){return closePart();}
		else if (structure.toLowerCase().equals("book")){return closeBook();}
		else if (structure.toLowerCase().equals("title")){return closeTitle();}
		else if (structure.toLowerCase().equals("chapter")){return closeChapter();}
		else if (structure.toLowerCase().equals("section")){return closeSection();}
		else if (structure.toLowerCase().equals("subsection")){return closeSubsection();}
		else if (structure.toLowerCase().equals("namedparagraph")){return closeSubsection();}
		else if (structure.toLowerCase().equals("article")){return closeArticle();}
		else if (structure.toLowerCase().equals("paragraph")){return closeParagraph();}
		else if (structure.toLowerCase().equals("alinea")){return closeAlinea();}
		else if (structure.toLowerCase().equals("amendment")){return closeAmendment();}
		System.err.println("no corresponding Akoma Ntoso structural element found with name: " + structure);
		System.exit(-1);
		return structure;
	}
	
	/**
	 * Associates the given features to the part header
	 * @param features the features of the part header
	 * @return the xml header of a part
	 */
	public String openPart(Map<String, String> features){
		String result = String.format(partStart,features.get("num"), features.get("title"), features.get("block"), features.get("id"));
		//System.out.println("part starter formatted as: " + result);
		return cleanHeader(result);
	}

	/**
	 * Associates the given features contained in the annotation to the part header
	 * @param ann the features of the part header
	 * @return the xml header of a part
	 */
	public String openPart(Annotation ann){
		Map<String, String> features = importFeatures(ann);
		return openPart(features);
	}

	/**
	 * returns </part> string
	 * @return an close part xml tag
	 */
	public String closePart(){
		return partEnd;
	}

	/**
	 * Associates the given features to the book header
	 * @param features the features of the book header
	 * @return the xml header of a book
	 */
	public String openBook(Map<String, String> features){
		String result = String.format(bookStart,features.get("num"), features.get("title"), features.get("block"), features.get("id"));
		//System.out.println("book starter formatted as: " + result);
		return cleanHeader(result);
	}

	/**
	 * Associates the given features contained in the annotation to the book header
	 * @param ann the features of the book header
	 * @return the xml header of a book
	 */
	public String openBook(Annotation ann){
		Map<String, String> features = importFeatures(ann);
		return openBook(features);
	}

	/**
	 * returns </book> string
	 * @return an close book xml tag
	 */
	public String closeBook(){
		return bookEnd;
	}


	/**
	 * Associates the given features to the title header
	 * @param features the features of the title header
	 * @return the xml header of a title
	 */
	public String openTitle(Map<String, String> features){
		String result = String.format(titleStart,features.get("num"), features.get("title"), features.get("block"), features.get("id"));
		//System.out.println("title starter formatted as: " + result);
		return cleanHeader(result);
	}

	/**
	 * Associates the given features contained in the annotation to the title header
	 * @param ann the features of the title header
	 * @return the xml header of a title
	 */
	public String openTitle(Annotation ann){
		Map<String, String> features = importFeatures(ann);
		return openTitle(features);
	}

	/**
	 * returns </title> string
	 * @return an close title xml tag
	 */
	public String closeTitle(){
		return titleEnd;
	}

	/**
	 * Associates the given features to the chapter header
	 * @param features the features of the chapter header
	 * @return the xml header of a chapter
	 */
	public String openChapter(Map<String, String> features){
		String result = String.format(chapterStart,features.get("num"), features.get("title"), features.get("block"), features.get("id"));
		//System.out.println("chapter starter formatted as: " + result);
		return cleanHeader(result);
	}

	/**
	 * Associates the given features contained in the annotation to the chapter header
	 * @param ann the features of the chapter header
	 * @return the xml header of a chapter
	 */
	public String openChapter(Annotation ann){
		Map<String, String> features = importFeatures(ann);
		return openChapter(features);
	}

	/**
	 * returns </chapter> string
	 * @return an close chapter xml tag
	 */
	public String closeChapter(){
		return chapterEnd;
	}

	/**
	 * Associates the given features to the section header
	 * @param features the features of the section header
	 * @return the xml header of a section
	 */
	public String openSection(Map<String, String> features){
		String result = String.format(sectionStart,features.get("num"), features.get("title"), features.get("block"), features.get("id"));
		//System.out.println("section starter formatted as: " + result);
		return cleanHeader(result);
	}

	/**
	 * Associates the given features contained in the annotation to the section header
	 * @param ann the features of the section header
	 * @return the xml header of a section
	 */
	public String openSection(Annotation ann){
		Map<String, String> features = importFeatures(ann);
		return openSection(features);
	}

	/**
	 * returns </section> string
	 * @return an close section xml tag
	 */
	public String closeSection(){
		return sectionEnd;
	}

	/**
	 * Associates the given features to the subsection header
	 * @param features the features of the subsection header
	 * @return the xml header of a subsection
	 */
	public String openSubsection(Map<String, String> features){
		String result = String.format(subsectionStart,features.get("num"), features.get("title"), features.get("block"), features.get("id"));
		//System.out.println("chapter starter formatted as: " + result);
		return cleanHeader(result);
	}

	/**
	 * Associates the given features contained in the annotation to the subsection header
	 * @param ann the features of the subsection header
	 * @return the xml header of a subsection
	 */
	public String openSubsection(Annotation ann){
		Map<String, String> features = importFeatures(ann);
		return openSubsection(features);
	}

	/**
	 * returns </subsection> string
	 * @return an close subsection xml tag
	 */
	public String closeSubsection(){
		return subsectionEnd;
	}

	/**
	 * Associates the given features to the article header
	 * @param features the features of the article header
	 * @return the xml header of an article
	 */
	public String openArticle(Map<String, String> features){
		String result = String.format(articleStart,features.get("num"), features.get("title"), features.get("block"), features.get("id"));
		result = result.replace("<article>","<article scl:uri=\""+Configuration.serverRoot+Configuration.rootELI+"/"+ ConfigTool.basicEli +features.get("id")+"\">");
		//System.out.println("chapter starter formatted as: " + result);
		return cleanHeader(result);
	}

	/**
	 * Associates the given features contained in the annotation to the article header
	 * @param ann the features of the article header
	 * @return the xml header of an article
	 */
	public String openArticle(Annotation ann){
		Map<String, String> features = importFeatures(ann);
		return openArticle(features);
	}

	/**
	 * returns </article> string
	 * @return an close article xml tag
	 */
	public String closeArticle(){
		return articleEnd;
	}

	/**
	 * Associates the given features to the paragraph header
	 * @param features the features of the paragraph header
	 * @return the xml header of a paragraph
	 */
	public String openParagraph(Map<String, String> features){
		String result = String.format(paragraphStart,features.get("num"), features.get("title"), features.get("block"), features.get("id"));
		//System.out.println("chapter starter formatted as: " + result);
		return cleanHeader(result);
	}

	/**
	 * Associates the given features contained in the annotation to the paragraph header
	 * @param ann the features of the paragraph header
	 * @return the xml header of a paragraph
	 */
	public String openParagraph(Annotation ann){
		Map<String, String> features = importFeatures(ann);
		return openParagraph(features);
	}
	/**
	 * returns </paragraph> string
	 * @return an close paragraph xml tag
	 */
	public String closeParagraph(){
		return paragraphEnd;
	}

	/**
	 * Associates the given features to the alinea header
	 * @param features the features of the alinea header
	 * @return the xml header of an alinea
	 */
	public String openAlinea(Map<String, String> features){
		String result = String.format(alineaStart,features.get("num"), features.get("title"), features.get("block"), features.get("id"));
		//System.out.println("chapter starter formatted as: " + result);
		return cleanHeader(result);
	}

	/**
	 * Associates the given features contained in the annotation to the alinea header
	 * @param ann the features of the alinea header
	 * @return the xml header of an alinea
	 */
	public String openAlinea(Annotation ann){
		Map<String, String> features = importFeatures(ann);
		return openAlinea(features);
	}

	/**
	 * returns </alinea> string
	 * @return an close alinea xml tag
	 */
	public String closeAlinea(){
		return alineaEnd;
	}
	
	/**
	 * Associates the given features to the amendment header
	 * @param features the features of the amendment header
	 * @return the xml header of an amendment
	 */
	public String openAmendment(Map<String, String> features){
		return amendmentStart;
	}
	
	/**
	 * Associates the given features contained in the annotation to the amendment header
	 * @param ann the features of the amendment header
	 * @return the xml header of an amendment
	 */
	public String openAmendment(Annotation ann){
		return amendmentStart;
	}

	/**
	 * returns </embeddedStructure> string
	 * @return an close amendment xml tag
	 */
	public String closeAmendment(){
		return amendmentEnd;
	}
	
	
	/**
	 * returns <act> string
	 * @return an open act xml tag
	 */
	public String openAct(){
		String result = String.format(actStart, Configuration.longTitle, Configuration.rootELI);
		//System.out.println("act starter formatted as: " + result);
		return cleanHeader(result);
	}
	
	/**
	 * returns </act> string
	 * @return an close act xml tag
	 */
	public String closeAct(){
		return actEnd;
	}
	
	/**
	 * returns <body> string
	 * @return an open body xml tag
	 */
	public String openBody(){
		String result = String.format(mainStart);
		//System.out.println("act starter formatted as: " + result);
		return result;
	}
	
	/**
	 * returns </body> string
	 * @return a close body xml tag
	 */
	public String closeBody(){
		return mainEnd;
	}

	/**
	 * Removes all empty xml tags from an header string.
	 * @param s the string to clean
	 * @return the cleaned String
	 */
	public String cleanHeader(String s){
		System.out.println("HEADER : "+s);
		String result = s;
		result = result.replace("null","");
		result = result.replace("<docNumber></docNumber>","");
		result = result.replace("<heading></heading>","");
		result = result.replace("<num></num>","");
		System.out.println("TRIM : "+result.trim());
		return result;
	}
	
	public String cleanFootNotesAnchor(String s){
		String result = s;
		Pattern p = Pattern.compile("\\[footnoteRef:([0-9]+)\\]");
		Matcher m = p.matcher(result);
		while(m.find()){
			result = result.replaceAll("\\[footnoteRef:"+m.group(1)+"\\](\\t)?", "<noteRef href=\""+m.group(1)+"\"/>");
		}
		
		return result;
	}
	
}
