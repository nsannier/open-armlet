package gateStandAlone;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//import armletLegalAnalysis.model.ToReplacePostProcess;

/**	Copyright (c) {2014-2019}, University of Luxembourg, Interdisciplinary Centre for Security, Reliability and Trust (SnT)
* 	Nicolas Sannier (sannier@svv.lu),
*	Morayo Adedjouma (morayoade@gmail.com),
*	Virgil Tassan (tassan-zanin-caser@svv.lu),
*	Wei Dou (dou@svv.lu),
*	Mehrdad Sabetzadeh (sabetzadeh@svv.lu),
*	Lionel Briand (briand@svv.lu),
*/

public class GateFinalPostProcessing {
	
	/**
	 * clear all the footnotes metadata from the given text
	 * @param inputName path of the input text
	 * @param outputName path of the output text
	 * @throws IOException
	 */
	public static void cleanFootnotes(String inputName, String outputName) throws IOException{
		FileInputStream is = new FileInputStream(new File(inputName));
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader reader = new BufferedReader(isr);
		
		String line = "";
		ArrayList<String> lines = new ArrayList<String>();

		while((line = reader.readLine()) != null) {
		    lines.add(line);
		}
		reader.close();
		
		//clear the footnote anchors metadata
		lines = cleanFootnoteAnchors(lines);
		
		//clear the footnote contents metadata
		lines = cleanFootnoteContents(lines);
		
		FileOutputStream os = new FileOutputStream(outputName);
		OutputStreamWriter osw = new OutputStreamWriter(os);
		BufferedWriter writer = new BufferedWriter(osw);
		for(String lineFinale :lines){
			writer.write(lineFinale+"\n");
		}
		writer.close();
	}
	
	/**
	 * replace all footnotes metadata by an href xml markup
	 * @param lines the text to clean
	 * @return
	 */
	public static ArrayList<String> cleanFootnoteAnchors(ArrayList<String> lines){
		
		int size = lines.size();
		
		ArrayList<String> res =new ArrayList<String>();
		
		for(int i=0;i<size;i++) {
			String line = lines.get(i);
			
			String tempLine = line;
			//regular expression to detect footnote anchor
			Pattern p = Pattern.compile("\\[footnoteRef:([0-9]+)\\]");
			Matcher m = p.matcher(line);
			while(m.find()){
				//replace the metadata footnote anchor by the appropriate xml tag
				tempLine = tempLine.replaceAll("\\[footnoteRef:"+m.group(1)+"\\](\\t)?", "<noteRef href=\""+m.group(1)+"\"/>");
			}
			res.add(tempLine);
		}

		return res;
	}
	
	/**
	 * clean the footnote contents metadata in the text
	 * @param lines the text to clean
	 * @return
	 */
	public static ArrayList<String> cleanFootnoteContents(ArrayList<String> lines){
		
		int size = lines.size();
		
		ArrayList<String> res =new ArrayList<String>();

		boolean addLine = true;
		
		for(int i=0; i<size; i++){
			addLine = true;
			
			String line = lines.get(i);
			
			//basic footnote content pattern [12: the text of the footnote]
			line = line.replaceAll("\\[[0-9]+:[^\\]]*\\](\\t)?", "");
			
			//remove process if the footnote is spread over several lines
			Pattern p = Pattern.compile("(\\[[0-9]+:)");
			Matcher m = p.matcher(line);
			while(m.find()){
				if(i+1 < size && lines.get(i+1).contains("]")){
					String nextLine = lines.get(i+1);
					line = line.split("\\"+m.group())[0].trim();
					nextLine = nextLine.split("\\]")[1].trim();
					res.add(line);
					res.add(nextLine);
					addLine = false;
					i++;
				}else{
					if(i+2 < size && lines.get(i+2).contains("]")){
						String nextNextLine = lines.get(i+2);
						line = line.split(m.group())[0].trim();
						nextNextLine = nextNextLine.split("\\]")[1].trim();
						res.add(line);
						res.add(nextNextLine);
						addLine = false;
						i++;i++;
				}
				}
			}
			if(addLine){
				res.add(line);
			}
		}


		return res;
		
	}
	
	public boolean addToRemove(ArrayList<String> list, ArrayList<String> patterns){
		//TODO
		//return if the succession patterns among lines exist
		return false;
	}
	
	public static void cleanUpXMLFile2(String inputName, String outputName) throws IOException{	
	FileInputStream is = new FileInputStream(new File(inputName));
	InputStreamReader isr = new InputStreamReader(is);
	BufferedReader reader = new BufferedReader(isr);
	
	ArrayList<String> list = new ArrayList<String>();
	
	String line = "";
	
	while ((line = reader.readLine()) != null) {
		    list.add(line);
		}
		reader.close();
		list.removeAll(Arrays.asList(" ","", null));
		
		int size = list.size();
		
		ArrayList<Integer> toRemove = new ArrayList<Integer>();
		
		ArrayList<ToReplacePostProcess> toReplace = new ArrayList<ToReplacePostProcess>();
		
		for(int i=0; i<size; i++){
			if(list.get(i).equals("<alinea><content><p></p>")){
				if(i+1 < size && list.get(i+1).equals("</content></alinea>")){
//					System.out.println("LINE "+i+list.get(i));
					toRemove.add(i);
					toRemove.add(i+1);
				}
			}
			if(list.get(i).equals("<alinea>")){
				if(i+1 < size && list.get(i+1).equals("<content><p></p>")){
					if(i+2 < size && list.get(i+2).equals("</content></alinea>")){
						toRemove.add(i);
						toRemove.add(i+1);
						toRemove.add(i+2);
					}
				}	
			}
			if(list.get(i).equals("<p></p>")){
				toRemove.add(i);
			}
			
			if(list.get(i).endsWith("</p>")){
				if(i+1 < size && list.get(i+1).equals("</content></alinea>")){
					if(i+2 < size && list.get(i+2).equals("<alinea><content><p></ol></li>")){
						//keep only  </ol></li>
						toReplace.add(new ToReplacePostProcess(i, "</p>$", ""));
						toRemove.add(i+1);
						toReplace.add(new ToReplacePostProcess(i+2, "<alinea><content><p></ol></li>", "</ol></li>"));
					}else{
						if(i+2 < size && list.get(i+2).equals("<alinea><content><p></ol>")){
							//keep only  </ol>
							toReplace.add(new ToReplacePostProcess(i, "</p>$", ""));
							toRemove.add(i+1);
							toReplace.add(new ToReplacePostProcess(i+2, "<alinea><content><p></ol>", "</ol>"));
						}
					}
				}
			}
			
		}
		
		
		
		
//		System.out.println(toReplace);
		int toReplaceSize = toReplace.size();
		
		for(int i=toReplaceSize-1; i>=0; i--){
			ToReplacePostProcess trpp = toReplace.get(i);
			String lineToReplace = list.get(trpp.lineNumber);
//			System.out.println("line to replace : "+lineToReplace);
			lineToReplace = lineToReplace.replaceAll(trpp.expressionToReplace,trpp.replacementExpression);
			list.set(trpp.lineNumber, lineToReplace);
		}
		
//		System.out.println(toRemove);
		int toRemoveSize = toRemove.size();
		
		for(int i=toRemoveSize-1; i>=0; i--){
//			System.out.println(toRemove.get(i));
			list.remove((int)toRemove.get(i));
		}
		
		//rewrite in text

		FileOutputStream os = new FileOutputStream(outputName);
		OutputStreamWriter osw = new OutputStreamWriter(os);
		BufferedWriter writer = new BufferedWriter(osw);
		for(String elem:list){
			writer.write(elem+"\n");
		}
		writer.close();
	}
	
	
//	public static void main(String[] args) throws IOException{
//		//cleanUpXMLFile("Test/CorporateFramework/output_Loi19150810.xml", "Test/CorporateFramework/Loi_19150810.xml");
//		cleanUpXMLFile2("Test/MoreDaily/1814.xml", "Test/MoreDaily/1814.xml_final.xml");
//	}

}
