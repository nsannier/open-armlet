package gateStandAlone;

/**	Copyright (c) {2014-2019}, University of Luxembourg, Interdisciplinary Centre for Security, Reliability and Trust (SnT)
* 	Nicolas Sannier (sannier@svv.lu),
*	Morayo Adedjouma (morayoade@gmail.com),
*	Virgil Tassan (tassan-zanin-caser@svv.lu),
*	Wei Dou (dou@svv.lu),
*	Mehrdad Sabetzadeh (sabetzadeh@svv.lu),
*	Lionel Briand (briand@svv.lu),
*/

public class ToReplacePostProcess {

	public int lineNumber;
	
	public String expressionToReplace;
	
	public String replacementExpression;

	public ToReplacePostProcess(int lineNumber, String expressionToReplace, String replacementExpression) {
		super();
		this.lineNumber = lineNumber;
		this.expressionToReplace = expressionToReplace;
		this.replacementExpression = replacementExpression;
	}

	@Override
	public String toString() {
		return "ToReplacePostProcess [lineNumber=" + lineNumber + ", expressionToReplace=" + expressionToReplace
				+ ", replacementExpression=" + replacementExpression + "]";
	}
	
	
	
}
