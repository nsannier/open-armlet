package gateStandAlone;

import gate.Annotation;
import gate.AnnotationSet;
import gate.Factory;
import gate.util.InvalidOffsetException;
import gate.util.OffsetComparator;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.text.WordUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

/**	Copyright (c) {2014-2019}, University of Luxembourg, Interdisciplinary Centre for Security, Reliability and Trust (SnT)
* 	Nicolas Sannier (sannier@svv.lu),
*	Morayo Adedjouma (morayoade@gmail.com),
*	Virgil Tassan (tassan-zanin-caser@svv.lu),
*	Wei Dou (dou@svv.lu),
*	Mehrdad Sabetzadeh (sabetzadeh@svv.lu),
*	Lionel Briand (briand@svv.lu),
*/

public class GateGenericXMLWriter {
	
	public static BufferedWriter XMLWriter; 
	public GateAkomaWriter gaw = new GateAkomaWriter();
	public GateAkomaConverter gac = new GateAkomaConverter();
	private ArrayList<Integer> parsedHighLevelDivisionElementOffset;
	private ArrayList<Integer> parsedBasicElementOffset;
	private ArrayList<Integer> parsedSubdivisionElementOffset;
	
    //public ArrayList<String> highLevelElementTypes;
    //public ArrayList<String> subElementTypes;
    //public String basicElement;
    
    public static String document; // initialized in writeXML.jape
    
    public static ArrayList<Annotation> amendmentList;
    public static ArrayList<AnnotationSet> listAmendmentAnnotationSet;
    public static ArrayList<String> amendmentContentList;
	
	
	/**
	 * initializes some elements for the XMLWriter. In particular it creates the writer, and import the information from structure.json
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public void initXMLWriter() throws JsonParseException, JsonMappingException, IOException{
		FileOutputStream os = new FileOutputStream(Configuration.outputText);
		OutputStreamWriter w = new OutputStreamWriter(os, "UTF-8");
		XMLWriter = new BufferedWriter(w);
		
		parsedHighLevelDivisionElementOffset = new ArrayList<Integer>();
		parsedBasicElementOffset = new ArrayList<Integer>();
		parsedSubdivisionElementOffset = new ArrayList<Integer>();
		
		amendmentList = new ArrayList<Annotation>();
		listAmendmentAnnotationSet = new ArrayList<AnnotationSet>();
		amendmentContentList = new ArrayList<String>();
	}
	
	
	/**
	 * imports the annotation_Head (will not work for other type of headers) features into a external Map
	 * @param ann the annotation whose FeatureMap has to be imported to capture its list of features
	 * @return the list of features of the annotation into a map
	 */
	public Map<String, String> importFeatures(Annotation ann){
		//TODO rewrite to become more generic regarding common properties
		HashMap<String, String> features = new HashMap<String, String>();
		features.put("num", (String) ann.getFeatures().get("num"));
		features.put("title", (String) ann.getFeatures().get("title"));
		features.put("block", (String) ann.getFeatures().get("block"));
		features.put("id", (String) ann.getFeatures().get("id"));
		return features;
	}

	/**
	 * closes the act and the Akoma Ntoso elements (</body></act></akomaNtoso>)
	 * @param style
	 * @throws IOException
	 */
	public void closeXMLHeader(String style) throws IOException{
		if (Configuration.style.toLowerCase().contains("akoma")){gaw.writeAkomaClosing();
		}
	}
	
	public String openElement(String elementType, Map<String, String> features){
		if (Configuration.style.toLowerCase().contains("akoma")){return gac.openElement(elementType, features);}
		return "";
	}
	

	public String closeElement(String elementType) throws IOException{
		if (Configuration.style.toLowerCase().contains("akoma")){return	gac.closeElement(elementType);
		}
		return "";
	}
	
	public String processText(AnnotationSet inputDocument, Annotation element){
		if (Configuration.style.toLowerCase().contains("akoma")){return gaw.processText(inputDocument, element, false);}
		return "";
	}
	
	public void writeHeaders(String title,String preamble, AnnotationSet inputDocument) throws IOException{ // addition NSA 20190109 addint inputDocument for handling footnotes
		if (Configuration.style.toLowerCase().contains("akoma")){gaw.writeAkomaHeaders(title,preamble, inputDocument);}
	}
	
	public void writeClosing() throws IOException{
		if(Configuration.style.toLowerCase().contains("akoma")){gaw.writeAkomaClosing();}
	}
	
	/**
	 * will initiate the generation of the body of the XML document
	 * the highLevelElement root (e.g. act) should not be the starter since already written. Therefore it goes for the next one
	 * @param inputDocument
	 * @param elementSegment
	 * @param elementType
	 * @throws IOException
	 */
	public void writeDocument(AnnotationSet inputDocument, Annotation elementSegment, String elementType) throws IOException {
		// not the one that is called by the jape script currently, see the method with the inputAS annotation set only.
		
		// writing the interleaved basic elements that are before the first high-Level division element
		writeInterleavedElements(inputDocument, elementSegment, elementType);
		
		// going for the rest of the structure
		WordUtils.capitalize(elementType);
		int i = 1;
		while (i < Configuration.highLevelElementTypes.size() && inputDocument.getContained(elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset()).get(Configuration.highLevelElementTypes.get(i) + "_Segment").isEmpty()){
			i++;
		}
		// if there are high-level elements found in the text, go for writing the remaining high-level elements
		if (i<Configuration.highLevelElementTypes.size()){
			String highLevelElementType = Configuration.highLevelElementTypes.get(i);
			AnnotationSet highLevelElementSegments = inputDocument.getContained(elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset()).get(highLevelElementType + "_Segment");
			List<Annotation> elemList = new ArrayList<Annotation>(highLevelElementSegments);
			Collections.sort(elemList, new OffsetComparator());
			// Iterate through the different segments of the given type and write their contents
			Iterator<Annotation> elemIter = elemList.iterator();
			while (elemIter.hasNext()) {
				Annotation highLevelElemSegment = elemIter.next();
				writeHighLevelElement(inputDocument, highLevelElemSegment, highLevelElementType);
			}
		}
	}

	
	/**
	 * will initiate the generation of the body of the XML document
	 * the highLevelElement root (e.g. act) should not be the starter since already written. Therefore it goes for the next one
	 * @param inputDocument
	 * @throws IOException
	 */
	public void writeDocument(AnnotationSet inputDocument) throws IOException {
		// the method that is currently called by the generation jape script
		// getting the root segment
		String elementType = Configuration.highLevelElementTypes.get(0);
		// going for the rest of the structure
		WordUtils.capitalize(elementType);
		AnnotationSet rootSegments = inputDocument.get(elementType+"_Segment");
		if (rootSegments == null || rootSegments.isEmpty()){
			System.err.println("No root segment ("+elementType +")found in the annotations - unable to further perform the XML generation");
			return;
		}
		// capturing the (unique) root segment
		Annotation elementSegment = rootSegments.iterator().next();
		
		getAmendmentAnnotations(inputDocument, elementSegment);
		
		// writing the interleaved basic elements that are before the first high-Level division element
		writeInterleavedElements(inputDocument, elementSegment, elementType);
		
		int i = 1;
		while (i < Configuration.highLevelElementTypes.size() && inputDocument.getContained(elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset()).get(Configuration.highLevelElementTypes.get(i) + "_Segment").isEmpty()){
			i++;
		}
		// if there are high-level elements found in the text, go for writing the remaining high-level elements
		if (i< Configuration.highLevelElementTypes.size()){
			String highLevelElementType = Configuration.highLevelElementTypes.get(i);
			AnnotationSet highLevelElementSegments = inputDocument.getContained(elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset()).get(highLevelElementType + "_Segment");
			// getting all the annotations of the highest-possible level
			List<Annotation> elemList = new ArrayList<Annotation>(highLevelElementSegments);
			Collections.sort(elemList, new OffsetComparator());
			// Iterate through the different segments of the given type and write their contents
			Iterator<Annotation> elemIter = elemList.iterator();
			while (elemIter.hasNext()) {
				Annotation highLevelElemSegment = elemIter.next();
				writeHighLevelElement(inputDocument, highLevelElemSegment, highLevelElementType);
			}
		}
		getAnnotationsBack(inputDocument);
	}
	
	
	
	/**
	 * will look for intermediate elements (high level divisions and basic elements) that are written between two high-level divisions as introductory provisions
	 * from a given element type, it will look for the first lower high-level division coming in the span, and will look for intermediate elements between the start of the segment and the start of the new division inside an imbricated loop
	 * WARNING: risk that more than two layers of interleaved (inverted) elements may not be handled correctly (e.g., Liv > art > chap > tit. In that situation art and chap will be interchanged)
	 * 
	 * @param inputDocument the document to parse
	 * @param elementSegment the current segment to write
	 * @param elementType the current high level division of the segment
	 * @throws IOException
	 */
	public void writeInterleavedElements(AnnotationSet inputDocument, Annotation elementSegment, String elementType) throws IOException{
		// look for the first HL element segment in the span
		int i = Configuration.highLevelElementTypes.indexOf(elementType);
		i++;
		while (i < Configuration.highLevelElementTypes.size() && inputDocument.getContained(elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset()).get(Configuration.highLevelElementTypes.get(i) + "_Segment").isEmpty()){
			i++;
			}

		long start = 0;
		long end = 0;
		
		if (i<Configuration.highLevelElementTypes.size()){
			// getting the type of the first HL division found in the segment
			String highLevelElementType = Configuration.highLevelElementTypes.get(i);
			//System.out.println("writeIntroductoryBasicElements for element: " + elementType + " - Having found " + highLevelElementType + " as next subElement");
			// capturing the first HL element in the span
			AnnotationSet highLevelElementSegments = inputDocument.getContained(elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset()).get(highLevelElementType + "_Segment");
			List<Annotation> elemList = new ArrayList<Annotation>(highLevelElementSegments);
			Collections.sort(elemList, new OffsetComparator());
			Annotation firstElem = elemList.get(0);
			
			// getting the span where to look for interleaved HL elements
			start = elementSegment.getStartNode().getOffset();
			end =  firstElem.getStartNode().getOffset();
			//System.out.println("span to look for interleaved elements: " + start + " - " + end);
			
			i++;
			while (i < Configuration.highLevelElementTypes.size() && inputDocument.getContained(elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset()).get(Configuration.highLevelElementTypes.get(i) + "_Segment").isEmpty()){
				i++;
				}
			if (i<Configuration.highLevelElementTypes.size()){
				// getting the type of the next HL division found in the segment
				String interleavedHighLevelElementType = Configuration.highLevelElementTypes.get(i);
				//System.out.println("writeIntroductoryBasicElements for element: " + elementType + " - Having found " + highLevelElementType + " as next subElement");
				// capturing the interleaved HL element in the span
				AnnotationSet interleavedHighLevelElementSegments = inputDocument.getContained(start, end).get(interleavedHighLevelElementType + "_Segment");
				elemList = new ArrayList<Annotation>(interleavedHighLevelElementSegments);
				Collections.sort(elemList, new OffsetComparator());
				Iterator<Annotation> elemIter = elemList.iterator();
				while (elemIter.hasNext()) {
					Annotation highLevelElemSegment = elemIter.next();
					writeHighLevelElement(inputDocument, highLevelElemSegment, interleavedHighLevelElementType);
				}
			}
		}
		// no structure in the text, only articles -- the span is the entire <root>_Segment
		else{
			// getting the span where to look for interleaved basic elements
			start = elementSegment.getStartNode().getOffset();
			end =  elementSegment.getEndNode().getOffset();
		}
		//System.out.println("span to look for interleaved elements: " + start + " - " + end);			
		// catching the interleaved basic elements
		AnnotationSet basicElementSegments = inputDocument.getContained(start, end).get(Configuration.basicElement + "_Segment");
		List<Annotation> basicElemList = new ArrayList<Annotation>(basicElementSegments);
		Collections.sort(basicElemList, new OffsetComparator());
		//System.out.println("have found: " + basicElemList.size() + " interleaved basic elements");
		// writing the XML for each elements
		for (Annotation ann : basicElemList){
			writeBasicElement(inputDocument, ann, Configuration.basicElement);
		}
		
	}
	

	/**
	 * processes the "block" features within a header annotation. Especially, it will processes its cross-references
	 * @param inputDocument the global input document as GATE annotation set
	 * @param elementHeader the header containing the block feature to process
	 * @param features the map to process and modify
	 * @return
	 */
	public Map<String, String> processBlocks(AnnotationSet inputDocument, Annotation elementHeader){
		Map<String, String> features = importFeatures(elementHeader);
		// managing the potential CRs in the block elements
		// will change the content of the Jape feature that were imported by replacing them with the XML tags to introduce
		String blockFeature = features.get("block");
		if (blockFeature != null && !blockFeature.equals("")){
			// some initialization for debugging and logging 
			int crStart = 0; int crEnd = 0;
			String crText = "";
			try{
				// get the cross references in the alinea
				AnnotationSet myRefs = inputDocument.getContained(elementHeader.getStartNode().getOffset(), elementHeader.getEndNode().getOffset()).get("CrossReferences_ref");
				List<Annotation> refsList = new ArrayList<Annotation>(myRefs);
				Collections.sort(refsList, new OffsetComparator());
				// Look at the CR within the alinea to replace them with their resolution
				//bottom-up approach for changes so that the CR offsets are not affected
				for (Annotation myRef : refsList){			
					// capturing the text of the cross reference and its offsets
					crStart = myRef.getStartNode().getOffset().intValue();
					crEnd = myRef.getEndNode().getOffset().intValue();
					crText = GateGenericXMLWriter.document.substring(crStart, crEnd);
					//System.out.println("processing cross-reference between offset "+ crStart + " and " + crEnd + " -- " + crText);			
					// building the cross reference resolution via the "eli" features in the cross reference
					String eli = (String) myRef.getFeatures().get("eli");
					String XMLRef =   gaw.writeAkomaCrossReference(crText, Configuration.serverRoot+eli);
					blockFeature = blockFeature.replace(crText,XMLRef);
				}
				features.put("block", blockFeature);
			}
			catch(Exception e){
				System.err.println("Something went wrong while processing cross references in header's block -- CR: " + crText + " at offsets: " + crStart +" - " + crEnd);
				e.printStackTrace();
			}
		}
		return features;
	}
	
	
	/**
	 * Handles the XML generation of high-level divisions
	 * goes for intermediate elements (elements in-between two consecutive high level-divisions before going for the recursion for the subdivisions and basic elements
	 * @param inputDocument the global input document as GATE annotation set
	 * @param elementSegment the structure segment annotation (e.g., 'Book_segment' or 'Chapter_segment' as reported in Configuration.highLevelElementTypes)
	 * @param elementType the corresponding type (e.g., Book or Chapter, such as in Configuration.highLevelElementTypes)
	 * @throws IOException to handle issues when writing the XML code
	 */
	public void writeHighLevelElement(AnnotationSet inputDocument, Annotation elementSegment, String elementType) throws IOException {
		//get the header of the element
		WordUtils.capitalize(elementType);
		AnnotationSet elementHeaders = inputDocument.get(elementType + "_Head", elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset());
		Annotation elementHead = elementHeaders.iterator().next();
		
		int startElem = elementHead.getStartNode().getOffset().intValue();

		//process the cross references in the element features
		Map<String, String> features = processBlocks(inputDocument, elementHead);
		
		// begin addition NSA 20180607 for handling repetition for HL elements
		if (parsedHighLevelDivisionElementOffset.indexOf(startElem) == -1){
			parsedHighLevelDivisionElementOffset.add(startElem);
		// end addition NSA 20180607 for handling repetition for HL elements

			// writing the XML element header
			XMLWriter.write(openElement(elementType,features));
			XMLWriter.newLine();
			
			// writing the interleaved basic elements
			writeInterleavedElements(inputDocument, elementSegment, elementType);
			
			// writing the rest of HL elements
			// getting all the annotations of subdivision (the first level that is not empty) within a given span)
			int i = Configuration.highLevelElementTypes.indexOf(elementType);
			//System.out.println("looking for the index of " + elementType + " in the high-level elements lists - position is: " + i);
			i++;
			while (i < Configuration.highLevelElementTypes.size() && inputDocument.getContained(elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset()).get(Configuration.highLevelElementTypes.get(i) + "_Segment").isEmpty()){
				i++;
				}
			//System.out.println("next subdivision of " + elementType + " in the high-level elements lists - position is: " + i);
			if (i<Configuration.highLevelElementTypes.size()){
				// we have found a subdivision segment
				String highLevelElementType = Configuration.highLevelElementTypes.get(i);
				AnnotationSet highLevelElementSegments = inputDocument.getContained(elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset()).get(highLevelElementType + "_Segment");
				List<Annotation> elemList = new ArrayList<Annotation>(highLevelElementSegments);
				Collections.sort(elemList, new OffsetComparator());
				// Iterate through the different segments of the given type and write their contents
				Iterator<Annotation> elemIter = elemList.iterator();
				while (elemIter.hasNext()) {
					Annotation highLevelElemSegment = elemIter.next();
					writeHighLevelElement(inputDocument, highLevelElemSegment, highLevelElementType);
				}
			}
			else{
				// write the contained basic element if no high level divisions elements are here
				AnnotationSet basicElementSegments = inputDocument.getContained(elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset()).get(Configuration.basicElement + "_Segment");
				List<Annotation> basicElemList = new ArrayList<Annotation>(basicElementSegments);
				Collections.sort(basicElemList, new OffsetComparator());
				for (Annotation ann : basicElemList){
					writeBasicElement(inputDocument, ann, Configuration.basicElement);
				}
			}
			// writes the closing tags for the element
			XMLWriter.write(closeElement(elementType));
			XMLWriter.newLine();
			XMLWriter.flush();
		
		// added code NSA 20180607 to handle repetitions of HL divisions
		}
		// for debugging only
		//else {
		//	System.out.println("High-level division already written: " + features.get("num") + " at offset: " + startElem);
		//}
		// added code NSA 20180607 to handle repetition of HL divisions
	}

	
	/**
	 * checks that the basic element has the same spelling than in the structure,
	 * checks that it has not been already treated (test on the header's offset), 
	 * writes the basic element XML tags, 
	 * write the basic element subdivisions.
	 * @param inputDocument
	 * @param elementSegment
	 * @param elementType
	 * @throws IOException
	 */
	public void writeBasicElement(AnnotationSet inputDocument, Annotation elementSegment, String elementType) throws IOException {
		//get the header of the element
		WordUtils.capitalize(elementType);
		AnnotationSet elementHeaders = inputDocument.get(elementType + "_Head", elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset());
		
		// To get the first basic element
		List<Annotation> elementHeadersList = new ArrayList<Annotation>(elementHeaders);
		Collections.sort(elementHeadersList, new OffsetComparator());
		Annotation elementHead = elementHeadersList.iterator().next();

		//process the cross references in the element features
		Map<String, String> features = processBlocks(inputDocument, elementHead);
		
		// writing the XML element header
		// verify that the segment (detected by its starting offset), was not already handled
		int startElem = elementHead.getStartNode().getOffset().intValue();
		
		if (parsedBasicElementOffset.indexOf(startElem) == -1){
			parsedBasicElementOffset.add(startElem);
			// writes the XML opening tags related to the structure element
			XMLWriter.write(openElement(elementType,features));
			XMLWriter.newLine();
			// writes the basic element's subdivisions
			// getting all the annotations of subdivision (the first level that is not empty) within a given span)
			int i = 0;
			while (i < Configuration.subElementTypes.size() && inputDocument.getContained(elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset()).get(Configuration.subElementTypes.get(i) + "_Segment").isEmpty()){
				i++;
				}
			if (i>=Configuration.subElementTypes.size()){
				// in case of error, warn and return the textual content of the segment
				int elemStart = elementSegment.getStartNode().getOffset().intValue();
				int elemEnd = elementSegment.getEndNode().getOffset().intValue();
				String workAround = document.substring(elemStart, elemEnd);
				XMLWriter.write(workAround);
				System.err.println("Unable to find a subdvision segment in "+Configuration.basicElement.toLowerCase()+": " + features.get("num") + " at offset: " +startElem + "\n"
						+ "The following text will be inserted as-is in the XML document: \n" + workAround);
			}
			else{
				String subElementType = Configuration.subElementTypes.get(i);
				AnnotationSet subElementSegments = inputDocument.getContained(elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset()).get( subElementType + "_Segment");
				List<Annotation> elemList = new ArrayList<Annotation>(subElementSegments);
				Collections.sort(elemList, new OffsetComparator());
				// Iterate through the different segments of the given type and write their contents
				Iterator<Annotation> elemIter = elemList.iterator();
				while (elemIter.hasNext()) {
					Annotation subElemSegment = elemIter.next();
					writeSubdivision(inputDocument, subElemSegment, subElementType);
				}
			}
			// writes the closing tags for the element
			XMLWriter.write(closeElement(elementType));
			XMLWriter.newLine();
			XMLWriter.flush();
		}
		// for debugging only
		//else{
			//System.out.println("Basic element already written: " + features.get("num") + " at offset: " + startElem);
		//}
	}


	/**
	 * checks that the subdivision element has the same spelling than in the structure,
	 * writes the subdivision element XML tags, 
	 * goes into the recursion for other subdivision or, if last, go for the content with the call to processText
	 * @param inputDocument
	 * @param elementSegment
	 * @param elementType
	 * @throws IOException
	 */
	public void writeSubdivision(AnnotationSet inputDocument, Annotation elementSegment, String elementType) throws IOException {
		//get the header of the element
		WordUtils.capitalize(elementType);
		AnnotationSet elementHeaders = inputDocument.get(elementType + "_Head", elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset());
		Annotation elementHead = elementHeaders.iterator().next();

			// process the cross references in the element features
			Map<String, String> features = processBlocks(inputDocument,elementHead);

			// get the feature from the element_head annotation
			XMLWriter.write(openElement(elementType, features));
			// writing the rest of subdivisions elements
			// getting all the annotations of subdivision (the first level that is not empty) within a given span)
			int i = Configuration.subElementTypes.indexOf(elementType);
			// System.out.println("looking for the index of " + elementType " in the subdivision elements lists - position is: " + i);
			i++;
			while (i < Configuration.subElementTypes.size() && inputDocument.getContained(elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset()).get(Configuration.subElementTypes.get(i) + "_Segment").isEmpty()) {
				i++;
			}
			// System.out.println("next subdivision of " + elementType + " in the subdivision elements lists - position is: " + i);
			if (i < Configuration.subElementTypes.size()) {
				// we have found a subdivision segment
				String subdivisionElement = Configuration.subElementTypes.get(i);
				AnnotationSet subdivisionElementSegments = inputDocument.getContained(elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset()).get(subdivisionElement + "_Segment");
				List<Annotation> elemList = new ArrayList<Annotation>(subdivisionElementSegments);
				Collections.sort(elemList, new OffsetComparator());
				// Iterate through the different segments of the given type and write their contents
				Iterator<Annotation> elemIter = elemList.iterator();
				while (elemIter.hasNext()) {
					Annotation subdivisionElementSegment = elemIter.next();
					writeSubdivision(inputDocument, subdivisionElementSegment, subdivisionElement);
				}
			} else {
				// write the contained element if no more subdivisions elements are here
				AnnotationSet lastElementSegments = inputDocument.getContained(elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset()).get(elementType + "_Segment");
				List<Annotation> elemList = new ArrayList<Annotation>(lastElementSegments);
				Collections.sort(elemList, new OffsetComparator());
				for (Annotation elem : elemList) {
					// verify that the segment (detected by its starting offset), was not already handled
					int startElem = elem.getStartNode().getOffset().intValue();
					if (parsedSubdivisionElementOffset.indexOf(startElem) == -1) {
						parsedSubdivisionElementOffset.add(startElem);
						XMLWriter.write(processText(inputDocument, elem).trim());
					}
					// for debugging only
					//else{
					//	System.out.println("subdivision element already written at offset: " + startElem);
					//}
				}
			}
			// writes the closing tags for the element
			XMLWriter.write(closeElement(elementType));
			XMLWriter.newLine();
			XMLWriter.flush();
	}
	
	/**
	 * get all the amendments in the text
	 * @param inputDocument
	 * @param elementSegment
	 */
	private void getAmendmentAnnotations(AnnotationSet inputDocument, Annotation elementSegment){
		long amendStart;
		long amendEnd;
		Annotation amendment;
		
		AnnotationSet amendments = inputDocument.getContained(elementSegment.getStartNode().getOffset(), elementSegment.getEndNode().getOffset()).get("Amendment_Segment");
//		inputDocument.get(arg0, arg1);
		amendmentList= new ArrayList<Annotation>(amendments);
		Collections.sort(amendmentList, new OffsetComparator());
		
		//get the span of the annotationSet for the amendments
		for(int i=0; i< amendmentList.size(); i++){
			amendment = amendmentList.get(i);
			amendStart = amendment.getStartNode().getOffset();
			amendEnd = amendment.getEndNode().getOffset();
			listAmendmentAnnotationSet.add(inputDocument.getContained(amendStart, amendEnd));
		
		}

		AnnotationSet amendmentAS;
	
		for(int i=0; i< listAmendmentAnnotationSet.size(); i++){
			amendmentAS = listAmendmentAnnotationSet.get(i);
			amendStart = amendmentAS.firstNode().getOffset();
			amendEnd = amendmentAS.lastNode().getOffset();
			
			//remove all the annotations contained by the amendment segment
			AnnotationSet toRemove = inputDocument.getContained((long)amendStart, (long)amendEnd);
			inputDocument.removeAll(toRemove); 
			System.out.println("Amendment "+i+" : "+amendStart+" "+amendEnd);
			//put back the amendment_segments themselves to be able to detect them in alinea
			try {
				inputDocument.add(amendStart, amendEnd, "Amendment_Segment", Factory.newFeatureMap());
			} catch (InvalidOffsetException e) {
				e.printStackTrace();
			}
			
			GateAmendmentWriter amendmentWriter = new GateAmendmentWriter();
			amendmentContentList.add(amendmentWriter.GenerateAmendmentXML(amendmentAS,amendmentList.get(i)));		
		}
	}
	
	
	private void getAnnotationsBack(AnnotationSet inputDocument){
		AnnotationSet amendment;
		for(int i=0; i< listAmendmentAnnotationSet.size(); i++){
			amendment = listAmendmentAnnotationSet.get(i);
			ArrayList<Annotation> annotationList = new ArrayList<Annotation>(amendment);
			for(Annotation ann : annotationList){
				if(!ann.getType().equals("Amendment_Segment")){
					inputDocument.add(ann);
				}

			}

		}
	}
	
}
