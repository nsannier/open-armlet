package gateStandAlone;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;


import org.apache.commons.lang3.text.WordUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;


import japescriptgen.DocumentNode;
import japescriptgen.DocumentStructure;

/**	Copyright (c) {2014-2019}, University of Luxembourg, Interdisciplinary Centre for Security, Reliability and Trust (SnT)
* 	Nicolas Sannier (sannier@svv.lu),
*	Morayo Adedjouma (morayoade@gmail.com),
*	Virgil Tassan (tassan-zanin-caser@svv.lu),
*	Wei Dou (dou@svv.lu),
*	Mehrdad Sabetzadeh (sabetzadeh@svv.lu),
*	Lionel Briand (briand@svv.lu),
*/

public class Configuration {
	public static String inputText;
	public static String serverRoot;
	public static String rootELI;
	public String title;
	public static String outputText;
	public String logFile;
	public String errorFile;
	public static String longTitle;
	public static boolean showUI;
	public String structure;
	public String header;
	public String jsonRules;
	public static String style;
	
	//TODO nothing in static
	
	//ExcludedRules
	public HashMap<String,String> excludedMapRules = new HashMap<String,String>();

	public final ObjectMapper objectMapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
    public DocumentStructure documentStructure;

    public static ArrayList<String> highLevelElementTypes;
    public static ArrayList<String> subElementTypes;
    public static String basicElement;
	
	public static Set<String> excludedRules = new HashSet<String>();
	
	public static Set<String> usedRules = new HashSet<String>();
	
	public static Set<String> overLappingRules = new HashSet<String>();

	/**
	 * build configuration based on config file
	 * @param path The path to the config file
	 */
	public Configuration(String path){
		readConfiguration(path);
	}
	
	/**
	 * build lighter configuration. use for embedded apps
	 * @param elemConfig
	 */
	public Configuration(String[] elemConfig){
		inputText = elemConfig[0];
		outputText  = elemConfig[1];
		logFile = elemConfig[2];
		errorFile = elemConfig[3];
		jsonRules = elemConfig[4];
		serverRoot = elemConfig[5];
		rootELI = elemConfig[6];
		
		style = "akoma";
		
		showUI = false;
	}
	
	/**
	 * reads the configuration to initialize all variables
	 * structure and header_rules can be specified in two ways:
	 * 		the keyword structure for the structure and the keyword header for the header_rules
	 * 		the keyword jsonRules to specify the directory with both files but they have to be written with _structure.json for structure and _rules.json for header rules
	 * @param path the path for the configuration file
	 */
	public void readConfiguration(String path){
		try{
			// Création du flux bufférisé sur un FileReader, immédiatement suivi par un 
			// try/finally, ce qui permet de ne fermer le flux QUE s'il le reader
			// est correctement instancié (évite les NullPointerException)
			FileInputStream is = new FileInputStream(path);
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader buff = new BufferedReader(isr);
			
			// need to initialize showUI may raise issue if not present otherwise
			showUI = false;
			
			try {
				String line;
				// Lecture du fichier ligne par ligne. Cette boucle se termine
				// quand la méthode retourne la valeur null.
				while ((line = buff.readLine()) != null) {
					if (line.contains("file")){
						inputText = line.split("=")[1].trim();
					}
					if (line.contains("shortTitle")){
						this.title = line.split("=")[1].trim();
					}
					if (line.contains("rootELI")){
						rootELI = line.split("=")[1].trim();
					}
					if (line.contains("output")){
						outputText= line.split("=")[1].trim();
					}
					if (line.contains("log")){
						this.logFile = line.split("=")[1].trim();
					}
					if (line.contains("error")){
						this.errorFile = line.split("=")[1].trim();
					}
					if (line.contains("longTitle")){
						longTitle = line.split("=")[1].trim();
					}
					if (line.contains("showUI")){
						showUI = Boolean.parseBoolean(line.split("=")[1].trim());
					}
					if (line.contains("structure")){
						this.structure = line.split("=")[1].trim();
					}
					if (line.contains("header")){
						this.header = line.split("=")[1].trim();
					}
					if (line.contains("jsonRules")){
						this.jsonRules = line.split("=")[1].trim();
					}
					if (line.contains("serverRoot")){
						serverRoot = line.split("=")[1].trim();
					}
					if (line.contains("style")){
						style = line.split("=")[1].trim();
					}
					if(line.contains("excluded_rules")){
						String[] lineSplitted = line.split("=");
						String elementStructure = lineSplitted[0].trim();
						elementStructure=elementStructure.replace("excluded_rules_","");
						this.excludedMapRules.put(elementStructure, lineSplitted[1].trim());
					}

				}
			} finally {
				// dans tous les cas, on ferme nos flux
				buff.close();
				if (inputText == null || inputText.equals("")){
					System.err.println("a correct Input file address should be provided for the text to parse");
					System.exit(1);
				}
				if (rootELI == null || rootELI.equals("")){
					System.err.println("a correct ELI ressource noame should be provided for the text");
					System.exit(1);
				}
				if (this.title == null || this.title.equals("")){
					System.out.println("Warning: No (short) title specified, default name will be empty");
					this.title = "";
				}
				if (outputText == null || outputText.equals("")){
					System.out.println("Warning: No output document specified, default name will be "+ ConfigTool.DEFAULT_XML);
					outputText = ConfigTool.DEFAULT_XML;
				}
				if (this.logFile == null || this.logFile.equals("")){
					System.out.println("Warning: No Log file specified, default name will be " + ConfigTool.DEFAULT_LOG);
					this.logFile = ConfigTool.DEFAULT_LOG;
				}
				if (this.errorFile == null || this.errorFile.equals("")){
					System.out.println("Warning: No Error log file specified, default name will be " + ConfigTool.DEFAULT_ERR);
					this.errorFile = ConfigTool.DEFAULT_ERR;
				}
				if (longTitle == null || longTitle.equals("")){
					System.out.println("Warning: No long title specified, default name will be the short title");
					longTitle = this.title;
				}
				if (serverRoot == null || serverRoot.equals("")){
					System.out.println("Warning: serverRoot is not specified, legilux will be the default root ");
					serverRoot = "http://eli.legilux.public.lu/";
				}
				if (this.structure == null || this.structure.equals("")){
					System.out.println("Warning: no text schema specified - default structure will be used");
					this.structure = ConfigTool.DEFAULT_STRUCTURE_PATH;
				}
				if (this.header == null){this.header="";}
				if (this.header.equals("") && !this.structure.equals(ConfigTool.DEFAULT_STRUCTURE_PATH)){
					System.err.println("a file describing the rules for the headers is required. \n"
							+ "Please complete the configuration file with the line: \"header = <path_to_the_header_file>_rules.json\"");
					System.exit(1);
				}
				if (this.jsonRules == null || this.jsonRules.equals("")){
					System.out.println("Warning: no text schema specified - default structure will be used");
					this.jsonRules = ConfigTool.DEFAULT_JSONRULES_PATH;
				}
				if (style == null || style.equals("")){
					System.out.println("Warning: no XML schema specified - default will be Akoma Ntoso");
					style = "akoma";
				}
				if (showUI){
					System.out.println("DEBUG: GateUI will be shown, messages will continue in GATE UI and log/err files");
				}
				
				if (!this.structure.equals(ConfigTool.DEFAULT_STRUCTURE_PATH)){
					String structureName = this.structure.replace("_structure.json", "");
					String headerName = this.header.replace("_rules.json", "");
					if(!structureName.equals(headerName)){
						System.err.println("ERROR: the file containing the structure and the file containing the rules for the header must be named following the rule : <name>_structure.json and <name>_rules.json \n"
							+ "Please correct the configuration file so that structure and header share the same root");
						System.exit(1);
					}
				}	
		
				
				//get the structure and the rules from jsonRules attribute
				configurateHeaderAndStructure();
				
				//Exclude some rules for avoid overlapping
				generateListOfExcludedRules();
				
				highLevelElementTypes = getHighLevelELements(new File(structure));
				//System.out.println(highLevelElementTypes);
				basicElement = getBasicElement(new File(structure));
				//System.out.println(basicElement);
				subElementTypes = getSubDivisionELements(new File(structure));
				//System.out.println(subElementTypes);	
			}
		} 
		catch (Exception e) {
			System.err.println("Something went wrong during the import of the configuration file - see below");
			e.printStackTrace();
		}
	}

	/**
	 * reads structure.json file and convert the highLevel division part into an ArrayList
	 * @param structureJsonFile the configuration file stating the structural organization of the text
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	public ArrayList<String> getHighLevelELements(File structureJsonFile) throws JsonParseException, JsonMappingException, IOException{
		assert(structureJsonFile.exists());
		documentStructure = objectMapper.readValue(structureJsonFile, DocumentStructure.class);
		DocumentNode highLevelDivisions = documentStructure.highLevelDivisionElements;

		ArrayList<String>elementList = new ArrayList<String>();
		DocumentNode node = highLevelDivisions;
		// putting the root element in the element list
		 while (node.child != null) {
			 elementList.add(WordUtils.capitalize(node.name));
			 node=node.child;
		 }
		 // capturing the last element
		 elementList.add(WordUtils.capitalize(node.name));
		 System.out.println("high-level divisions: " + elementList.toString());
		 return elementList;
	}
	
	/**
	 * get the json structure file and the json rules file based on the directory
	 */
	void configurateHeaderAndStructure() {
		if(this.jsonRules!=null && this.jsonRules!="" && new File(this.jsonRules).exists()){
			File directory = new File(this.jsonRules);
			File[] fList = directory.listFiles();
			System.out.println(directory+"  FILES : ");
			if(fList!=null){
				for(File f : fList){
					if(f.getName().contains("_rules.json")){
						this.header = this.jsonRules+"/"+f.getName();
						System.out.println(f);
					}
					if(f.getName().contains("_structure.json")){
						this.structure = this.jsonRules+"/"+f.getName();
						System.out.println(f);
					}
				}
			}else{
				System.out.println("Warning : No jsonRules folder given!");
			}
		}else{
			System.out.println("Warning : No jsonRules folder given!");
		}
			
		
	}
	
	/**
	 * reads structure.json file and convert the subdivision part into an ArrayList
	 * @param structureJsonFile the configuration file stating the structural organization of the text
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	public ArrayList<String> getSubDivisionELements(File structureJsonFile) throws JsonParseException, JsonMappingException, IOException{
		assert(structureJsonFile.exists());
		documentStructure = objectMapper.readValue(structureJsonFile, DocumentStructure.class);
		DocumentNode subDivisions = documentStructure.subDivisionElements;

		ArrayList<String>elementList = new ArrayList<String>();
		DocumentNode node = subDivisions;
		// putting the root element in the element list
		 while (node.child != null) {
			 elementList.add(WordUtils.capitalize(node.name));
			 node=node.child;
		 }
		 // capturing the last element
		 elementList.add(WordUtils.capitalize(node.name));
		 System.out.println("subdivisions: " + elementList.toString());
		 return elementList;
	}
	
	public String displayConfiguration(){
		String res ="";
		res += "\nInputText: "+ inputText;
		res += "\nServerRoot: "+ serverRoot;
		res += "\nRootEli: "+ rootELI;
		res += "\nTitle: "+ title;
		res += "\nOutputText: "+ outputText;
		res += "\nLogFile: "+ logFile;
		res += "\nErrorFile: "+ errorFile;
		res += "\nLongTitle: "+ longTitle;
		res += "\nShowUI: "+ showUI;
		res += "\nStructure: "+ structure;
		res += "\nHeader: "+ header;
		res += "\nJsonRules: "+ jsonRules;
		res += "\nStyle: "+ style;
		return res;
	}
	
	/**
	 * reads structure.json file and return the name of the basic element
	 * @param structureJsonFile the configuration file stating the structural organization of the text
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	public String getBasicElement(File structureJsonFile) throws JsonParseException, JsonMappingException, IOException{
		assert(structureJsonFile.exists());
		DocumentNode basicNode = documentStructure.basicElements;
		String basicElement = WordUtils.capitalize(basicNode.name); 
		System.out.println("basicNode: " + basicElement);
		return basicElement;
	}
	
	public void generateListOfExcludedRules(){
		for(Entry<String, String> entry: this.excludedMapRules.entrySet()){
			//Convert first letter to an uppercase
			String key = entry.getKey();
			key = key.substring(0,1).toUpperCase() + key.substring(1).toLowerCase();
			
			String val = entry.getValue();
			String[] rules = val.split(",");
			for(int i=0; i<rules.length;i++){
				String rule = rules[i].replaceAll("\\s+","");
				excludedRules.add(key+rule+"Head");
			}
		}
		System.out.println("RULES : "+excludedRules);
	}
	
	//Write the overlapping rules and used rules in an output file
	//Remind the excluded rules given in input
	public static void writeInfoUser() throws IOException{
		FileOutputStream os = null;
		OutputStreamWriter osr = null;
		BufferedWriter buffWriter = null;
		try{
			File configFile = new File(inputText);
			os = new FileOutputStream(configFile.getParent()+"/infoRules.txt");
			osr = new OutputStreamWriter(os);
			buffWriter = new BufferedWriter(osr);
			buffWriter.write("USED RULES:\n");
			//get the list of used rules for the given text
			List<String> ruleList = new ArrayList<String>(usedRules);
			Collections.sort(ruleList);
			for(String s: ruleList){
				buffWriter.write("\t"+s+"\n");
			}
			buffWriter.write("OVERLAPPING RULES:\n");
			//get the list of overlapping rules
			List<String> overlapList = new ArrayList<String>(overLappingRules);
			Collections.sort(overlapList);
			for(String s: overlapList){
				buffWriter.write("\t"+s+"\n");
			}
			buffWriter.write("EXCLUDED RULES:\n");
			//get the list of excluded rules given in output
			List<String> excludedList = new ArrayList<String>(excludedRules);
			Collections.sort(excludedList);
			for(String s: excludedList){
				buffWriter.write("\t"+s+"\n");
			}
		} catch (IOException e) {
		    e.printStackTrace();
		    System.err.println(os+"\n"+osr+"\n"+buffWriter);
		} finally {
		    if (buffWriter != null)
		    	buffWriter.close();
		}
	}

	@Override
	public String toString() {
		return "Configuration [title=" + title + ", logFile=" + logFile + ", errorFile=" + errorFile + ", jsonRules="
				+ jsonRules + "]";
	}


	
	
	
}
