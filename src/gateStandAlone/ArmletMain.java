package gateStandAlone;

import gate.Corpus;
import gate.CorpusController;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;
import gate.Gate;
import gate.creole.ExecutionException;
import gate.creole.ResourceInstantiationException;
import gate.creole.SerialAnalyserController;
import gate.gui.MainFrame;
import gate.persist.PersistenceException;
import gate.util.GateException;
import gate.util.persistence.PersistenceManager;
import japescriptgen.JapeScriptsGenerator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import javax.swing.SwingUtilities;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;

/**	Copyright (c) {2014-2019}, University of Luxembourg, Interdisciplinary Centre for Security, Reliability and Trust (SnT)
* 	Nicolas Sannier (sannier@svv.lu),
*	Morayo Adedjouma (morayoade@gmail.com),
*	Virgil Tassan (tassan-zanin-caser@svv.lu),
*	Wei Dou (dou@svv.lu),
*	Mehrdad Sabetzadeh (sabetzadeh@svv.lu),
*	Lionel Briand (briand@svv.lu),
*/

public class ArmletMain {

	/**
	 * GateScriptManager linked to the software
	 */
	static GateScriptManager gsm;

	//Path for files
	private String headerRuleFilePath;
	private String japeScriptsPath;
	private String structureFilePath;
	
	/**
	 * the configuration process
	 */
	public Configuration configuration;
	
    /**
     * the pipeline of the several documents' path
     */
	private ArrayList<String> pipeline;
	
	/**
	 * Contains the current executed Gate Application as java object
	 */
	private CorpusController controller;
	
	/**
	 * Contains all Gate Applications in the software
	 */
	public Map<String,SerialAnalyserController> controllerCollection;

	/**
	 * TODO use it or remove it
	 */
	private static Logger logger = Logger.getLogger("logger");
	
	/**
	 * the multiplicity determines the mode of the execution : single document or several documents
	 */
	public String multiplicity;

	/**
	 * Initializes path for needed files based on the name of the used structure
	 * @param structureName the name of the structure used
	 */
    public void readStructure(String structureName){
    	
    	// TODO get structure path from folder 
    	structureFilePath = String.format(ConfigTool.standardCustomStructurePath, structureName);
    	// TODO get jape path from folder 
    	japeScriptsPath = String.format(ConfigTool.standardJapeStructurePath, structureName);	
    	headerRuleFilePath = configuration.header;
    }
	
	/**
	 * will dynamically change the build.properties file in order to the add the java classes in the current project in the class loading 
	 * @throws IOException
	 */
	public void changeBuildProperties() throws IOException{
		FileOutputStream os = new FileOutputStream(ConfigTool.gatePathBuildProperties);
		OutputStreamWriter osw = new OutputStreamWriter(os);
		BufferedWriter bw = new BufferedWriter(osw);
		
		File f = new File("");
		String path = f.getAbsolutePath()+"/bin";
		System.out.println("Classpath location: " + path);
		bw.write("run.gate.class.path=" + path);
		bw.close();
	}
	
	/**
	 * Initializes Gate parameters (gate-xml, plug-ins location, classpath (not useful), if the UI should be shown or not)  and loads the application scripts
	 * @throws IOException
	 * @throws GateException
	 */
	public void initGate() throws IOException, GateException{
		System.setErr(new PrintStream(new FileOutputStream(FileDescriptor.err)));
		System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
		// init gate
		System.out.println("*** Initializing GATE *** ");
		Gate.setGateHome(new File(ConfigTool.setGateHome));
		System.out.println("Gate Home location: " + Gate.getGateHome().getAbsolutePath());
		Gate.setSiteConfigFile(new File(ConfigTool.setSiteConfigFile));
		Gate.setPluginsHome(new File(ConfigTool.setPluginsHome));
		System.out.println("Gate plugins location: " + Gate.getPluginsHome().getAbsolutePath());
		changeBuildProperties(); // useful to set up for using Gate Developer, which needs to have access to the java classes, not useful otherwise, when called from the Jar

		Scanner reader = new Scanner(System.in);  // Reading from System.in
		System.out.println("Show Gate Developper User Interface?");
		String answer = reader.next();
		if (answer.toLowerCase().equals("true") || answer.toLowerCase().equals("t") || answer.toLowerCase().equals("yes") || answer.toLowerCase().equals("y") || answer.toLowerCase().equals("oui") || answer.toLowerCase().equals("o")){
			Configuration.showUI = true;
		} else {
			Configuration.showUI = false;
		}
		System.out.println("The provided file is a text configuration file (press 1), a pipeline of configuration files (press 2) or a pipeline of configuration files using multithreading (press 3) -- exit (hit any other keys)");
		// Reading from System.in
		multiplicity = reader.next();
		reader.close();
		if (!multiplicity.equals("1") && !multiplicity.equals("2") && !multiplicity.equals("3")){
			System.out.println("*** Exiting GateStandAlone application due to wrong input***");
			System.exit(0);
		}
		// initializing Gate and depending on the choice, launching the Gate UI
		Gate.init();
		if (Configuration.showUI){
			try {
				showGateUI();
			} catch (InvocationTargetException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	/**
	 * Initializes Gate parameters (gate-xml, plug-ins location, classpath (not useful), if the UI should be shown or not)  and loads the application scripts
	 * @throws IOException
	 * @throws GateException
	 */
	public void initGateForSCLEmbedded() throws IOException, GateException{
		System.setErr(new PrintStream(new FileOutputStream(FileDescriptor.err)));
		System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
		// init gate
		System.out.println("*** Initializing GATE *** ");
		Gate.setGateHome(new File(ConfigTool.setGateHome));
		System.out.println("Gate Home location: " + Gate.getGateHome().getAbsolutePath());
		Gate.setSiteConfigFile(new File(ConfigTool.setSiteConfigFile));
		Gate.setPluginsHome(new File(ConfigTool.setPluginsHome));
		System.out.println("Gate plugins location: " + Gate.getPluginsHome().getAbsolutePath());
		changeBuildProperties(); // useful to set up for using Gate Developer, which needs to have access to the java classes, not useful otherwise, when called from the Jar

		Configuration.showUI = false;
		
		// initializing Gate and depending on the choice, launching the Gate UI
		Gate.init();
		
	}
	
	/**
	 * 	
	 * when executed Gate UI will be visible
	 * @throws InvocationTargetException
	 * @throws InterruptedException
	 */
	protected void showGateUI() throws InvocationTargetException, InterruptedException {
		SwingUtilities.invokeAndWait(new Runnable() {
			public void run() {
				MainFrame.getInstance().setVisible(true);
			}
		});
	}
	

	
	
	/**
	 * Reads the configuration file to extract the path to the file to import
	 * set the features for the documents (especially its format (.doc, .docx, .txt only are admitted) and encoding)
	 * add the text to the corpus to be processed
	 * 
	 * @param configFile the configuration file that contains the different parameters, including the path of the the text to process
	 * @return
	 */
	public Corpus setCorpus(){	
//		Configuration.readConfiguration(configFile);	
		System.out.println("*** Importing the document to process ***");
		try {
			// importing the document to parse
			File docFile = new File(configuration.inputText);
			FeatureMap params = Factory.newFeatureMap();
			params.put(Document.DOCUMENT_URL_PARAMETER_NAME, docFile.toURL());
			params.put(Document.DOCUMENT_ENCODING_PARAMETER_NAME, "utf-8");
			String fileExtension = FilenameUtils.getExtension(configuration.inputText);
			if (fileExtension.equals("doc")) {
				params.put(Document.DOCUMENT_MIME_TYPE_PARAMETER_NAME, "application/msword");
			} else if (fileExtension.equals("docx")) {
				params.put(Document.DOCUMENT_MIME_TYPE_PARAMETER_NAME, "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
			} else if (fileExtension.equals("txt")) {
				params.put(Document.DOCUMENT_MIME_TYPE_PARAMETER_NAME, "text/plain");
			} else {
				System.err.println("The file to process has an incorrect format (ideally: .txt, otherwise, .doc/.docx)");
				System.exit(1);
			}
			System.out.println("Mime :"+Document.DOCUMENT_MIME_TYPE_PARAMETER_NAME+" "+params.get(Document.DOCUMENT_MIME_TYPE_PARAMETER_NAME));
			FeatureMap feats = Factory.newFeatureMap();
			Document doc = (Document) Factory.createResource("gate.corpora.DocumentImpl", params, feats);
	
			// preparing the corpus (one text file imported from the config file)
			Corpus corpus = Factory.newCorpus("LegalCorpus");
			corpus.setName("ARMLET corpus");
			corpus.add(doc);
			return corpus;
		} catch (Exception e) {
			System.err.println("Something went wrong in the creating of the corpus. See below:");
			e.printStackTrace();
			System.exit(1);
		}
		return null;
	}
	

	/**
	 * Run the software with the given text
	 * @param corpus the corpus containing the text to process
	 * @throws JSONException 
	 * @throws IOException 
	 * @throws PersistenceException 
	 * @throws ResourceInstantiationException
	 * @throws MalformedURLException
	 * @throws FileNotFoundException
	 * @throws ExecutionException
	 */
	public void processSingleText(Corpus corpus) throws PersistenceException, ResourceInstantiationException, IOException, JSONException{
		controller.setCorpus(corpus);
		// executing the application over the corpus
		System.out.println("*** Executing the application ***");
		// executing the app over the corpus
		try {
			controller.execute();
		} catch (ExecutionException e) {
			System.err.println("Something went wrongduring the execution of the scripts, see below:");
			e.printStackTrace();
		}
	}

	/**
	 * Will populate a list of config files that will be processed iteratively
	 * @param file the configuration file containing a series of individual configuration files (one per line)
	 * @throws FileNotFoundException
	 */
	public void importPipeline(String file) throws FileNotFoundException{
		FileInputStream is = new FileInputStream(file);
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader buff = new BufferedReader(isr);
		pipeline = new ArrayList<String>();
		try {
			String line;
			// Lecture du fichier ligne par ligne. Cette boucle se termine
			// quand la méthode retourne la valeur null.
			while ((line = buff.readLine()) != null) {pipeline.add(line);}
			buff.close();
		}
		catch (Exception e) {
			System.err.println("Something went wrong while reading the pipeline of configuration files");
			// erreur de fermeture des flux
				e.printStackTrace();
				}	
	}

	/**
	 * Launch the process of jape scripts generation with the given structure name
	 * @param structureName the name of the structure
	 * @throws IOException
	 * @throws JSONException
	 */
	public void generateHeaderAndSegmentScripts(String structureName) throws IOException, JSONException{
		readStructure(structureName);
		File structureFile = new File(structureFilePath);
		System.out.println("getStructureName :"+structureFilePath);
        if (!structureFile.exists()) {
        	structureFile = new File(ConfigTool.defaultStructureFilePath);
            System.out.println("No custom structure.\n"
            		+ "Default structure loaded!");
        }

        File templateFile = new File(ConfigTool.templateFilePath);
        if (!templateFile.exists()) {
            System.out.println("Cannot find the *templates* file!\n" +
                    "Please place " +
                    "scriptTemplates.json\n" +
                    "in folder \"input\".");
            return;
        }

        File headerRuleFile = new File(headerRuleFilePath);
        if (!headerRuleFile.exists()) {
        	headerRuleFile = new File(ConfigTool.defaultHeaderRuleFilePath);
        	 System.out.println("No custom rule file.\n"
             		+ "Default header rule file loaded!");
        }

        JapeScriptsGenerator scriptsGenerator = new JapeScriptsGenerator(structureFile, templateFile);

        File generatedHeaderRuleFile = new File(ConfigTool.generatedHeaderRuleFileName);
        //generate headers.java + rulefile.json
        // TODO outputfile should be similar to : CustomStructure/X/headers
        scriptsGenerator.generateHeaderScripts(headerRuleFile, generatedHeaderRuleFile, japeScriptsPath+ConfigTool.pathJoiner+ConfigTool.headerScriptsFolderName);
        //generate segments.java
     // TODO outputfile should be similar to : CustomStructure/X/segments
        scriptsGenerator.generateSegmentScripts(japeScriptsPath+ConfigTool.pathJoiner+ConfigTool.segmentScriptsFolderName);
	}
	
	/**
	 * Search in the configuration the name of the structure
	 * @return the structure name in the configuration files, the default one otherwise
	 */
	public String getStructureName(){
		String structurePath = configuration.structure;
		System.out.println("getStructureName : "+structurePath);
		if(!structurePath.equals(ConfigTool.DEFAULT_STRUCTURE_PATH)){
			File path = new File(structurePath);
			return path.getName().replace("_structure.json", "");
		}else{
			return ConfigTool.DEFAULT_STRUCTURE_PATH;
		}

	}
	
	/**
	 * Create an application based on the given name. Load it if it exists or create it otherwise
	 * @param appName name of the application
	 * @return
	 * @throws PersistenceException
	 * @throws ResourceInstantiationException
	 * @throws IOException
	 * @throws JSONException
	 */
	public SerialAnalyserController getAppController(String appName) throws PersistenceException, ResourceInstantiationException, IOException, JSONException{

		//test if app is in the gate controller
		if (this.controllerCollection.containsKey(appName)){
			System.out.println("The "+appName+" App was already loaded in GATE");
			return controllerCollection.get(appName);
		}else{
			//test if app is in the folder App
			File appFile = new File(String.format(ConfigTool.standardAppPath,appName));
			if (appFile.exists()){
				System.out.println("Get the "+appName+" App in the standard apps folder");
				SerialAnalyserController tempController = (SerialAnalyserController) PersistenceManager.loadObjectFromFile(appFile);
				controllerCollection.put(appName, tempController);
				return tempController;
			}else{
				//test if app in in the folder custom apps
				appFile = new File(String.format(ConfigTool.customAppPath,appName));
				if(appFile.exists()){
					System.out.println("Get the "+appName+" App in the custom apps folder");
					//TODO if the app exists but not the files, throw an exception.
					//way to handle it?
					//try/catch and rebuilt??
					SerialAnalyserController tempController = (SerialAnalyserController) PersistenceManager.loadObjectFromFile(appFile);
					controllerCollection.put(appName, tempController);
					return tempController;
				}else{
					//create app
					System.out.println("The "+appName+" App didn't exist and was created");
					return createCustomApp(appName);
				}
				
			}
		}
	}
	
	/**
	 * Create a custom app based on the custom structure given in the config file
	 * @param appName name of the generated app
	 * @return
	 * @throws IOException
	 * @throws JSONException
	 * @throws ResourceInstantiationException
	 * @throws PersistenceException
	 */
	public SerialAnalyserController createCustomApp(String appName) throws IOException, JSONException, ResourceInstantiationException, PersistenceException{
		GateScriptManager gsm = new GateScriptManager();
		String customStructureName = getStructureName();
		generateHeaderAndSegmentScripts(customStructureName);
		
		//lists for the structure
		ArrayList<String> structuredList = gsm.importStructure(new File(configuration.structure));
		
		//create the custom application
		SerialAnalyserController myStructureApp = null;
		//Create the headerApp or segmentApp
		if(appName.contains("Header")){
			myStructureApp = gsm.createHeaderAppFromScripts(appName, String.format(ConfigTool.customHeaderPath,customStructureName),structuredList);
		}
		if(appName.contains("Segment")){
	        Collections.reverse(structuredList);
			myStructureApp = gsm.createSegmentAppFromScripts(appName,String.format(ConfigTool.customSegmentPath,customStructureName),structuredList);
		}
		// TODO modify path to get something similar : CustomStructure/X/X.gapp
		PersistenceManager.saveObjectToFile(myStructureApp, new File(String.format(ConfigTool.customAppPath,myStructureApp)));
        
		
		
		return myStructureApp;
	}
	
	/**
	 * Run the gate software with the configFile given
	 * @param configFile file which contains all parameters
	 * @throws GateException
	 * @throws IOException
	 * @throws JSONException
	 */
	public void launchApplication() throws GateException, IOException, JSONException{
//		Configuration.readConfiguration(configFile);
		System.out.println(configuration.displayConfiguration());
		try {
			System.setErr(new PrintStream(new FileOutputStream(configuration.errorFile)));
			System.setOut(new PrintStream(new FileOutputStream(configuration.logFile)));
		} catch (FileNotFoundException e) {
			System.err.println("Something went wrong while setting the log files");
			e.printStackTrace();
		}
		
		Corpus corpus = setCorpus();
		//get the name of the structure json file in order to built header and segment applications according to that name (eg: default_rules.json => "default")
		String structureNameID = getStructureName();
		String[] customProcess = ConfigTool.ProcessPipeline;
		System.out.println("nameID :"+structureNameID);
		if (structureNameID.equals(ConfigTool.DEFAULT_STRUCTURE_PATH)){
			
			//load the default Header.gapp and Segment.gapp files
			String[] listProcess = customProcess;
			System.out.println(Arrays.toString(listProcess));
			for (String process : listProcess){
				controller = getAppController(process);
				processSingleText(corpus);
			}

		}
		else{
			//build custom app
			for (int i = 0; i < customProcess.length; i++)
			{
			    if (customProcess[i].equals("Header"))
			    {
			    	customProcess[i] = "Header"+structureNameID;
			    }
			    if (customProcess[i].equals("Segment"))
			    {
			    	customProcess[i] = "Segment"+structureNameID;
			    }
			}
			String[] listProcess = customProcess;
			System.out.println(Arrays.toString(listProcess));
			for (String process : listProcess){
				controller = getAppController(process);
				processSingleText(corpus);
			}
			// going back to System.out for output messages
			System.setErr(new PrintStream(new FileOutputStream(FileDescriptor.err)));
			System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
			System.out.println("*** Process finished, see log/err files for more information ***");		
			// TODO write real log file path???
		}
	}	
	
	/**
	 * will populate a list of config files that will be processed iteratively
	 * @param file the configuration file containing a series of individual configuration files (one per line)
	 * @throws FileNotFoundException
	 */
	public ArrayList<String> parsePipeline(String file) throws FileNotFoundException{
		FileInputStream is = new FileInputStream(file);
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader buff = new BufferedReader(isr);
		ArrayList<String> pipeline = new ArrayList<String>();
		try {
			String line;
			// Lecture du fichier ligne par ligne. Cette boucle se termine
			// quand la méthode retourne la valeur null.
			while ((line = buff.readLine()) != null) {pipeline.add(line);}
			buff.close();
		}catch (Exception e) {
			System.err.println("Something went wrong while reading the pipeline of configuration files");
			// erreur de fermeture des flux
				e.printStackTrace();
		}	
		return pipeline;
	}
	
	
	public static void main(String[] args) throws GateException, IOException, JSONException, InterruptedException {
		// mandatory init for every element needing gate API (i.e its factory)
		ArmletMain gsa = new ArmletMain();
		//Initialize the GATE pipeline
		gsa.controllerCollection = new HashMap<String,SerialAnalyserController>();
		//Create the tool to generate the scripts
		ArmletMain.gsm = new GateScriptManager();
		
		gsa.initGate();
		
		//Corpus corpus = gsa.setCorpus(args[0]); //useless?
		System.out.println("multiplicity = "+gsa.multiplicity);
		if (gsa.multiplicity.equals("1")){
			gsa.configuration = new Configuration(args[0]);
			gsa.launchApplication();
			GateFinalPostProcessing.cleanFootnotes(gsa.configuration.outputText,gsa.configuration.outputText+"_cleanFootnote.xml");
			GateFinalPostProcessing.cleanUpXMLFile2(gsa.configuration.outputText+"_cleanFootnote.xml", gsa.configuration.outputText+"_final.xml");
		}
		
		//No multithreading
		if (gsa.multiplicity.equals("2")){
			ArrayList<String> pipeline = gsa.parsePipeline(args[0]);	
			for(String s : pipeline){
				gsa.configuration = new Configuration(s);
				gsa.launchApplication();
				GateFinalPostProcessing.cleanFootnotes(gsa.configuration.outputText,gsa.configuration.outputText+"_cleanFootnote.xml");
				GateFinalPostProcessing.cleanUpXMLFile2(gsa.configuration.outputText+"_cleanFootnote.xml", gsa.configuration.outputText+"_final.xml");
			}
		}
		
		//NOT FUNCTIONNAL FOR MULTITHREADING due to static variables
		if (gsa.multiplicity.equals("3")){
			ArrayList<String> pipeline = gsa.parsePipeline(args[0]);	
			//TODO multithread
			for(String s : pipeline){
//				System.out.println(s);
////				Multithread test
				PipelineMultiThread pmt = new PipelineMultiThread(s, gsa, s);
				//TODO clean up for the pipeline
				pmt.start();
				Thread.sleep(500);
//				Not multithread
//				gsa.launchApplication(s);
//				GateFinalPostProcessing.cleanUpXMLFile(Configuration.outputText, Configuration.outputText+"_final.xml");
			}
		}
		
		System.out.println("*** process finished, Please take a look at the XML, log and error files for more information ***");

	}

}






