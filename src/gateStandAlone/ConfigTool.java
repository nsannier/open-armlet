package gateStandAlone;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

/**	Copyright (c) {2014-2019}, University of Luxembourg, Interdisciplinary Centre for Security, Reliability and Trust (SnT)
* 	Nicolas Sannier (sannier@svv.lu),
*	Morayo Adedjouma (morayoade@gmail.com),
*	Virgil Tassan (tassan-zanin-caser@svv.lu),
*	Wei Dou (dou@svv.lu),
*	Mehrdad Sabetzadeh (sabetzadeh@svv.lu),
*	Lionel Briand (briand@svv.lu),
*/

public class ConfigTool {
	
	ConfigTool(String configCustom) throws IOException{
		customFields = new HashMap<String,String>();
		try(BufferedReader br = new BufferedReader(new FileReader(configCustom))) {
		    StringBuilder sb = new StringBuilder();
		    String line = br.readLine();
		    String[] keyValue;
		    int i = 1;
		    while (line != null) {
			    keyValue = line.split(":=");
			    if(keyValue.length ==2){
			    	 customFields.put(keyValue[0].trim(), keyValue[1].trim());
			    }else{
			    	System.err.println("can't read line "+i+" as a property");
			    }
		        line = br.readLine();
		        i+=1;
		    }
		}
	}
	
	public HashMap<String,String> customFields;
	
	//Gate Config
	public static String gatePathBuildProperties = "Gate8.1/build.properties";
	public static String setGateHome = "Gate8.1/";
	public static String setSiteConfigFile = "Gate8.1/gate.xml";
	public static String setPluginsHome = "GATE8.1/plugins";

	//Configuration file
	public static String DEFAULT_XML = "default.xml";
	public static String DEFAULT_STRUCTURE_PATH = "Structure/scl_default/scl_default_structure.json";
	public static String DEFAULT_JSONRULES_PATH = "Structure/scl_default/scl_default_rules.json";
	public static String DEFAULT_LOG = "Log/default_log.txt";
	public static String DEFAULT_ERR = "Log/default_err.txt";
	
	//PATH BUILDER ELEMENT
    public static final String headerScriptsFolderName = "headers";
    public static final String segmentScriptsFolderName = "segments";
    public static final String standardJapeStructurePath = "./CustomJape/%s";
    public static final String standardCustomStructurePath = "./Structure/%s/%<s_structure.json";
//    public static final String standardCustomStructurePath = "./Structure/CustomStructure/%s/%<s_structure.json";
    
    //CUSTOM APP PROCESS
    public static final String[] ProcessPipeline = {"Preliminary","Header","Segment","GenericCrossReferences","GenericWriter"};
//    public static final String[] ProcessPipeline = {"Preliminary",""};
    
    //CUSTOM PREAMBLE NAME
    public static final String customSegmentPreambleFile = "CustomMarkPreambleSegment.jape";
    
    //CUSTOM AMENDMENT NAME
    public static final String customSegmentAmendmentFile = "./Jape/preliminary/MarkAmendmentSegment.jape";
    
    //JAPE PATH
    public static final String customCleaningExludedRuleFile = "./Jape/CleanExcludedRules.jape";
    public static final String customWriteMatchedRule = "./Jape/WriteMatchedRule.jape";
    
    //RULE FILE PATH
    public static final String templateFilePath = "./src/japescriptgen/tools/scriptTemplates.json";

    //GENERATED TEMP FILE PATH
    public static final String generatedHeaderRuleFileName = "./src/japescriptgen/tools/generatedHeaderRules.json";
	
    //DEFAULT FILE PATH
    public static final String defaultHeaderRuleFilePath = "./src/japescriptgen/tools/headerRules.json";
    public static final String defaultStructureFilePath = "Structure/CustomStructure/DefaultStructure.json";
    
	//JAPE SCRIPT GENERATOR ELEMENTS
    public static final String ruleReplacePattern = "<(\\S+?)>";
    public static final String importFileExpression = "Imports: { import gateStandAlone.* ;}";
    public static final String headerScriptFileNameTemplate = "CustomMark%sHead.jape";
    public static final String headerScriptInput = "Token SpaceToken num HeaderLabel OpenQuote Footnote_Anchor Footnote_Content";
    public static final String headerOption = "appelt";
    public static final String pathJoiner = "/";
    public static final String defaultRule = "{Token.string==\"%s\"}";
    public static final String headerString = "header";
    public static final String commentCreateFeatureMap = "create the new token";
    public static final String commentCreateProperty = "create property %s";
    public static final String segmentScriptFileNameTemplate = "CustomMark%sSegment.jape";
    public static final String segmentOption = "all";
    public static final String segmentRuleGroup = "(%s):%s";
    public static final String segmentSpanVariable = "reference";
    public static final String segmentBeginVariable = "left";
    public static final String segmentEndVariable = "right";
    public static final String endOfDocument = "EOD";
    public static final String commentGetElementAnnotations = "================Get all the contained %s annotations ==============";
    public static final String crossReferenceVariable = "crossRef";
    public static final String crossReferenceId = "CrossReference_ref";
	
    //Structure element name
    public static final String highestElementName = "act";
    
    //basic element eli abreviation
    public static final String basicEli = "art_";
    
    //APP PATH PLACEHOLDER
    public static final String standardAppPath = "Apps/%s.gapp";
    public static final String customAppPath = "Apps/CustomApps/%s.gapp";
    public static final String customHeaderPath = "CustomJape/%s/headers";
    public static final String customSegmentPath = "CustomJape/%s/segments";
    
	//DEFAULT PATH APP
	public static final String DEFAULT_APP_PATH = "Apps/DefaultApp.gapp";
	
	
	/**
	 * return the value of a static variable. Checked first if it was changed by the user at the initialisation in properties.txt
	 * @param variable the name of the variable
	 * @return the value of the given variable
	 * @throws Exception
	 */
	public String getVariable(String variable) throws Exception{
		if(customFields.containsKey(variable)){
//			System.out.println("IN THE MAP");
			return customFields.get(variable);
		}else{
//			System.out.println("AS A STATIC");
			return (String)ReflectUtils.getValueOf(this,variable);
		}
	}
	
	public static void main(String[] args) throws Exception{
		ConfigTool cf = new ConfigTool("properties.txt");
		System.out.println(cf.getVariable("standardAppPath"));
		System.out.println(cf.getVariable("highestElementName"));
		System.out.println(cf.getVariable("commentCreateFeatureMap"));
		System.out.println(cf.getVariable("customSegmentAmendmentFile"));
		System.out.println(cf.getVariable("standardJapeStructurePath"));
		System.out.println(cf.getVariable("standardCustomStructurePath"));
	}
	
}
