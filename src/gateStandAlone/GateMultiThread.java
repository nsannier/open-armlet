package gateStandAlone;

/**	Copyright (c) {2014-2019}, University of Luxembourg, Interdisciplinary Centre for Security, Reliability and Trust (SnT)
* 	Nicolas Sannier (sannier@svv.lu),
*	Morayo Adedjouma (morayoade@gmail.com),
*	Virgil Tassan (tassan-zanin-caser@svv.lu),
*	Wei Dou (dou@svv.lu),
*	Mehrdad Sabetzadeh (sabetzadeh@svv.lu),
*	Lionel Briand (briand@svv.lu),
*/

public class GateMultiThread implements Runnable {

 private Thread t;
   private String threadName;
   private boolean isFinished;
   
   GateMultiThread( String name) {
	   isFinished = false;
      threadName = name;
      System.out.println("Creating " +  threadName );
   }
   
   public void run() {
      System.out.println("Running " +  threadName );
      try {
         for(int i = 6; i > 0; i--) {
            System.out.println("Thread: " + threadName + ", " + i);
            // Let the thread sleep for a while.
            Thread.sleep(50);
         }
      } catch (InterruptedException e) {
         System.out.println("Thread " +  threadName + " interrupted.");
      }
      System.out.println("Thread " +  threadName + " exiting.");
      isFinished = true;
   }
   
   public void start () {
      System.out.println("Starting " +  threadName );
      if (t == null) {
         t = new Thread (this, threadName);
         t.start ();
      }
   }
   
   public static boolean checkAllThread(GateMultiThread[] threads){
	   for(int i=0; i <threads.length; i++){
		   if(!threads[i].isFinished){
			   return false;
		   }
	   }
	   return true;
   }
	
   public static void main(String args[]) {
	   long startTime = System.nanoTime();

	   GateMultiThread[] threads = new GateMultiThread[100];
	   
	   for(int i=0; i<3; i++){
		   threads[i] = new GateMultiThread( "Thread-"+i);
		   threads[i].start();
	   }
	   
	   GateMultiThread R1 = new GateMultiThread( "Thread-1");
	      R1.start();
	      
	      GateMultiThread R2 = new GateMultiThread( "Thread-2");
	      R2.start();
	      
	      while(checkAllThread(threads)){
	    	  System.out.println("notDOne");
	      }
	      
	      long endTime = System.nanoTime();

	      long duration = (endTime - startTime);
	      
	      System.out.println("Time process :"+ duration/1000000d);
	   }   

  
}
