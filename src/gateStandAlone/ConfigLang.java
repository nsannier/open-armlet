package gateStandAlone;

/**	Copyright (c) {2014-2019}, University of Luxembourg, Interdisciplinary Centre for Security, Reliability and Trust (SnT)
* 	Nicolas Sannier (sannier@svv.lu),
*	Morayo Adedjouma (morayoade@gmail.com),
*	Virgil Tassan (tassan-zanin-caser@svv.lu),
*	Wei Dou (dou@svv.lu),
*	Mehrdad Sabetzadeh (sabetzadeh@svv.lu),
*	Lionel Briand (briand@svv.lu),
*/

public class ConfigLang {

	
	public final static String langAnd = "et";
	public final static String langOr = "ou";
	public final static String langTo = "à";
	public final static String langComma = ",";
	
	//To write in lower case
	public final static String langFirst = "premier";
	public final static String lang1st = "1er";
	public final static String langSecond = "second";
	public final static String lang2nd = "2nd";
	
}
