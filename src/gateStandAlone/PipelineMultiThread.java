package gateStandAlone;

import java.io.IOException;

import org.json.JSONException;

import gate.Document;
import gate.corpora.DocumentImpl;
import gate.util.GateException;

/**	Copyright (c) {2014-2019}, University of Luxembourg, Interdisciplinary Centre for Security, Reliability and Trust (SnT)
* 	Nicolas Sannier (sannier@svv.lu),
*	Morayo Adedjouma (morayoade@gmail.com),
*	Virgil Tassan (tassan-zanin-caser@svv.lu),
*	Wei Dou (dou@svv.lu),
*	Mehrdad Sabetzadeh (sabetzadeh@svv.lu),
*	Lionel Briand (briand@svv.lu),
*/
public class PipelineMultiThread implements Runnable {

	private Thread t;
	private String threadName;
	private ArmletMain gsa;
	private String config;
	

	
	 public PipelineMultiThread(String threadName, ArmletMain gsa, String config) {
		this.threadName = threadName;
		this.gsa = gsa;
		this.config = config;
	}

	public void run() {
	      System.out.println("Running " +  threadName );
	      try {
	    	  gsa.configuration = new Configuration(config);
	    	  gsa.launchApplication();
	      
	      } catch (GateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      System.out.println("Thread " +  threadName + " exiting.");
	   }
	   
	   public void start () {
	      System.out.println("Starting " +  threadName );
	      if (t == null) {
	         t = new Thread (this, threadName);
	         t.start ();
	      }
	   }

}
