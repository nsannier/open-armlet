package gateStandAlone;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.apache.commons.io.FilenameUtils;
import org.json.JSONException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import gate.creole.SerialAnalyserController;
import gate.util.GateException;

/**	Copyright (c) {2014-2019}, University of Luxembourg, Interdisciplinary Centre for Security, Reliability and Trust (SnT)
* 	Nicolas Sannier (sannier@svv.lu),
*	Morayo Adedjouma (morayoade@gmail.com),
*	Virgil Tassan (tassan-zanin-caser@svv.lu),
*	Wei Dou (dou@svv.lu),
*	Mehrdad Sabetzadeh (sabetzadeh@svv.lu),
*	Lionel Briand (briand@svv.lu),
*/

public class SCLEmbedded {

	public void run(String pathToInputFile) throws GateException, IOException, JSONException{
		ArmletMain gsa = new ArmletMain();
		gsa.controllerCollection = new HashMap<String,SerialAnalyserController>();
		ArmletMain.gsm = new GateScriptManager();
		try {
             System.out.println("...initGateForSCLEmbedded");
             gsa.initGateForSCLEmbedded();
		} catch(Exception ex) {
             //ex.printStackTrace();
             System.out.println("!!! Gate already created ("+ex.getMessage()+") !!!");
		}

		//Corpus corpus = gsa.setCorpus(args[0]); //useless?
		System.out.println("multiplicity = "+gsa.multiplicity);
		gsa.configuration = buildConfiguration(pathToInputFile);
		gsa.launchApplication();
		GateFinalPostProcessing.cleanUpXMLFile2(gsa.configuration.outputText, gsa.configuration.outputText+"_final.xml");
	}
	
	public Configuration buildConfiguration(String pathToInputFile) throws JsonParseException, JsonMappingException, IOException{
		String[] elements = new String[7];
		File inputFile = new File(pathToInputFile);
		String folder = inputFile.getParent();		
		String filename = FilenameUtils.getBaseName(pathToInputFile);
		elements[0] = pathToInputFile;
		elements[1] = folder+'/'+filename+"_output.xml";
		elements[2] = folder+'/'+filename+"_log.txt";
		elements[3] = folder+'/'+filename+"_err.txt";
		elements[4] = "Structure/DailyTexts";
		elements[5] = "eli/to/fill";
		elements[6] = "http://eli.legilux.public.lu/";
		
		Configuration c = new Configuration(elements);
		
		c.configurateHeaderAndStructure();
		c.generateListOfExcludedRules();
		
		Configuration.highLevelElementTypes = c.getHighLevelELements(new File(c.structure));
		Configuration.basicElement = c.getBasicElement(new File(c.structure));
		Configuration.subElementTypes = c.getSubDivisionELements(new File(c.structure));	
		
		return c;
	}
	
	public static void main(String[] args){
		SCLEmbedded sclem = new SCLEmbedded();
		try {
			File f = new File(args[0]);
			String absPath = f.getAbsolutePath();
			sclem.run(absPath);
		} catch (GateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		System.out.println(sclem.buildConfiguration("Test/CodeSecuriteSociale/texte_Securite_Sociale.txt"));
	}
	
}
