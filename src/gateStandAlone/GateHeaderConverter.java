package gateStandAlone;

/**	Copyright (c) {2014-2019}, University of Luxembourg, Interdisciplinary Centre for Security, Reliability and Trust (SnT)
* 	Nicolas Sannier (sannier@svv.lu),
*	Morayo Adedjouma (morayoade@gmail.com),
*	Virgil Tassan (tassan-zanin-caser@svv.lu),
*	Wei Dou (dou@svv.lu),
*	Mehrdad Sabetzadeh (sabetzadeh@svv.lu),
*	Lionel Briand (briand@svv.lu),
*/

public class GateHeaderConverter {
	
	public static String extra=""; 

	/**
	 * Convert a String starting with a roman number to int.
	 * The String can be more than a roman number (ex IIIbis or III-1). 
	 * In that case, the right part will be stored in "extra" and the result will just be the int value of the left part
	 * works synergistically with convertHeader method
	 * @param number the roman number to convert. 
	 * @return the int value for the roman number part of the string
	 */
	public static int romanToArabic(String number){
		extra="";
		String left = number;
		if (left.isEmpty()) return 0;
		if (left.startsWith("M")) return (1000 + romanToArabic(left.substring(1)));
		if (left.startsWith("CM")) return 900 + romanToArabic(left.substring(2));
		if (left.startsWith("D")) return 500 + romanToArabic(left.substring(1));
		if (left.startsWith("CD")) return 400 + romanToArabic(left.substring(2));
		if (left.startsWith("C")) return 100 + romanToArabic(left.substring(1));
		if (left.startsWith("XC")) return 90 + romanToArabic(left.substring(2));
		if (left.startsWith("L")) return 50 + romanToArabic(left.substring(1));
		if (left.startsWith("XL")) return 40 + romanToArabic(left.substring(2));
		if (left.startsWith("X")) return 10 + romanToArabic(left.substring(1));
		if (left.startsWith("IX")) return 9 + romanToArabic(left.substring(2));
		if (left.startsWith("V")) return 5 + romanToArabic(left.substring(1));
		if (left.startsWith("IV")) return 4 + romanToArabic(left.substring(2));
		if (left.startsWith("I")) return 1 + romanToArabic(left.substring(1));

		extra = left;
		return 0;
	 }	 

	public static String convertOrdinal(String number){
		if(number.toLowerCase().equals(ConfigLang.langFirst)) {return "1";}
		if(number.toLowerCase().equals(ConfigLang.lang1st)) {return "1";}
		if(number.toLowerCase().equals(ConfigLang.langSecond)) {return "2";}
		if(number.toLowerCase().equals(ConfigLang.lang2nd)) {return "2";}

		return number;
	}
	
	
	/**
	 * convert an id expressed a roman number into a String with the value in arabic number
	 * extras are captured in the extra that has been left by the romanToArabic method
	 * @param id the id to convert from roman number to int (and some extra)
	 * @return
	 */
	 public static String convertNumber(String id){
		 int res = romanToArabic(id);
		 if (res == 0){return extra;}
		 else {
			 return romanToArabic(id) + extra;
		 }
	 }
	 
	 
	 /**
	  * convert an expression into the a URI fragment following ELI convention
	  * @param expression the id to convert
	  * @return
	  */
	 public static String convertExpression(String expression){
		 String space = " ";
		 String result = "";
		 String[] elements = expression.split(space); 
		 for (String s : elements){
			 String temp = trimLastDot(s);
			 temp = convertNumber(temp);
			 result = result + convertOrdinal(temp);
		 }
		 result = result.replace(ConfigLang.langAnd, "_");
		 result = result.replace(ConfigLang.langOr, "_");
		 result = result.replace(ConfigLang.langTo, "--");
		 return result;
	 }	 
	 
	 
	 
	 public static String trimLastDot(String s) {
		if (s.endsWith(".")){
			return s.substring(0, s.length()-1);
		}
		return s;
	}

	public static void main (String[] args){
		 System.out.println(convertExpression("I"));
		 System.out.println(convertExpression("II"));
		 System.out.println(convertExpression("IIbis"));
		 System.out.println(convertExpression("IIIter"));
		 System.out.println(convertExpression("IVquater"));
		 System.out.println(convertExpression("Vquinquies"));
		 System.out.println(convertExpression("VIsixies"));
		 System.out.println(convertExpression("II-1"));
		 System.out.println(convertExpression("II-2"));
		 System.out.println(convertExpression("18"));
		 System.out.println(convertExpression("XIVa"));
		 System.out.println(convertExpression("1-1-2"));
		 System.out.println(convertExpression("PREMIER"));
		 System.out.println(convertExpression("1er"));
		 System.out.println(convertExpression("SECOND"));
		 System.out.println(convertExpression("2nd"));
		 System.out.println(convertExpression("1 à 10bis"));
		 System.out.println(convertExpression("XII et XIII"));
		 System.out.println(convertExpression("XII à XIIIquater"));
		 System.out.println(convertExpression("Ier"));
		 System.out.println(convertExpression("3. à IVbis."));
		 System.out.println(convertExpression("3.2.1-2.4. à IVbis."));

	 }
	 
}
