file = Test/Loi20170728/texte_loi20170728.txt
shortTitle = Loi du 28/07/2017
longTitle = Loi du 28/07/2017 modifiant le Code pénal et le Code de procédure pénale
rootELI = http://eli.legilux.public.lu/eli/
output = Test/Loi20170728/texte_loi20170728.xml
log = Test/Loi20170728/logStandAlone.txt
error = Test/Loi20170728/errorStandAlone.txt
jsonRules = Structure/DefaultStructure