Loi du 28 juillet 2017 modifiant le Code pénal et le Code de procédure pénale, en vue de transposer
la directive 2014/62/UE du Parlement européen et du Conseil du 15 mai 2014 relative à la protection
pénale de l’euro et des autres monnaies contre la contrefaçon, et remplaçant la décision-cadre
2000/383/JAI du Conseil.
Nous Henri, Grand-Duc de Luxembourg, Duc de Nassau,
Notre Conseil d’État entendu ;
De l’assentiment de la Chambre des Députés ;
Vu la décision de la Chambre des Députés du 4 juillet 2017 et celle du Conseil d’État du 14 juillet 2017 portant
qu’il n’y a pas lieu à second vote ;
Avons ordonné et ordonnons :

Art. Ier.
Le Code pénal est modifié comme suit :
1) À l’article 57-1, paragraphe 1, alinéa 1er, la référence aux articles 162, 168, 173, 176, 180, tirets 3 à 6, 186, tirets 3 à 6, 192-1 et 192-2 est remplacée par la référence aux articles 161, 162, 163, 166 et 169, points 2 et 3.
2) À l’article 57-1, paragraphe 2, la référence aux articles 162, 163, 168, 169, 170, 173, 176, 177, 180, tirets 3 à 6, 185, 186, tirets 3 à 6, 187-1, 192-1 et 192-2 est remplacée par la référence aux articles 161, 162, 163, 164, 165, 166, 169, points 2 et 3, 178 et 179.
3) À l’article 57-1, paragraphe 3, la référence aux articles 162, 163, 168, 169, 170, 173, 176, 177, 180, tirets 3 à 6, 185, 186, tirets 3 à 6, 187-1, 192-1 et 192-2 est remplacée par la référence aux articles 161, 162, 163, 164, 165, 166, 169, points 2 et 3, 178 et 179.
4) Au Titre III du Livre II, les Chapitres Ier, II, III et la Disposition commune aux trois chapitres, ensemble avec les articles 160 à 192-2, sont abrogés et remplacés par les dispositions suivantes :
«
Chapitre Ier. - De la contrefaçon, de l’altération ou de la falsification de la monnaie, des instruments de paiement corporels protégés contre les imitations ou les utilisations frauduleuses, et des titres représentatifs de droits de propriété, de créances ou de valeurs mobilières
Art. 160.
Aux fins du présent chapitre, on entend par « monnaie » les billets et les pièces ayant cours légal dans le Grand-Duché de Luxembourg ou à l’étranger ou dont l’émission est autorisée par une loi d’un État étranger ou en vertu d’une disposition y ayant force de loi.
Aux fins du présent chapitre, on entend par « instruments de paiement corporels » les instruments de paiement corporels, émis par les prestataires de services de paiement ou les établissements commerciaux, et protégés contre les imitations ou les utilisations frauduleuses, permettant, en association, le cas échéant, avec un autre instrument, d’effectuer des transferts ou des retraits d’argent ou de valeur monétaire.
Aux fins du présent chapitre, on entend par « titres » les titres représentatifs de droits de propriété, de créances ou de valeurs mobilières, qui ont été légalement émis par une personne morale de droit public ou privé, luxembourgeois ou d’un État étranger, sous quelque dénomination que ce soit, ou par une institution financière internationale, ou par une personne physique.
Art. 161.
Le fait de contrefaire, d’altérer ou de falsifier de la monnaie, des instruments de paiement corporels ou des titres, quel que soit le moyen employé pour produire le résultat, est puni de la réclusion de dix à quinze ans.
Art. 162.
Le fait de contrefaire, d’altérer ou de falsifier de la monnaie qui n’a plus cours légal, mais qui peut encore être échangée contre une monnaie ayant cours légal, est puni d’un emprisonnement de trois mois à cinq ans et d’une amende de 500 euros à 75.000 euros.
La tentative du délit prévu à l’alinéa précédent est punie d’un emprisonnement de trois mois à deux ans et d’une amende de 500 euros à 25.000 euros.
La monnaie contrefaite, altérée ou falsifiée est confisquée.
Art. 163.
Le fait de participer, de concert avec les auteurs des infractions prévues aux articles 161 ou 162, soit à l’émission de la monnaie, des instruments de paiement corporels ou des titres, contrefaits, altérés ou falsifiés, soit à leur introduction sur le territoire luxembourgeois, est puni des peines prévues respectivement aux articles 161 ou 162.
La tentative de participation à l’émission ou à l’introduction sur le territoire luxembourgeois de monnaie visée à l’alinéa premier de l’article 162 est punie d’un emprisonnement de trois mois à deux ans et d’une amende de 500 euros à 25.000 euros.
Art. 164.
Le fait de recevoir, de détenir, de transporter, d’importer, d’exporter ou de se procurer, avec connaissance mais sans s’être rendu coupable de la participation énoncée au précédent article, de la monnaie, des instruments de paiement corporels ou des titres, contrefaits, altérés ou falsifiés, dans le but de leur mise en circulation, est puni d’un emprisonnement d’un an à cinq ans et d’une amende de 500 euros à 75.000 euros.
Est puni de la même peine, le fait de mettre en circulation de la monnaie, des instruments de paiement corporels ou des titres, contrefaits, altérés ou falsifiés.
La tentative de l’un des délits prévus aux alinéas précédents est punie d’un emprisonnement de trois mois à deux ans et d’une amende de 500 euros à 25.000 euros.
La monnaie, les instruments de paiement corporels et les titres, contrefaits, altérés ou falsifiés, sont confisqués.
Art. 165.
Le fait de remettre en circulation ou de tenter de remettre en circulation de la monnaie, des instruments de paiement corporels ou des titres, contrefaits, altérés ou falsifiés, reçus pour bons mais dont on a vérifié ou fait vérifier les vices après réception, est puni d’un emprisonnement de trois mois à un an et d’une amende de 500 euros à 10.000 euros ou d’une de ces peines seulement.
La monnaie, les instruments de paiement corporels et les titres, contrefaits, altérés ou falsifiés sont confisqués.
Art. 166.
Le fait de fabriquer, de recevoir, de posséder, de se procurer, de vendre ou de céderà un tiers des instruments, des objets, des programmes ou des données d’ordinateur, ou tout autre procédé, devant servir à la contrefaçon, à l’altération ou à la falsification de monnaie, d’instruments de paiement corporels ou de titres, est puni de la réclusion de cinq à dix ans, s’il a été commis dans le but de contrefaire, de falsifier ou d’altérer des monnaies, des instruments de paiement corporels ou des titres.
Le fait de fabriquer, de falsifier, de recevoir, de posséder, de se procurer, de vendre ou de céder à un tiers des dispositifs de sécurité tels que des hologrammes, des filigranes ou d’autres éléments servant à protéger la monnaie, les instruments de paiement corporels et les titres contre la contrefaçon, l’altération ou la falsification, est puni des mêmes peines, s’il a été commis dans le but de contrefaire, de falsifier ou d’altérer des monnaies, des instruments de paiement corporels ou des titres.
Les objets et dispositifs mentionnés ci-dessus sont confisqués, alors même que la propriété n’en appartient pas au condamné.
Chapitre II. - De la contrefaçon, de l’altération ou de la falsification des sceaux, timbres, poinçons et marques
Art. 167.
Le fait de contrefaire, d’altérer ou de falsifier le sceau de l’État ou de faire usage du sceau contrefait, altéré ou falsifié, est puni de la réclusion de dix à quinze ans.
Art. 168.
Aux fins des articles 169 à 176, les termes « sceaux », « timbres », « poinçons » et « marques » désignent tant les sceaux, timbres, poinçons et marques d'une autorité quelconque luxembourgeoise, d'une personne morale de droit public ou de droit privé luxembourgeois, sous quelque dénomination que ce soit, ou d'une personne physique, que les sceaux, timbres, poinçons et marques d’un État étranger, d’une organisation internationale, d’une autorité étrangère quelconque ou d’une personne morale de droit public ou privé d’un État étranger, sous quelque dénomination que ce soit, ou d’une personne physique.
Art. 169.
Est puni de la réclusion de cinq à dix ans
1. Le fait de contrefaire, d’altérer ou de falsifier des timbres ou des poinçons servant à marquer les matières d’or ou d’argent, ou de faire usage de ces timbres ou poinçons contrefaits, altérés ou falsifiés;
2. Le fait de fabriquer, de recevoir, de posséder, de se procurer, de vendre ou de céder à un tiers des instruments, des objets, des programmes ou des données d’ordinateur, ou tout autre procédé, devant servir à la contrefaçon, à l’altération ou à la falsification de timbres, s’il a été commis dans le but de contrefaire, de falsifier ou d’altérer des timbres;
3. Le fait de fabriquer, de falsifier, de recevoir, de posséder, de se procurer, de vendre ou de céder à un tiers des dispositifs de sécurité servant à protéger les timbres contre la contrefaçon, l’altération ou la falsification, s’il a été commis dans le but de contrefaire, de falsifier ou d’altérer des timbres.
Art. 170.
Le fait de sciemment exposer en vente des papiers ou des matières d'or ou d'argent marqués d'un timbre ou d'un poinçon contrefaits, altérés ou falsifiés est puni de la réclusion de cinq à dix ans.
Art. 171.
Si les marques apposées par le bureau de garantie ont été frauduleusement appliquées sur d’autres objets, ou si ces marques ou l’empreinte d’un timbre ont été contrefaites sans emploi d’un poinçon ou d’un timbre contrefait, les coupables sont punis d’un emprisonnement de six mois à cinq ans.
Art. 172.
Le fait de recevoir, de posséder ou de se procurer avec connaissance du papier ou des matières d’or ou d’argent marqués d'un timbre ou d’un poinçon contrefaits, altérés ou falsifiés, et d’en faire usage, est puni d'un emprisonnement de trois mois à six mois et d’une amende de 500 euros à 15.000 euros.
Art. 173.
Est puni d'un emprisonnement de trois mois à cinq ans et d’une amende de 500 euros à 75.000 euros, et pourra être puni de l'interdiction conformément à l'article 24
1. Le fait de contrefaire, d’altérer ou de falsifier des sceaux, timbres, poinçons ou marques ou de faire usage de ces sceaux, timbres, poinçons ou marques contrefaits, altérés ou falsifiés;
2. Le fait de se procurer indûment les vrais sceaux, timbres, poinçons ou marques ayant l'une des destinations visées aux articles 167 et 169, et d’en faire une application ou un usage préjudiciable aux droits et aux intérêts soit de l'État luxembourgeois, d'une autorité quelconque luxembourgeoise, d'une personne morale de droit public ou de droit privé luxembourgeois, sous quelque dénomination que ce soit, ou même d'une personne physique, soit d’un État étranger, d’une organisation internationale, d’une autorité étrangère quelconque ou d’une personne morale de droit public ou privé d’un État étranger, sous quelque dénomination que ce soit, ou d’une personne physique.
La tentative de l’un de ces délits est punie d'un emprisonnement de trois mois à deux ans et d’une amende de 500 euros à 25.000 euros.
Art. 174.
Le fait de contrefaire, d’altérer ou de falsifier des timbres-poste ou autres timbres adhésifs, ou d’exposer en vente ou de mettre en circulation des timbres-poste ou autres timbres adhésifs contrefaits, altérés ou falsifiés, est puni d'un emprisonnement de trois mois à trois ans et d’une amende de 500 euros à 25.000 euros, et peut être puni de l'interdiction conformément à l'article 24.
La tentative de l’un des délits prévus à l’alinéa précédent est punie d'un emprisonnement de trois mois à un an et d’une amende de 500 euros à 25.000 euros.
Art. 175.
Est puni d'une amende de 500 euros à 25.000 euros
1. Le fait de se procurer des timbres-poste ou autres timbres adhésifs contrefaits, altérés ou falsifiés, et d’en faire usage ;
2. Le fait de faire disparaître, soit d'un timbre-poste ou autre timbre adhésif, soit d'un coupon pour le transport des personnes ou des choses, la marque indiquant qu'ils ont déjà servi, ou de faire usage d’un tel timbre-poste ou autre timbre adhésif ou d’un tel coupon.
Art. 176.
Le fait d’apposer ou de faire apposer par addition, retranchement ou par une altération quelconque, sur des objets fabriqués, le nom d'un fabricant autre que celui qui en est l'auteur ou la raison commerciale d'une fabrique autre que celle de la fabrication, est puni d'un emprisonnement de trois mois à cinq ans et d'une amende de 500 euros à 75.000 euros.
Tout marchand, commissionnaire ou débitant quelconque, qui aura sciemment exposé en vente, importé ou mis en circulation des objets prévus à l’alinéa précédent est puni de la même peine.
Chapitre III. - Dispositions communes
Art. 177.
Les personnes coupables des infractions mentionnées aux articles 161 à 164, et 166 sont exemptes de peines, si, avant toute émission de monnaie contrefaite, altérée ou falsifiée, ou d’autres instruments de paiement corporels contrefaits, altérés ou falsifiés, ou de titres représentatifs de droits de propriété, de créances ou de valeurs mobilières contrefaits, altérés ou falsifiés, et avant toutes poursuites, elles en ont donné connaissance et révélé les auteurs à l'autorité.
Art. 178.
Les articles 161 à 165 s'appliquent également quand les infractions sont commises moyennant de la monnaie fabriquée en utilisant les installations ou du matériel légaux, en violation des droits ou des conditions en vertu desquels les autorités compétentes autorisent l'émission de la monnaie, et sans l'accord des autorités compétentes.
Art. 179.
Les articles 161 à 166, 169 et 178 s'appliquent également quand les infractions sont commises moyennant de la monnaie, qui, bien que destinée à être mise en circulation, n'a pas encore été émise et constitue de la monnaie ayant cours légal.
Art. 180.
Les confiscations prévues aux deux chapitres précédents sont prononcées même en cas d’acquittement, d’exemption de peine, d’extinction ou de prescription de l’action publique.
»
est remplacé par l’intitulé
5) L’intitulé « Dispositions communes aux quatre chapitres précédents »
« Dispositions communes aux chapitres Ier, II et IV qui précèdent » .
6) Les articles 213 et 214 sont remplacés par les dispositions suivantes :
«
Art. 213.
L'application des peines portées contre ceux qui auront fait usage de monnaie contrefaite, altérée ou falsifiée, de titres représentatifs de droits de propriété, de créances ou de valeurs mobilières, de sceaux, de timbres, de poinçons, de marques, de dépêches télégraphiques et écrits contrefaits, altérés ou falsifiés n'aura lieu qu'autant que ces personnes auront fait usage de ces faux, dans une intention frauduleuse ou à dessein de nuire.
Art. 214.
Dans les cas prévus aux chapitres Ier, II et IV qui précèdent et pour lesquels aucune amende n'est spécialement portée, il sera prononcé une amende de 500 euros à 125.000 euros.
»
7) Le premier alinéa de l’article 501 est modifié comme suit :
«
Art. 501.
Seront punis d'un emprisonnement de huit jours à un an et d'une amende de 251 euros à 10.000 euros ou de l'une de ces peines seulement, ceux qui, même sans intention frauduleuse, auront fabriqué, vendu, colporté ou distribué tous objets, instruments, imprimés ou formules obtenus par un procédé quelconque qui, par leur forme extérieure, présenteraient avec la monnaie, les titres de rente et timbres des postes ou des télégraphes, les titres représentatifs de droits de propriété, de créances ou de valeurs mobilières, autres que des signes monétaires sous forme de billets ou généralement avec les valeurs fiduciaires émises au Grand-Duché ou à l'étranger, une ressemblance de nature à faciliter l'acceptation desdits objets, instruments, imprimés ou formules au lieu et place des valeurs imitées.
»
8) À l’article 506-1, point 1, tiret 8, la référence aux articles 184, 187, 187-1, 191 et 309 est remplacée par la référence aux articles 173, 176 et 309.
9) Le point 4 de l’article 556 est modifié comme suit :
«
Ceux qui, à défaut de convention contraire, auront refusé de recevoir de la monnaie non fausse ni altérée, selon la valeur pour laquelle elle a cours légal dans le Grand-Duché ;
».
Art. II.
Le Code de procédure pénale est modifié comme suit :