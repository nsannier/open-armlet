Unable to find a subdvision segment in article: Art. 168. at offset: 118031
The following text will be inserted as-is in the XML document: 
Art. 168. 
Ne sont pas déductibles les dépenses suivantes: 
(1). les dépenses faites en vue de remplir des obligations imposées à la collectivité par ses statuts ou son pacte social; (2). l'impôt sur le revenu des collectivités, l'impôt sur la fortune et l'impôt commercial communal; (3). les rémunérations imposables en vertu du premier alinéa , numéro 2 de l' article 91 ; (4). les dépenses faites dans un but cultuel, charitable ou d'intérêt général sans préjudice de la disposition prévue sub premier alinéa numéro 3 de l' article 109 . 



