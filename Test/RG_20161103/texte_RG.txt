Règlement grand-ducal du 3 novembre 2016 concernant la réglementation temporaire de la circulation sur N14 entre Diekirch et Moschbierg à l’occasion de travaux.

Nous Henri, Grand-Duc de Luxembourg, Duc de Nassau;

Vu la loi modifiée du 14 février 1955 concernant la réglementation de la circulation sur toutes les voies publiques;

Vu l'article 2 (1) de la loi modifiée du 12 juillet 1996 portant réforme du Conseil d'Etat et considérant qu'il y a urgence;

Sur le rapport de Notre Ministre du Développement durable et des Infrastructures et après délibération du Gouvernement en Conseil;

Arrêtons:

Art.1er.- Pendant la phase d’exécution des travaux, à l’endroit ci-après, la vitesse maximale est limitée à 70 km/heure dans les deux sens et il est interdit aux conducteurs de véhicules automoteurs de dépasser des véhicules automoteurs autres que les motocycles à deux roues sans side-car et les cyclomoteurs à deux roues :
- sur la N14 (PK 1370-1820) entre Diekirch et Moschbierg.
Ces dispositions sont indiquées par les signaux C,14 et C,13aa. Le signal A,21, complété par un panneau additionnel portant l’inscription « sortie de chantier » est également mis en place.
Art.2.- Les infractions aux dispositions du présent règlement sont punies conformément à l'article 7 de la loi modifiée du 14 février 1955 concernant la réglementation de la circulation sur toutes les voies publiques.
Art. 3.- Notre Ministre du Développement durable et des Infrastructures est chargé de l’exécution du présent règlement qui sera publié au Mémorial.


Le Ministre du Développement durable et des Infrastructures,

François Bausch	Château de Berg, le 3 novembre 2016
Henri

