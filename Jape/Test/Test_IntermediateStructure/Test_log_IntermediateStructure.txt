*** Importing the document to process ***
Mime :mimeType text/plain
getStructureName : Structure/scl_default/scl_default_structure.json
[Preliminary, Headerscl_default, Segmentscl_default, GenericCrossReferences, GenericWriter]
Get the Preliminary App in the standard apps folder
*** Executing the application ***
START TEXT PROCESSING
END TEXT PROCESSING
Get the Headerscl_default App in the custom apps folder
*** Executing the application ***
SIZE : 1372
Get the Segmentscl_default App in the custom apps folder
*** Executing the application ***
Get the GenericCrossReferences App in the standard apps folder
*** Executing the application ***
Get the GenericWriter App in the standard apps folder
*** Executing the application ***
START XML WRITE UP
STARTING THE XML GENERATION
Writing XML document
Writing the Document's header
Writing the Document's body
Basic element already written: Art. L. 010-1. at offset: 132
Basic element already written: Art. L. 010-2. at offset: 2147
Basic element already written: Art. L. 010-1. at offset: 132
Basic element already written: Art. L. 010-2. at offset: 2147
Basic element already written: Art. L. 100-1. at offset: 2319
Basic element already written: Art. L. 100-1. at offset: 2319
Basic element already written: Art. L. 111-1. at offset: 2465
Basic element already written: Art. L. 111-2. at offset: 3614
Basic element already written: Art. L. 111-3. at offset: 4118
Basic element already written: Art. L. 111-4. at offset: 7800
Basic element already written: Art. L. 111-5. at offset: 8301
Basic element already written: Art. L. 111-6. at offset: 9975
Basic element already written: Art. L. 111-7. at offset: 10747
Basic element already written: Art. L. 111-8. at offset: 11824
Basic element already written: Art. L. 111-9. at offset: 13521
Basic element already written: Art. L. 111-10. at offset: 14963
Basic element already written: Art. L. 111-11. at offset: 16679
Basic element already written: Art. L. 111-12. at offset: 17402
Basic element already written: Art. L. 111-13. at offset: 18213
Basic element already written: Art. L. 111-14. at offset: 18292
Basic element already written: Art. L. 111-15. at offset: 18370
Basic element already written: Art. L. 111-16. at offset: 18418
Basic element already written: Art. L. 111-17. at offset: 18466
Basic element already written: Art. L. 111-18. at offset: 18514
Basic element already written: Art. L. 111-19. at offset: 18562
Basic element already written: Art. L. 112-1. at offset: 18658
Basic element already written: Art. L. 112-2. at offset: 18705
Basic element already written: Art. L. 112-3. at offset: 18752
Basic element already written: Art. L. 112-4. at offset: 18799
Basic element already written: Art. L. 113-1. at offset: 18893
Basic element already written: Art. L. 113-2. at offset: 18940
Basic element already written: Art. L. 113-3. at offset: 18987
Basic element already written: Art. L. 113-4. at offset: 19034
Basic element already written: Art. L. 113-5. at offset: 19081
Basic element already written: Art. L. 113-6. at offset: 19128
Basic element already written: Art. L. 121-1. at offset: 19319
Basic element already written: Art. L. 121-2. at offset: 20086
Basic element already written: Art. L. 121-3. at offset: 20383
Basic element already written: Art. L. 121-4. at offset: 20760
Basic element already written: Art. L. 121-5. at offset: 26301
Basic element already written: Art. L. 121-6. at offset: 29250
Basic element already written: Art. L. 121-7. at offset: 34286
Basic element already written: Art. L. 121-8. at offset: 35207
Basic element already written: Art. L. 121-9. at offset: 35923
Basic element already written: Art. L. 122-1. at offset: 36210
Basic element already written: Art. L. 122-2. at offset: 42255
Basic element already written: Art. L. 122-3. at offset: 43361
Basic element already written: Art. L. 122-4. at offset: 44896
Basic element already written: Art. L. 122-5. at offset: 46234
Basic element already written: Art. L. 122-6. at offset: 50026
Basic element already written: Art. L. 122-7. at offset: 50186
Basic element already written: Art. L. 122-8. at offset: 51363
Basic element already written: Art. L. 122-9. at offset: 51694
Basic element already written: Art. L. 122-10. at offset: 51893
Basic element already written: Art. L. 122-11. at offset: 52418
Basic element already written: Art. L. 122-12. at offset: 53313
Basic element already written: Art. L. 122-13. at offset: 53414
Basic element already written: Art. L. 123-1. at offset: 54574
Basic element already written: Art. L. 123-2. at offset: 56214
Basic element already written: Art. 123-2. at offset: 56729
Basic element already written: Art. L. 123-3. at offset: 56931
Basic element already written: Art. L. 123-4. at offset: 57342
Basic element already written: Art. L. 123-5. at offset: 58227
Basic element already written: Art. L. 123-6. at offset: 59323
Basic element already written: Art. L. 123-7. at offset: 59686
Basic element already written: Art. L. 123-8. at offset: 60442
Basic element already written: Art. L. 124-1. at offset: 60678
Basic element already written: Art. L. 124-2. at offset: 61204
Basic element already written: Art. L. 124-3. at offset: 63992
Basic element already written: Art. L. 124-4. at offset: 65318
Basic element already written: Art. L. 124-5. at offset: 65933
Basic element already written: Art. L. 124-6. at offset: 66893
Basic element already written: Art. L. 124-7. at offset: 67610
Basic element already written: Art. L. 124-8. at offset: 70964
Basic element already written: Art. L. 124-9. at offset: 71450
Basic element already written: Art. L. 124-10. at offset: 73317
Basic element already written: Art. L. 124-11. at offset: 76307
Basic element already written: Art. L. 124-12. at offset: 79293
Basic element already written: Art. L. 124-13. at offset: 81395
Basic element already written: Art. L. 125-1. at offset: 81805
Basic element already written: Art. L. 125-2. at offset: 84376
Basic element already written: Art. L. 125-3. at offset: 84599
Basic element already written: Art. L. 125-4. at offset: 84823
Basic element already written: Art. L. 125-5. at offset: 85898
Basic element already written: Art. L. 125-6. at offset: 87212
Basic element already written: Art. L. 125-7. at offset: 87858
Basic element already written: Art. L. 125-8. at offset: 88613
Basic element already written: Art. L. 125-9. at offset: 90156
Basic element already written: Art. L. 126-1. at offset: 90633
Basic element already written: Art. L. 127-1. at offset: 93915
Basic element already written: Art. L. 127-2. at offset: 95309
Basic element already written: Art. L. 127-3. at offset: 98418
Basic element already written: Art. L. 127-4. at offset: 100291
Basic element already written: Art. L. 127-5. at offset: 100802
Basic element already written: Art. L. 127-6. at offset: 102649
Basic element already written: Art. L. 131-1. at offset: 104619
Basic element already written: Art. L. 131-2. at offset: 105709
Basic element already written: Art. L. 131-3. at offset: 106769
Basic element already written: Art. L. 131-4. at offset: 109294
Basic element already written: Art. L. 131-5. at offset: 110706
Basic element already written: Art. L. 131-6. at offset: 111047
Basic element already written: Art. L. 131-7. at offset: 112561
Basic element already written: Art. L. 131-8. at offset: 113739
Basic element already written: Art. L. 131-9. at offset: 115527
Basic element already written: Art. L. 131-10. at offset: 115896
Basic element already written: Art. L. 131-11. at offset: 116729
Basic element already written: Art. L. 131-12. at offset: 117727
Basic element already written: Art. L. 131-13. at offset: 118585
Basic element already written: Art. L. 131-14. at offset: 119847
Basic element already written: Art. L. 131-15. at offset: 120232
Basic element already written: Art. L. 131-16. at offset: 120510
Basic element already written: Art. L. 131-17. at offset: 121120
Basic element already written: Art. L. 131-18. at offset: 121521
Basic element already written: Art. L. 131-19. at offset: 121832
Basic element already written: Art. L. 131-20. at offset: 122799
Basic element already written: Art. L. 131-21. at offset: 123059
Basic element already written: Art. L. 132-1. at offset: 123950
Basic element already written: Art. L. 132-2. at offset: 127380
Basic element already written: Art. L. 132-3. at offset: 128243
Basic element already written: Art. L. 132-4. at offset: 128825
Basic element already written: Art. L. 133-1. at offset: 129157
Basic element already written: Art. L. 133-2. at offset: 130629
Basic element already written: Art. L. 133-3. at offset: 131184
Basic element already written: Art. L. 134-1. at offset: 131548
Basic element already written: Art. L. 134-1. at offset: 132400
Basic element already written: Art. L. 134-2. at offset: 132920
Basic element already written: Art. L. 134-3. at offset: 133525
Basic element already written: Art. L. 141-1. at offset: 135733
Basic element already written: Art. L. 141-2. at offset: 139886
Basic element already written: Art. L. 141-3. at offset: 141344
Basic element already written: Art. L. 141-4. at offset: 141784
Basic element already written: Art. L. 142-1. at offset: 142053
Basic element already written: Art. L. 142-2. at offset: 143503
Basic element already written: Art. L. 142-3. at offset: 146175
Basic element already written: Art. L. 142-4. at offset: 149173
Basic element already written: Art. L.143-1. at offset: 150972
Basic element already written: Art. L.143-2. at offset: 153305
Basic element already written: Art. L.143-3. at offset: 155265
Basic element already written: Art. L.144-1. at offset: 155683
Basic element already written: Art. L.144-2. at offset: 156754
Basic element already written: Art. L.144-3. at offset: 157101
Basic element already written: Art. L.144-4. at offset: 157436
Basic element already written: Art. L.144-5. at offset: 158256
Basic element already written: Art. L.144-6. at offset: 160280
Basic element already written: Art. L.144-7. at offset: 161449
Basic element already written: Art. L.144-8. at offset: 161885
Basic element already written: Art. L.144-9. at offset: 163032
Basic element already written: Art. L.144-10. at offset: 163587
Basic element already written: Art. L. 151-1. at offset: 164838
Basic element already written: Art. L. 151-2. at offset: 165522
Basic element already written: Art. L. 151-3. at offset: 165956
Basic element already written: Art. L. 151-4. at offset: 167088
Basic element already written: Art. L. 151-5. at offset: 167261
Basic element already written: Art. L. 151-6. at offset: 167516
Basic element already written: Art. L. 151-7. at offset: 167963
Basic element already written: Art. L. 151-8. at offset: 168888
Basic element already written: Art. L. 151-9. at offset: 169084
Basic element already written: Art. L. 161-1. at offset: 169337
Basic element already written: Art. L. 161-2. at offset: 169920
Basic element already written: Art. L. 161-3. at offset: 172150
Basic element already written: Art. L. 161-4. at offset: 173095
Basic element already written: Art. L. 161-5. at offset: 173417
Basic element already written: Art. L. 161-6. at offset: 174067
Basic element already written: Art. L. 161-7. at offset: 175213
Basic element already written: Art. L. 161-8. at offset: 176294
Basic element already written: Art. L. 162-1. at offset: 177068
Basic element already written: Art. L. 162-2. at offset: 178570
Basic element already written: Art. L. 162-3. at offset: 179982
Basic element already written: Art. L. 162-4. at offset: 180278
Basic element already written: Art. L. 162-5. at offset: 181837
Basic element already written: Art. L. 162-6. at offset: 183146
Basic element already written: Art. L. 162-7. at offset: 184224
Basic element already written: Art. L. 162-8. at offset: 185423
Basic element already written: Art. L. 162-9. at offset: 187656
Basic element already written: Art. L. 162-10. at offset: 187869
Basic element already written: Art. L. 162-11. at offset: 189840
Basic element already written: Art. L. 162-12. at offset: 190180
Basic element already written: Art. L. 162-13. at offset: 196019
Basic element already written: Art. L. 162-14. at offset: 197432
Basic element already written: Art. L. 162-15. at offset: 197774
Basic element already written: Art. L. 163-1. at offset: 198063
Basic element already written: Art. L. 163-2. at offset: 198669
Basic element already written: Art. L. 163-3. at offset: 200792
Basic element already written: Art. L. 163-4. at offset: 202829
Basic element already written: Art. L. 163-5. at offset: 204529
Basic element already written: Art. L. 163-6. at offset: 204654
Basic element already written: Art. L. 164-1. at offset: 205188
Basic element already written: Art. L. 164-2. at offset: 205453
Basic element already written: Art. L. 164-3. at offset: 206710
Basic element already written: Art. L. 164-4. at offset: 207341
Basic element already written: Art. L. 164-5. at offset: 207759
Basic element already written: Art. L. 164-6. at offset: 208782
Basic element already written: Art. L. 164-7. at offset: 209171
Basic element already written: Art. L. 164-8. at offset: 210620
Basic element already written: Art. L. 164-9. at offset: 213111
Basic element already written: Art. L. 164-10. at offset: 213966
Basic element already written: Art. L. 164-11. at offset: 214308
Basic element already written: Art. L. 165-1. at offset: 215108
Basic element already written: Art. L. 166-1. at offset: 218417
Basic element already written: Art. L. 166-2. at offset: 219278
Basic element already written: Art. L. 166-3. at offset: 226824
Basic element already written: Art. L. 166-4. at offset: 228573
Basic element already written: Art. L. 166-5. at offset: 230282
Basic element already written: Art. L. 166-6. at offset: 231008
Basic element already written: Art. L. 166-7. at offset: 232173
Basic element already written: Art. L. 166-8. at offset: 232776
Basic element already written: Art. L. 166-9. at offset: 233380
Closing the document
END XML WRITE UP
