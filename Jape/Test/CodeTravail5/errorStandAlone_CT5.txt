Exception in thread "AWT-EventQueue-0" java.lang.NullPointerException
	at gate.gui.docview.AnnotationSetsView$MouseStoppedMovingAction.actionPerformed(AnnotationSetsView.java:1963)
	at javax.swing.Timer.fireActionPerformed(Timer.java:313)
	at javax.swing.Timer$DoPostEvent.run(Timer.java:245)
	at java.awt.event.InvocationEvent.dispatch(InvocationEvent.java:311)
	at java.awt.EventQueue.dispatchEventImpl(EventQueue.java:756)
	at java.awt.EventQueue.access$500(EventQueue.java:97)
	at java.awt.EventQueue$3.run(EventQueue.java:709)
	at java.awt.EventQueue$3.run(EventQueue.java:703)
	at java.security.AccessController.doPrivileged(Native Method)
	at java.security.ProtectionDomain$JavaSecurityAccessImpl.doIntersectionPrivilege(ProtectionDomain.java:80)
	at java.awt.EventQueue.dispatchEvent(EventQueue.java:726)
	at java.awt.EventDispatchThread.pumpOneEventForFilters(EventDispatchThread.java:201)
	at java.awt.EventDispatchThread.pumpEventsForFilter(EventDispatchThread.java:116)
	at java.awt.EventDispatchThread.pumpEventsForHierarchy(EventDispatchThread.java:105)
	at java.awt.EventDispatchThread.pumpEvents(EventDispatchThread.java:101)
	at java.awt.EventDispatchThread.pumpEvents(EventDispatchThread.java:93)
	at java.awt.EventDispatchThread.run(EventDispatchThread.java:82)
Exception in thread "AWT-EventQueue-0" java.lang.NullPointerException
	at gate.gui.docview.AnnotationSetsView$MouseStoppedMovingAction.actionPerformed(AnnotationSetsView.java:1963)
	at javax.swing.Timer.fireActionPerformed(Timer.java:313)
	at javax.swing.Timer$DoPostEvent.run(Timer.java:245)
	at java.awt.event.InvocationEvent.dispatch(InvocationEvent.java:311)
	at java.awt.EventQueue.dispatchEventImpl(EventQueue.java:756)
	at java.awt.EventQueue.access$500(EventQueue.java:97)
	at java.awt.EventQueue$3.run(EventQueue.java:709)
	at java.awt.EventQueue$3.run(EventQueue.java:703)
	at java.security.AccessController.doPrivileged(Native Method)
	at java.security.ProtectionDomain$JavaSecurityAccessImpl.doIntersectionPrivilege(ProtectionDomain.java:80)
	at java.awt.EventQueue.dispatchEvent(EventQueue.java:726)
	at java.awt.EventDispatchThread.pumpOneEventForFilters(EventDispatchThread.java:201)
	at java.awt.EventDispatchThread.pumpEventsForFilter(EventDispatchThread.java:116)
	at java.awt.EventDispatchThread.pumpEventsForHierarchy(EventDispatchThread.java:105)
	at java.awt.EventDispatchThread.pumpEvents(EventDispatchThread.java:101)
	at java.awt.EventDispatchThread.pumpEvents(EventDispatchThread.java:93)
	at java.awt.EventDispatchThread.run(EventDispatchThread.java:82)
Unable to find a subdvision segment in article: Art. L. 591-1. at offset: 294410
The following text will be inserted as-is in the XML document: 
Art. L. 591-1. - Objet
Le présent titre réglemente l'intervention de l'Etat dans l'organisation et le financement d'initiatives prises par les employeurs en matière de lutte pour l'intégration des demandeurs d'emploi difficiles à insérer ou réinsérer sur le marché du travail, et ce indépendamment de la situation conjoncturelle.


Unable to find a subdvision segment in article: Art. L. 591-2. at offset: 294741
The following text will be inserted as-is in the XML document: 
Art. L. 591-2. - Définitions
Aux fins du présent titre, on entend par
(a) "activités d'insertion ou de réinsertion professionnelle": activités d'un employeur ayant comme finalité de préparer l'intégration ou la réintégration sur le marché du travail de personnes éprouvant des difficultés particulières pour trouver un emploi indépendamment de l'évolution conjoncturelle;
(b) "activités socio-économiques": activités d'un employeur ayant comme finalité d'offrir au bénéficiaire, dans le cadre d'un contrat de travail, un emploi et un encadrement de nature socio-économique tenant compte des difficultés éprouvées par le bénéficiaire pour trouver un emploi sur le marché du travail;
(c) "agrément": autorisation du ministre ayant l'emploi dans ses attributions d'exercer une activité d'insertion ou de réinsertion professionnelle ou une activité socio-économique;
(d) "bénéficiaire": le demandeur d'emploi, sans emploi, sans distinction d'âge, qui remplit les conditions prévues par l'article L. 591-3, paragraphe (1) et qui participe soit à une activité d'insertion ou de réinsertion professionnelle soit à une activité socio-économique;
(e) "bilan de compétence": le bilan de compétence permet d'analyser les compétences professionnelles et personnelles, les aptitudes et les motivations du demandeur d'emploi et de définir un projet professionnel et le cas échéant un projet de formation;
(f) "bilan d'insertion professionnelle (BIP)": le BIP est un outil de travail en groupe qui agit sur le processus d'insertion professionnelle du demandeur d'emploi par une triple action:
- acquisition d'une meilleure connaissance du fonctionnement du marché de l'emploi et de la vie en entreprise;
- prise en compte de son positionnement personnel et professionnel par rapport aux exigences du marché du travail;
- élaboration d'un projet professionnel réaliste et d'action, adapté aux conditions du marché du travail;
(g) "diagnostic évolutif de l'insertion professionnelle (DEIP)": le DEIP est un protocole scientifique qui a pour but l'évaluation objective de l'employabilité du demandeur d'emploi afin de déterminer son aptitude à suivre un programme d'intégration dans un environnement professionnel donné;
(h) "convention de coopération": convention conclue entre le ministre ayant l'emploi dans ses attributions et l'employeur relative aux conditions et modalités du soutien financier par l'Etat d'initiatives prises pour le rétablissement du plein emploi en application du présent titre;
(i) "demandeur d'emploi": personne sans emploi, disponible pour le marché du travail, à la recherche d'un emploi approprié, non affectée à une mesure active pour remploi, indemnisée ou non indemnisée et ayant respecté les obligations de suivi de "l'Agence pour le développement de l'emploi";
(j) initiative prise en matière de lutte pour l'intégration des demandeurs d'emploi difficiles à insérer ou réinsérer sur le marché du travail": terme général reprenant à la fois les activités d'insertion ou de réinsertion professionnelles et les activités socio-économiques;
(k) "mesure active en faveur de l'emploi":
- le contrat d'appui emploi au sens des articles L. 543-1 à L. 543-14;
- le contrat d'initiation à l'emploi au sens des articles L. 543-15 à L. 543-29;
- le stage de réinsertion au sens des articles L. 524-1 à L. 524-7;
- le pool des assistants, conformément à l'article VII, paragraphe (1) de la loi du 31 juillet 1995 relative à l'emploi et à la formation professionnelle, telle que modifiée;
- les mises au travail de chômeurs indemnisés au sens de l'article L. 523-1 paragraphes (2) et (3);
- les formations, séminaires ou toute autre mesure assignée par les services compétents de "l'Agence pour le développement de l'emploi";
(l) "parcours d'insertion individuel": le parcours d'insertion individuel précise pour chaque demandeur d'emploi, où la situation l'exige, les actions à déployer et qui sont destinées à soutenir, lancer ou relancer le processus d'insertion professionnelle du demandeur d'emploi.


Unable to find a subdvision segment in article: Art. L. 591-4. at offset: 300239
The following text will be inserted as-is in the XML document: 
Art. L. 591-4. - Mise en œuvre
La mise en œuvre et le suivi du présent titre sont confiés à "l'Agence pour le développement de l'emploi".



Unable to find a subdvision segment in article: Art. L. 592-1. at offset: 300483
The following text will be inserted as-is in the XML document: 
Art. L. 592-1. - Forme juridique de l'employeur
Les avantages financiers accordés en application du chapitre III du présent titre s'adressent à tous les employeurs dans les conditions et sous les réserves fixées au chapitre III.


Unable to find a subdvision segment in article: Art. L. 592-3. at offset: 302037
The following text will be inserted as-is in the XML document: 
Art. L. 592-3. - Etablissement d'un parcours d'insertion individuel du bénéficiaire
Au cours des activités d'insertion ou de réinsertion professionnelles respectivement des activités socio-économiques, le bénéficiaire se voit établir, sur base du bilan de compétences, du bilan d'insertion professionnel et/ou du diagnostic évolutif de l'insertion professionnelle, un parcours d'insertion individuel élaboré en étroite concertation par les services compétents de "l'Agence pour le développement de l'emploi", l'employeur et le bénéficiaire, en fonction du niveau de formation et de l'occupation de ce dernier.


Unable to find a subdvision segment in article: Art. L. 593-3. at offset: 304359
The following text will be inserted as-is in the XML document: 
Art. L. 593-3. - Conditions d'obtention de l'agrément
Pour obtenir l'agrément, l'employeur doit:
a) documenter les prestations à fournir à l'égard des bénéficiaires et plus particulièrement les mesures d'encadrement prévues;
b) remplir les conditions d'honorabilité dans le chef des membres des organes dirigeants de la personne morale responsables de la gestion des activités d'insertion ou de réinsertion professionnelles et des activités socio-économiques dans le chef du personnel dirigeant;
c) ne pas avoir été mis en état de faillite, de concordat préventif de faillite ou de déconfiture;
d) avoir répondu à l'ensemble des exigences légales en matière de législation sur les sociétés et associations;
e) suffire, s'il y a lieu, aux obligations de la loi modifiée du 10 juin 1999 relative aux établissements classés;
f) garantir que les activités agréées soient accessibles aux bénéficiaires indépendamment de toutes considérations d'ordre idéologique, philosophique ou religieux et que le bénéficiaire ait droit à la protection de sa vie privée et au respect de ses convictions idéologiques, philosophiques et religieuses.


