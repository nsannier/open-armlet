file = Test/Conge_Parental/texte_Conge_Parental.txt
shortTitle = Loi congé parental
longTitle = Loi du **/**/**** portant réforme du congé parental et modifiant blablabla
rootELI = http://eli.legilux.public.lu/eli/etat/leg/loi/2016/11/22/n2
output = Test/Conge_Parental/output_Conge_Parental.xml
log = Test/Conge_Parental/logStandAlone.txt
error = Test/Conge_Parental/errorStandAlone.txt
jsonRules = Structure/DefaultStructure